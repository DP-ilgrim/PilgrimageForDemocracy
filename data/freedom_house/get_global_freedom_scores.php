#!/usr/bin/php
<?php

$countries = array();

function get_data() {
	// hxnormalize -x -l 2000:
	//                -l 2000 prevents "Partly free" from being broken up into two lines, adding unnecessary spaces.
	$command = "curl -Ss https://freedomhouse.org/countries/freedom-world/scores | hxnormalize -x -l 2000  | hxselect   table tr.country-scores | tr --delete '\n' > table_global_freedom.html";
	shell_exec($command);
}

#
# Get summary
#
# $ curl -Ss https://freedomhouse.org/country/taiwan | hxnormalize -x | hxselect -c -s '\n' div.field--name-field-intro p
#   Taiwan’s vibrant and competitive democratic system has allowed for regular peaceful transfers of power since 2000, and protections for civil
#   liberties are generally robust. Ongoing concerns include inadequate safeguards against the exploitation of migrant workers and the Chinese
#   government’s efforts to influence policymaking, media, and the democratic infrastructure.
function get_summary($country) {
	global $countries;
	$command = "curl -Ss https://freedomhouse.org/country/${country} | hxnormalize -x -l 20000 | hxselect -c -s '\n' div.field--name-field-intro p";
	exec ($command, $lines);
	echo $country . ":\t" . $lines[0] . "\n\n";
	$countries[$country]['summary'] = str_replace("'", "\'", $lines[0]);
}

#
# Get list of countries
#
# $ curl -Ss https://freedomhouse.org/countries/freedom-world/scores | hxnormalize -x | hxselect  -c -s '\n' table tr.country-scores td   a::attr\(href\)
#   /country/abkhazia/freedom-world/2023
#   /country/afghanistan/freedom-world/2023
#   /country/albania/freedom-world/2023
#   /country/algeria/freedom-world/2023
#   /country/andorra/freedom-world/2023
#   /country/angola/freedom-world/2023
#   ...
function get_country($data) {
	global $countries;
	$command = "echo '${data}' | hxselect  -c -s '\n' td   a::attr\(href\)";
	exec ($command, $lines);
	$line = array_shift($lines);
	preg_match('#/country/(.*)/freedom#', $line, $match);
	$country = $match[1];
	$countries[$country] = array();
	return $country;
}

#
# Get name of countries
#
# $ curl -Ss https://freedomhouse.org/countries/freedom-world/scores | hxnormalize -x | hxselect -c -s '\n' table tr.country-scores td a
#   Abkhazia*
#   Afghanistan
#   Albania
#   Algeria
#   Andorra
#   Angola
function get_name($country, $data) {
	global $countries;
	$command = "echo '${data}' | hxselect  -c -s '\n' td   a";
	exec ($command, $lines);
	$line = array_shift($lines);
	$countries[$country]['name'] = str_replace("*", "", $line);
}

#
# Get total score
#
# $ curl -Ss https://freedomhouse.org/countries/freedom-world/scores | hxnormalize -x | hxselect -c  -s '\n' table tr.country-scores td:nth-child\(2\) span.score
#
function get_total_score($country, $data) {
	global $countries;
	$command = "echo '${data}' | hxselect  -c -s '\n' td:nth-child\(2\) span.score";
	exec ($command, $lines);
	$countries[$country]['score'] = $lines[0];
}

#
# Get status
#
# $ curl -Ss https://freedomhouse.org/countries/freedom-world/scores | hxnormalize -x | hxselect -c  -s '\n' table tr.country-scores td:nth-child\(2\) span.status
#
function get_status($country, $data) {
	global $countries;
	$command = "echo '${data}' | hxselect  -c -s '\n' td:nth-child\(2\) span.status";
	exec ($command, $lines);
	$countries[$country]['status'] = $lines[0];
}

#
# Get political rights
#
# $ curl -Ss https://freedomhouse.org/countries/freedom-world/scores | hxnormalize -x | hxselect -c  -s '\n' table tr.country-scores td:nth-child\(3\)
#
function get_political_rights($country, $data) {
	global $countries;
	$command = "echo '${data}' | hxselect  -c -s '\n' td:nth-child\(3\)";
	exec ($command, $lines);
	$countries[$country]['political rights'] = $lines[0];
}

#
# Get civil liberties
#
# $ curl -Ss https://freedomhouse.org/countries/freedom-world/scores | hxnormalize -x | hxselect -c  -s '\n' table tr.country-scores td:nth-child\(4\)
#
function get_civil_liberties($country, $data) {
	global $countries;
	$command = "echo '${data}' | hxselect  -c -s '\n' td:nth-child\(4\)";
	exec ($command, $lines);
	$countries[$country]['civil liberties'] = $lines[0];
}

function write_data() {
	global $countries;

	$content  = "<?php\n";
	$content .= "# The content of this file is copyrighted by Freedom House https://freedomhouse.org/\n";
	$content .= "# The summary for each country can only be used in specially designed page elements that\n";
	$content .= "# 1) make it plain that the text was written by Freedom House\n";
	$content .= "# 2) link back to the Freedom House website where the given summary can be found.\n";
	$content .= "#\n";
	$content .= "# Freedom House and the Pilgrimage for Democracy and Social Justice\n";
	$content .= "# are two projects independent from each other\n";
	$content .= "# and neither one endorses the other\n";
	$content .= "# although there is a lot of communality between the two projects' goals.\n";
	$content .= "\n";
	$content .= "\n";
	$content .= "\$freedom_house_global_freedom = array(\n";
	// [taiwan] => Array (
	//                     [name] => Taiwan
	//                     [status] => Free
	//                     [political rights] => 38
	//                     [civil liberties] => 56
	//                     [score] => 94
	//                     [summary] => '...'
	// )

	foreach ($countries as $country => $data) {
		$content .= "\t'${country}' => array(\n";
			$content .= "\t\t'name'             => '${data['name']}',\n";
			$content .= "\t\t'status'           => '${data['status']}',\n";
			$content .= "\t\t'political rights' => '${data['political rights']}',\n";
			$content .= "\t\t'civil liberties'  => '${data['civil liberties']}',\n";
			$content .= "\t\t'score'            => '${data['score']}',\n";
			$content .= "\t\t'summary'          => '${data['summary']}',\n";
		$content .= "\t),\n";
	}
	$content .= ");\n";
	file_put_contents('data.php', $content);
}


$do_get_data = 1;
if ($do_get_data) {
	get_data();
}

$command = "cat table_global_freedom.html | hxselect  -c -s '\n' tr.country-scores";
$lines = array();
exec ($command, $lines);

foreach ($lines as $line) {
	$country = get_country($line);
	get_name            ($country, $line);
	get_status          ($country, $line);
	get_political_rights($country, $line);
	get_civil_liberties ($country, $line);
	get_total_score     ($country, $line);

	$do_get_summary = 1;
	if ($do_get_summary) {
		// Getting the summary takes a long time as we need to query the Freedom House website for each country.
		get_summary($country);
	}
}

write_data();

exit;
