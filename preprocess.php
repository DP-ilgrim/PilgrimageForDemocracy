#!/usr/bin/php
<?php
require_once('include/preprocess.php');
global $preprocess_index_src;


$options = '';
$process_all = true;
$src = '';

$show_keyword = false;
if (isset($argv[1])) {
	if ($argv[1] == "wip") {
		$options .= ' --wip ';
		if (isset($argv[2])) {
			$src = $argv[2];
			$process_all = false;
		}
	}
	else if ($argv[1] == "kw") {
		if (isset($argv[2])) {
			$show_keyword = true;
			$src = $argv[2];
			$process_all = false;
		}
	}
	else {
		$src = $argv[1];
		$process_all = false;
	}
}



if ($process_all) {
	foreach(glob("http/*.html") as $file) {
		unlink($file);
	}

	echo "Preprocessing all targets.\n";
	foreach ($preprocess_index_src AS $src => $data) {
		$dest = $data["dest"];
		shell_exec("php $src $options > $dest");
	}
}
else {
	if (isset($preprocess_index_src[$src])) {
		echo "Preprocessing $src .\n";
		$dest = $preprocess_index_src[$src]["dest"];
		shell_exec("php $src $options > $dest");

		if ($show_keyword) {
			echo "\n";
			$dest_keyword = substr($dest, 5);
			if (isset($preprocess_index_dest[$dest_keyword])) {
				$keyword = create_auto_keyword($dest_keyword);
				echo $keyword . "\n";
			}
		}
	}
	else {
		echo "	!! The source ${src} is not valid.\n";
	}
}

update_preprocess_index();
