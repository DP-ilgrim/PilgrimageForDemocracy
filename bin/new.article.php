#!/usr/bin/php
<?php
include_once('include.php');
require_once('include/preprocess.php');

function get_wikipedia_sources() {
	$sections = '';
	while (true) {
		$wikipediaLink = (string)readline("Link to wikipedia article (leave empty to quit): ");
		if (!$wikipediaLink) {
			break;
		}
		$wikipediaSection = create_wikipedia_section($wikipediaLink);
		$sections .= $wikipediaSection;
	}
	return $sections;
}

$directory = "";
if (!empty($argv[1])) {
	$directory = $argv[1];
}

$isCountry = (string)readline("Is the new article a country? (no) ");

if ($isCountry) {
	$articleName = (string)readline("Name of country: ");
}
else {
	$articleName = (string)readline("Title of article: ");
}

$fileName = strtolower($articleName);
$fileName = str_replace(" ", "_", $fileName);
$fileName = tidy_titlePath($fileName);

$src  = 'src/' . $directory . $fileName . ".php";
$dest = 'http/' . $directory . $fileName . ".html";

if (isset($preprocess_index_src[$src])) {
	echo "The file \"$src\" already exists. Aborting.\n";
	exit;
}

$sections = scandir('section/');

echo "\t 0: \t New section\n";
foreach ($sections AS $key => $section) {
	if ($section != "." && $section != ".." && $section != "all.php") {
		echo "\t $key: \t $section \n";
	}
}

$section_index = (int)readline("Which section shall the page preview object be inserted into? ");
$section = $sections[$section_index];
if ($section_index == 0) {
	$new_section_name = readline("Enter the name of the new section (without spaces, nor .php extension): ");
	$section = $new_section_name . '.php';
	$header  = "<?php\n";
	$header .= "include_once('include/init.php');\n\n";
	file_put_contents("section/${section}", $header);
}


$section_content = file_get_contents('templates/section.php');
$section_content = str_replace("%ArticleName%", $articleName, $section_content);
$section_content = str_replace("%titlePath%", $fileName, $section_content);
$section_content = str_replace("%directory%", $directory, $section_content);

file_put_contents("section/${section}", $section_content, FILE_APPEND);

$content = file_get_contents('templates/article.php');
$content = str_replace("%ArticleName%", $articleName, $content);
$content = str_replace("%titlePath%", $fileName, $content);

if ($isCountry) {
	$content = str_replace("%section%", 'world.php', $content);
	$content = str_replace("%section_democracy_world%", '$body .= printPageSection(\'world.html\');', $content);
	$content = str_replace("%countryIndices%", '$body .= printCountryIndices(\'' . $articleName . '\');', $content);
}
else {
	$content = str_replace("%section%", $section, $content);
	$content = str_replace("%section_democracy_world%", '', $content);
	$content = str_replace("%countryIndices%", '', $content);
}

$content .= get_wikipedia_sources();


file_put_contents($src, $content);
chmod($src, 0755);


echo "\n";
echo $src;
echo "\n";

$preprocess_index_src[$src] = array(
	'dest' => $dest,
);

$dest = $fileName . ".html";
$preprocess_index_dest[$directory . $dest] = array(
	'include' => "section/$section",
	'name' => "div_section_${fileName}",
	'keyword' => $articleName,
);

update_preprocess_index();
$keywords = create_auto_keyword($dest);
file_put_contents("templates/keywords.php", $keywords, FILE_APPEND);
