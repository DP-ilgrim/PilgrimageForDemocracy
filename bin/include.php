<?php

$source_dir = '';

function tidy_titlePath($titlePath) {
	$titlePath = str_replace(" ", "_", $titlePath);
	$titlePath = str_replace("–", "_", $titlePath);
	$titlePath = str_replace("-", "_", $titlePath);
	$titlePath = str_replace(":", "_", $titlePath);
	$titlePath = str_replace(",", "_", $titlePath);
	$titlePath = str_replace(";", "_", $titlePath);
	$titlePath = str_replace(".", "_", $titlePath);
	$titlePath = str_replace("(", "_", $titlePath);
	$titlePath = str_replace(")", "_", $titlePath);
	$titlePath = str_replace("'", "_", $titlePath);
	$titlePath = str_replace("?", "_", $titlePath);
	$titlePath = str_replace("__", "_", $titlePath);
	$titlePath = trim($titlePath, "_");
	return $titlePath;
}

function create_wikipedia_section($link) {
	global $source_dir;
	$summary = '';

	$link = urldecode($link);
	$titlePath = strstr($link, 'wiki/', false);
	$titlePath = substr($titlePath, 5);

	$titlePath = tidy_titlePath($titlePath);

	$title = str_replace("_", " ", $titlePath);;

	$content = file_get_contents($source_dir . 'templates/wikipedia.section.php');
	$content = str_replace("%link%", $link, $content);
	$content = str_replace("%titlePath%", $titlePath, $content);
	$content = str_replace("%title%", $title, $content);
	$content = str_replace("%summary%", $summary, $content);

	return  $content;
}
