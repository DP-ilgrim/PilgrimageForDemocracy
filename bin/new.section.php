#!/usr/bin/php
<?php
include_once('include.php');

$source_dir = '../';

$summary   = "";
$link      = "";
$class     = "";
$title     = "";
$titlePath = "";
$content   = "";
$prefix    = "";

if (substr($argv[1], 0, 4) == "http") {
	$link = urldecode($argv[1]);

	if (strstr($link, 'https://freedomhouse.org/')) {
		$class = 'FreedomHouseContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h1 | tr --delete '\n'";
		$prefix = 'freedomhouse_';

	}
	elseif (strstr($link, 'scholar.google')) {
		$class = 'GoogleScholarContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  div#gsc_prf_in | tr --delete '\n'";
		$prefix = 'googlescholar_';
	}
	elseif (strstr($link, 'wikipedia.org/')) {
		$class = 'WikipediaContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h1 span | tr --delete '\n'";
		$prefix = 'wikipedia_';
	}
	elseif (strstr($link, 'https://codeberg.org/')) {
		$class = 'CodebergContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h1 span#issue-title | tr --delete '\n'";
		$prefix = 'codeberg_';
	}
	elseif (!empty($argv[2])) {
		$class = 'WebsiteContentSection';
		$command = '';
		$arg = 2;
		while (!empty($argv[$arg])) {
			$title .= $argv[$arg] . " ";
			$arg++;
		}
	}
	else {
		$class = 'WebsiteContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h1 | tr --delete '\n' | html2text -from_encoding UTF-8";
	}

	if ($command) {
		exec ($command, $lines);
	}
	if (!empty($lines)) {
		$title = $lines[0];
	}
	else {
		if (!$title) {
			echo "Failed to extract h1 title from the page. You can try again by passing the title as arguments after the link.\n";
		}
	}
	$titlePath = tidy_titlePath($title);
}


$content = file_get_contents($source_dir . 'templates/div.php');
$content = str_replace("%link%", $link, $content);
$content = str_replace("%object-class%", $class, $content);
$content = str_replace("%object-name%", $prefix . $titlePath, $content);
$content = str_replace("%title%", $title, $content);
$content = str_replace("%summary%", $summary, $content);


echo "\n";
echo $content;
echo "\n";
