<?php
include_once('section/speech.php');
$h1['en'] = 'Freedom of speech';

//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>While the concept of freedom of speech is well documented (see Wikipedia articles below),
	it is however often misunderstood.</p>

	<p>The purpose of this page will be to explain a few salient points and try to correct some misconceptions.</p>
	HTML;



$div_wikipedia_Freedom_of_speech = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Freedom_of_speech', 'Freedom of speech');
$div_wikipedia_Freedom_of_speech['stars']   = -1;
$div_wikipedia_Freedom_of_speech['class'][] = '';
$div_wikipedia_Freedom_of_speech['en'] = <<<HTML
	<p>Freedom of speech is a principle that supports the freedom of an individual or a community to articulate their opinions and ideas
	without fear of retaliation, censorship, or legal sanction.
	The right to freedom of expression has been recognised as a human right in the Universal Declaration of Human Rights
	and international human rights law by the United Nations.
	Many countries have constitutional law that protects free speech.</p>
	HTML;

$div_wikipedia_Freedom_of_speech_by_country = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Freedom_of_speech_by_country', 'Freedom of speech by country');
$div_wikipedia_Freedom_of_speech_by_country['stars']   = -1;
$div_wikipedia_Freedom_of_speech_by_country['class'][] = '';
$div_wikipedia_Freedom_of_speech_by_country['en'] = <<<HTML
	<p>The list is partially composed of the respective countries' government claims and does not fully reflect the de facto situation.</p>
	HTML;

$div_wikipedia_Speech_crimes = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Speech_crimes', 'Speech crimes');
$div_wikipedia_Speech_crimes['stars']   = -1;
$div_wikipedia_Speech_crimes['class'][] = '';
$div_wikipedia_Speech_crimes['en'] = <<<HTML
	<p>Speech crimes are certain kinds of speech that are criminalized by promulgated laws or rules.
	Criminal speech is a direct preemptive restriction on freedom of speech, and the broader concept of freedom of expression.</p>
	HTML;

$div_wikipedia_Censorship = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Censorship', 'Censorship');
$div_wikipedia_Censorship['stars']   = -1;
$div_wikipedia_Censorship['class'][] = '';
$div_wikipedia_Censorship['en'] = <<<HTML
	<p>Direct censorship may or may not be legal, depending on the type, location, and content.
	Many countries provide strong protections against censorship by law, but none of these protections are absolute
	and frequently a claim of necessity to balance conflicting rights is made, in order to determine what could and could not be censored.</p>
	HTML;


$body .= printSection($div_stub);

$body .= printSection($div_introduction);

$body .= printSection($div_wikipedia_Freedom_of_speech);
$body .= printSection($div_wikipedia_Freedom_of_speech_by_country);
$body .= printSection($div_wikipedia_Speech_crimes);
$body .= printSection($div_wikipedia_Censorship);

include('include/page.php');
