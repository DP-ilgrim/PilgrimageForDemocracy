<?php
include_once('section/all.php');
$h1['en'] = 'Lawmaking';



//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Lawmaking = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Lawmaking', 'Lawmaking');
$div_wikipedia_Lawmaking['stars']   = -1;
$div_wikipedia_Lawmaking['class'][] = '';
$div_wikipedia_Lawmaking['en'] = <<<HTML
	<p>Lawmaking is the process of crafting legislation. In its purest sense, it is the basis of governance.
	Lawmaking in modern democracies is the work of legislatures, which exist at the local, regional, and national levels
	and make such laws as are appropriate to their level, and binding over those under their jurisdictions.</p>
	HTML;

$div_wikipedia_Legislation = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Legislation', 'Legislation');
$div_wikipedia_Legislation['stars']   = -1;
$div_wikipedia_Legislation['class'][] = '';
$div_wikipedia_Legislation['en'] = <<<HTML
	<p>Legislation is the process or result of enrolling, enacting, or promulgating laws
	by a legislature, parliament, or analogous governing body.</p>
	HTML;

$div_wikipedia_Legislature = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Legislature', 'Legislature');
$div_wikipedia_Legislature['stars']   = -1;
$div_wikipedia_Legislature['class'][] = '';
$div_wikipedia_Legislature['en'] = <<<HTML
	<p>A legislature is an assembly with the authority to make laws for a political entity such as a country or city.
	They are often contrasted with the executive and judicial powers of government.</p>
	HTML;

$div_wikipedia_Rule_of_law = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Rule_of_law', 'Rule of law');
$div_wikipedia_Rule_of_law['stars']   = -1;
$div_wikipedia_Rule_of_law['class'][] = '';
$div_wikipedia_Rule_of_law['en'] = <<<HTML
	<p>The rule of law is a political ideal that all citizens and institutions within a country, state, or community
	are accountable to the same laws, including lawmakers and leaders.</p>
	HTML;

$div_wikipedia_Deliberative_democracy = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Deliberative_democracy', 'Deliberative democracy');
$div_wikipedia_Deliberative_democracy['stars']   = -1;
$div_wikipedia_Deliberative_democracy['class'][] = '';
$div_wikipedia_Deliberative_democracy['en'] = <<<HTML
	<p>Deliberative democracy or discursive democracy is a form of democracy in which deliberation is central to decision-making.
	It often adopts elements of both consensus decision-making and majority rule.
	Deliberative democracy differs from traditional democratic theory in that authentic deliberation, not mere voting,
	is the primary source of legitimacy for the law.
	Deliberative democracy is closely related to consultative democracy, in which public consultation with citizens is central to democratic processes.</p>
	HTML;


$div_wikipedia_Agonism = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Agonism', 'Agonism');
$div_wikipedia_Agonism['stars']   = -1;
$div_wikipedia_Agonism['class'][] = '';
$div_wikipedia_Agonism['en'] = <<<HTML
	<p>Agonism (from Greek ἀγών agon, "struggle") is a political and social theory
	that emphasizes the potentially positive aspects of certain forms of conflict.
	It accepts a permanent place for such conflict in the political sphere,
	but seeks to show how individuals might accept and channel this conflict positively.
	Agonists are especially concerned with debates about democracy, and the role that conflict plays in different conceptions of it.</p>
	HTML;


$body .= printSection($div_stub);

$body .= printSection($div_introduction);

$body .= printPageSection('institutions.html');

$body .= printSection($div_wikipedia_Lawmaking);
$body .= printSection($div_wikipedia_Rule_of_law);
$body .= printSection($div_wikipedia_Legislature);
$body .= printSection($div_wikipedia_Legislation);
$body .= printSection($div_wikipedia_Deliberative_democracy);
$body .= printSection($div_wikipedia_Agonism);

include('include/page.php');
