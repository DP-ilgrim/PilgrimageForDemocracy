<?php
include_once('section/institutions.php');
$h1['en'] = 'World Future Council';


//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_World_Future_Council = newSection('wikipedia', 'https://en.wikipedia.org/wiki/World_Future_Council', 'World Future Council');
$div_wikipedia_World_Future_Council['stars']   = -1;
$div_wikipedia_World_Future_Council['class'][] = '';
$div_wikipedia_World_Future_Council['en'] = <<<HTML
	<p>The World Future Council (WFC) is a German non-profit foundation..
	It works to pass on a healthy and sustainable planet with just and peaceful societies to future generations.</p>
	HTML;


$body .= printSection($div_stub);

$body .= printSection($div_introduction);
$body .= printSection($div_wikipedia_World_Future_Council);
$body .= printPageSection('institutions.html');


include('include/page.php');
