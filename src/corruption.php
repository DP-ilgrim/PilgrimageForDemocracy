<?php
include_once('section/institutions.php');
$h1['en'] = 'Corruption';

$div_introduction= newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>Corruption is one of the most malign form of cancer of democracy.
	Corruption is an obstacle to the realisation of all human rights, without exception.</p>

	<p>Corruption disproportionately impacts the poorest members of the community.</p>

	<blockquote>
	"Not a single Sustainable Development Goal will be attained, and development will remain a utopia,
	as long as corruption prevails.
	Corruption kills and destroys lives daily.
	It is a curse which will only be curbed by our human determination to promote and protect the common good."
	<br>
	- Ketakandriana Rafitoson,<br>
	Executive Director of Transparency International Initiative Madagascar
	</blockquote>
	HTML;

$h2_transparency_international = newH2();
$h2_transparency_international['en'] = 'Transparency International';

$div_transparency_international = newSection();
$div_transparency_international['stars']   = -1;
$div_transparency_international['class'][] = '';
$div_transparency_international['en'] = <<<HTML
	<h3><a href="https://www.transparency.org/">transparency.org</a></h3>

	<p>The vision of <strong>Transparency International</strong> is of a world in which
	government, politics, business, civil society and people's daily lives are free of corruption.
	In order to get there, their mission is to stop corruption and promote
	transparency, accountability and integrity at all levels and across all sectors of society.</p>

	<p>The Corruption Perceptions Index reveals that the continued failure of most countries
	to significantly control corruption is contributing to a crisis in democracy around the world.</p>

	<p>Corruption chips away at democracy to produce a vicious cycle,
	where corruption undermines democratic institutions and, in turn, weak institutions are less able to control corruption.</p>


	<p>See their <a href="https://www.transparency.org/en/cpi/2021">2021 Corruption Perception Index</a>.</p>

	<p>See wikipedia: <a href="https://en.wikipedia.org/wiki/Transparency_International">Transparency International</a>.</p>
	HTML;

$body .= printSection($div_section_institutions);
$body .= printSection($div_stub);

$body .= printSection($div_introduction);

$body .= printH2($h2_transparency_international);
$body .= printSection($div_transparency_international);


include('include/page.php');
