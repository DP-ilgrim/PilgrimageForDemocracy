<?php
include_once('include/init.php');
$h1['en'] = 'Fair share';


$h2_introduction = newH2();
$h2_introduction['en'] = 'Introduction';

$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<ul>
		<li>People with money make money. People without it, don’t.</li>
		<li>the returns to capital are likely greater than those to labour.</li>
	</ul>

	<p>These two statements, if true, represent the root cause of a lot of social injustice.
	This section of the website aims to explore why it is so,
	and what can be done to remedy the problem.</p>

	<p>The goal is <em>not</em> to "<em>redistribute wealth</em>",
	i.e. take money from some people who earned it, and redistribute it to very poor people.
	On the contrary. The goal is to ensure that wages and benefits paid are in proportion to
	the people's labor, effort, experience, qualities and abilities.</p>

	<p>Also, in times of national economic hardship,
	it is often those who have the least who are asked to make the most sacrifices.</p>

	<p>Overall, the economic system is most unbalanced, and benefits the same few.
	We aim to investigate the <em>systemic causes</em> of this imbalance,
	and offer potential solutions.</p>
	HTML;

$div_balanced_economic_system = newSection();
$div_balanced_economic_system['stars']   = -1;
$div_balanced_economic_system['class'][] = '';
$div_balanced_economic_system['en'] = <<<HTML
	<h3>A balanced economic system</h3>

	<p>We ought to strike a balanced approach between unrestrained economic liberalism,
	and <a href="https://en.wikipedia.org/wiki/Dirigisme">socialist dirigism</a>.	</p>

	<p>See wikipedia: <a href="https://en.wikipedia.org/wiki/Social_market_economy">Social market economy</a>.</p>
	HTML;


$body .= printSection($div_stub);
$body .= printH2($h2_introduction);
$body .= printSection($div_introduction);
$body .= printSection($div_balanced_economic_system);


include('include/page.php');
