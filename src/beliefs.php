<?php
include_once('section/all.php');
$h1['en'] = 'Beliefs';



$snp['description'] = "Beliefs from the point of view of democracy and social justice.";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>This page shall discuss beliefs from the point of view of democracy and social justice.</p>

	<p>We discuss beliefs of any kind, whether religious, political or any other.</p>
	HTML;


$h2_Beliefs_and_the_first_principles_of_democracy = newH2();
$h2_Beliefs_and_the_first_principles_of_democracy['en'] = 'Beliefs and the first principles of democracy';

$div_Freedom_of_conscience = newSection();
$div_Freedom_of_conscience['stars']   = -1;
$div_Freedom_of_conscience['class'][] = '';
$div_Freedom_of_conscience['en'] = <<<HTML
	<h3>Freedom of conscience</h3>

	<p><strong>Freedom of conscience</strong> is one of the major tenets of democracy,
	according to which anyone is absolutely free to believe what they want.</p>

	<p>The belief that one holds may be false, and it may even be harmful to oneself,
	but one is still free to hold onto it, in respect of <strong>personal responsibility</strong> and <strong>personal liberty</strong>.</p>
	HTML;



$h2_Beliefs_and_the_second_principles_of_democracy = newH2();
$h2_Beliefs_and_the_second_principles_of_democracy['en'] = 'Beliefs and the second principles of democracy';


$div_Sharing_with_respect = newSection();
$div_Sharing_with_respect['stars']   = -1;
$div_Sharing_with_respect['class'][] = '';
$div_Sharing_with_respect['en'] = <<<HTML
	<h3>Sharing with respect</h3>

	<p>When one cherishes or feel strongly about some specific beliefs, it is natural to want to share it with other people in the community.
	However, and it is especially true for religious beliefs, one cannot impose one's beliefs onto others.
	When one communicates one's beliefs with other, one shall do so with respect,
	including respecting the possibility that other parties may not be interested or may have contradictory beliefs.</p>
	HTML;


$div_Harmful_wrong_beliefs = newSection();
$div_Harmful_wrong_beliefs['stars']   = -1;
$div_Harmful_wrong_beliefs['class'][] = '';
$div_Harmful_wrong_beliefs['en'] = <<<HTML
	<h3>Harmful wrong beliefs</h3>

	<p>A mature democracy shall have a way to deal with some strongly held beliefs that are both wrong, and harmful to others and to society in general.</p>

	<p>One good example of such is the widely held belief that the 2020 US presidential elections
	was massively fraudulent and that Donald J. Trump was somehow the legitimate president elect.
	These largely debunked claims have nonetheless been the cause of major social and political troubles in the US,
	putting at risk the very survival of American democracy.</p>
	HTML;




$div_wikipedia_Belief = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Belief', 'Belief');
$div_wikipedia_Belief['stars']   = -1;
$div_wikipedia_Belief['class'][] = '';
$div_wikipedia_Belief['en'] = <<<HTML
	<p>Beliefs are the subject of various important philosophical debates.
	Notable examples include:
	"What is the rational way to revise one's beliefs when presented with various sorts of evidence?",
	"Is the content of our beliefs entirely determined by our mental states?", or
	"Do the relevant facts have any bearing on our beliefs?"</p>
	HTML;

$body .= printSection($div_stub);
$body .= printSection($div_introduction);

$body .= printH2($h2_Beliefs_and_the_first_principles_of_democracy);
$body .= printSection($div_Freedom_of_conscience);
$body .= printPageSection('primary_features_of_democracy.html');

$body .= printH2($h2_Beliefs_and_the_second_principles_of_democracy);
$body .= printSection($div_Sharing_with_respect);
$body .= printSection($div_Harmful_wrong_beliefs);
$body .= printPageSection('secondary_features_of_democracy.html');

$body .= printSection($div_wikipedia_Belief);


include('include/page.php');
