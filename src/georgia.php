<?php
include_once('section/world.php');

$h1['en'] = 'Georgia';

$snp['description'] = "Georgia, democracy and human rights.";
$snp['image'] = "/copyrighted/roman-odintsov-8747731.1200-630.jpg";

$h2_introduction = newH2();
$h2_introduction['en'] = 'Georgia today';

$r1 = newRef('https://www.aljazeera.com/news/2023/3/8/georgians-protest-against-draft-law-on-media-nonprofits', 'Georgians protest ‘foreign agents’ draft law on media, nonprofits');
$r2 = newRef('https://www.aljazeera.com/news/2023/3/8/foreign-agents-law-why-are-protests-taking-place-in', '‘Foreign agents’ law: Why are protests taking place in Georgia?');
$r3 = newRef('https://www.aljazeera.com/program/inside-story/2023/3/8/whats-behind-the-recent-protests-in-georgia', 'What’s behind the recent protests in Georgia?');
$r4 = newRef('https://www.aljazeera.com/news/2023/3/9/no-to-the-russian-law-georgians-protest-foreign-agents-law', '‘No to the Russian law’: Georgians protest ‘foreign agents’ bill');
$r5 = newRef('https://www.aljazeera.com/news/2023/3/9/georgia-withdraws-foreign-agents-bill-after-days-of-protests', 'Georgia withdraws ‘foreign agents’ bill after days of protests');
$r6 = newRef('https://www.aljazeera.com/news/2023/3/10/georgias-parliament-drops-foreign-agents-bill', 'Georgia’s parliament drops controversial ‘foreign agents’ bill');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<h3>Overview</h3>

	<p>Georgia applied for EU membership together with Ukraine and Moldova days after Russia invaded Ukraine in February 2022.</p>


	<h3>Protests against ‘Foreign agents’ law</h3>

	<p>In March 2023, the parliament of Georgia was reviewing a bill “On Transparency of Foreign Influence”,
	stipulating that media outlets and NGOs could be classified as “foreign agents”
	if they receive more than 20 percent of their funding from abroad.</p>

	<p>The law is curtailing freedom of the press.
	Critics have pointed out the similarities between such a proposed law and a law enacted in 2012 in Russia,
	used to shut down or discredit organisations critical of the government.
	The law could also harm Georgia’s chances of EU membership.</p>

	<p>In Georgia, a large share of the media is controlled by the government.
	A large part of independent media gets outside support.</p>

	<p>Thousands of protesters gathered in Tbilisi in opposition of the proposed law.
	Police forces used water cannons and tear gas against the protesters, and arrested hundreds.</p>

	<p>After three days of protests, Georgia’s governing party announced it would withdraw the bill
	which was subsequently voted down by the parliament in the second reading.
	See ${r1} ${r2} ${r3} ${r4} ${r5} ${r6}.</p>
	HTML;


$div_wikipedia_2023_georgian_protests = newSection('wikipedia', 'https://en.wikipedia.org/wiki/2023_Georgian_protests', '2023 Georgian protests');
$div_wikipedia_2023_georgian_protests['stars']   = -1;
$div_wikipedia_2023_georgian_protests['class'][] = '';
$div_wikipedia_2023_georgian_protests['en'] = <<<HTML
	<p>The 2023 Georgian protests were a series of street demonstrations taking place throughout Georgia
	over parliamentary backing of a proposed "Law on Transparency of Foreign Influence",
	which requires NGOs to register as "agents of foreign influence" if the funds they receive from abroad amount to more than 20% of their total revenue.</p>
	HTML;


$div_wikipedia_history = newSection('wikipedia',
                         'https://en.wikipedia.org/wiki/History_of_Georgia_(country)#Independent_Georgia',
			 'History of Georgia');
$div_wikipedia_history['stars']   = -1;
$div_wikipedia_history['class'][] = '';
$div_wikipedia_history['en'] = <<<HTML
	<p>Georgia declared its independence from the Soviet Union in 1991.</p>
	HTML;


$div_wikipedia_politics = newSection('wikipedia',
                          'https://en.wikipedia.org/wiki/Politics_of_Georgia_(country)',
			  'Politics of Georgia (country)');
$div_wikipedia_politics['stars']   = -1;
$div_wikipedia_politics['class'][] = '';
$div_wikipedia_politics['en'] = <<<HTML
	<p>Georgia is parliamentary representative democratic republic with a multi-party system.</p>
	HTML;


$div_wikipedia_corruption = newSection('wikipedia',
                            'https://en.wikipedia.org/wiki/Corruption_in_Georgia',
			    'Corruption in Georgia');
$div_wikipedia_corruption['stars']   = -1;
$div_wikipedia_corruption['class'][] = '';
$div_wikipedia_corruption['en'] = <<<HTML
	<p>Before the 2003 Rose Revolution, according to Foreign Policy,
	Georgia was among the most corrupt nations in Eurasia.
	After the revolution, the level of corruption abated dramatically.
	In 2010, Transparency International said that
	Georgia was "the best corruption-buster in the world."</p>
	HTML;


$div_wikipedia_human_rights = newSection('wikipedia',
                              'https://en.wikipedia.org/wiki/Human_rights_in_Georgia_(country)',
			      'Human rights in Georgia');
$div_wikipedia_human_rights['stars']   = -1;
$div_wikipedia_human_rights['class'][] = '';
$div_wikipedia_human_rights['en'] = <<<HTML
	<p>Human rights in Georgia are guaranteed by the country's constitution.
	However, it has been alleged by Amnesty International, Rights Watch,
	the United States Department of State and the Georgian opposition
	that these rights are often breached.</p>
	HTML;



$body .= printSection($div_section_democracy_world);
$body .= printSection($div_stub);
$body .= printCountryIndices('Georgia');

$body .= printH2($h2_introduction);
$body .= printSection($div_introduction);

$body .= printSection($div_wikipedia_history);
$body .= printSection($div_wikipedia_politics);
$body .= printSection($div_wikipedia_corruption);
$body .= printSection($div_wikipedia_human_rights);
$body .= printSection($div_wikipedia_2023_georgian_protests);




include('include/page.php');
