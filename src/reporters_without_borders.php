<?php
include_once('section/media.php');
$h1['en'] = 'Reporters Without Borders';

$snp['description'] = "Reporters without borders, for press freedom";
$snp['image'] = "/copyrighted/Reporters_Without_Borders.1200-630.png";


$div_rwb_official_website = newSection();
$div_rwb_official_website['stars']   = -1;
$div_rwb_official_website['class'][] = '';
$div_rwb_official_website['en'] = <<<HTML
	<h3><a href="https://rsf.org/en">Reporters Without Borders</a></h3>

	<p>The organization's official website.</p>
	HTML;


$h2_rwb_press_freedom_index = newH2();
$h2_rwb_press_freedom_index['en'] = 'Press Freedom Index';
$h2_rwb_press_freedom_index['id'] = 'press-freedom-index';

$div_rwb_press_freedom_index = newSection();
$div_rwb_press_freedom_index['stars']   = -1;
$div_rwb_press_freedom_index['class'][] = '';
$div_rwb_press_freedom_index['en'] = <<<HTML
	<p>The Press Freedom Index is an annual ranking of countries compiled and published by Reporters Without Borders.
	The score and ranking of each country as calculated by Reporters Without Borders
	is included in our own country profiles.</p>

	<p>See the full data in their website:</p>

	<ul>
		<li><a href="https://rsf.org/en/index?year=2022">Press Freedom Index (2022)</a></li>
	</ul>
	HTML;


$div_wikipedia_reporters_without_border = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Reporters_Without_Borders', 'Reporters Without Borders');
$div_wikipedia_reporters_without_border['stars']   = -1;
$div_wikipedia_reporters_without_border['class'][] = '';
$div_wikipedia_reporters_without_border['en'] = <<<HTML
	<p>Reporters Without Borders is an international non-profit and non-governmental organization
	with the stated aim of safeguarding the right to freedom of information.
	It describes its advocacy as founded on the belief that everyone requires access to the news and information,
	in line with Article 19 of the Universal Declaration of Human Rights that recognizes the right to receive and share information regardless of frontiers.</p>
	HTML;

$div_wikipedia_press_freedom_index = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Press_Freedom_Index', 'Press Freedom Index');
$div_wikipedia_press_freedom_index['stars']   = -1;
$div_wikipedia_press_freedom_index['class'][] = '';
$div_wikipedia_press_freedom_index['en'] = <<<HTML
	<p>The Press Freedom Index is an annual ranking of countries compiled and published by Reporters Without Borders since 2002
	based upon the organisation's own assessment of the countries' press freedom records in the previous year.
	Reporters Without Borders is careful to note that the index only deals with press freedom
	and does not measure the quality of journalism in the countries it assesses,
	nor does it look at human rights violations in general.</p>
	HTML;

$div_wikipedia_the_uncensored_library = newSection('wikipedia', 'https://en.wikipedia.org/wiki/The_Uncensored_Library', 'The Uncensored Library');
$div_wikipedia_the_uncensored_library['stars']   = -1;
$div_wikipedia_the_uncensored_library['class'][] = '';
$div_wikipedia_the_uncensored_library['en'] = <<<HTML
	<p>The Uncensored Library is a Minecraft server and map	released by Reporters Without Borders
	as an attempt to circumvent censorship in countries without freedom of the press.
	The library contains banned reporting from Mexico, Russia, Vietnam, Saudi Arabia, and Egypt.</p>
	HTML;


$body .= printSection($div_stub);

$body .= printSection($div_rwb_official_website);
$body .= printSection($div_wikipedia_reporters_without_border);

$body .= printH2($h2_rwb_press_freedom_index);
$body .= printSection($div_rwb_press_freedom_index);
$body .= printSection($div_wikipedia_press_freedom_index);

$body .= printSection($div_wikipedia_the_uncensored_library);


include('include/page.php');
