<?php
include_once('section/project.php');
$h1['en'] = 'List of movies';


$snp['description'] = "Some movies worth watching...";
//$snp['image'] = "/copyrighted/";


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Some movies which are related to the themes discussed in the $Pilgrimage.</p>
	HTML;


$div_codeberg_List_of_documentaries_and_movies = new CodebergContentSection();
$div_codeberg_List_of_documentaries_and_movies->setTitleText('List of documentaries and movies');
$div_codeberg_List_of_documentaries_and_movies->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/3');
$div_codeberg_List_of_documentaries_and_movies->content = <<<HTML
	<p>list of relevant documentaries and movies.</p>
	HTML;



$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>list</h3>

	<ul>
		<li>${'The Remains of the Day'}</li>
	</ul>
	HTML;


$body .= printPageSection('lists.html');

$body .= $div_introduction->print();
$body .= $div_codeberg_List_of_documentaries_and_movies->print();
$body .= $div_list->print();

// Featured
$body .= printPageSection('the_remains_of_the_day.html');


include('include/page.php');
