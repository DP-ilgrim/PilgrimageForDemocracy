<?php
include_once('section/global_issues.php');
include_once('section/society.php');
include_once('section/features_of_democracy.php');
$h1['en'] = 'Global Issues';

$snp['description'] = "We live in an interconnected world, with issues affecting the whole of humanity.";
$snp['image'] = "/copyrighted/ben-white-gEKMstKfZ6w.1200-630.jpg";


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	We live in an interconnected world, with issues affecting the whole of humanity.
	HTML;


$body .= printSection($div_stub);
$body .= printSection($div_introduction);

$body .= printSection($div_section_hunger);
$body .= printSection($div_section_global_natural_resources);
$body .= printSection($div_section_peaceful_opposition_war);
$body .= printSection($div_section_living_together);
$body .= printSection($div_section_what_is_democracy_secondary_feature);


include('include/page.php');
