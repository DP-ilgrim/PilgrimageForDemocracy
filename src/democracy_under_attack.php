<?php
include_once('section/threats.php');
$h1['en'] = 'Democracy under attack';



//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>Democracy is under attack:</p>
	<ul>
	<li>Russia's war in Ukraine</li>
	<li>Cyber warfare</li>
	<li>The People's Republic of China against Taiwan (Republic of China)</li>
	<li>Transnational Repression</li>
	<li>Foreign interference in elections</li>
	<li>From within: greed, indifference and lack of solidarity</li>
	</ul>
	HTML;



$body .= printSection($div_stub);

$body .= printSection($div_introduction);

$body .= printPageSection('transnational_authoritarianism.html');
$body .= printPageSection('cyberwarfare.html');

include('include/page.php');
