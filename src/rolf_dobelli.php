<?php
include_once('section/people.php');
$h1['en'] = 'Rolf Dobelli';


//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

$r1 = newRef('https://www.theguardian.com/media/2013/apr/12/news-is-bad-rolf-dobelli', 'News is bad for you – and giving up reading it will make you happier');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Rolf Dobelli is the author of "${'The Art of Thinking Clearly'}" and of "Stop Reading the News".</p>

	<p>In his books, Dobelli argues that: ${r1}</p>
	<ul>
		<li>News misleads.</li>
		<li>News is irrelevant.</li>
		<li>News has no explanatory power.</li>
		<li>News is toxic to your body.</li>
		<li>News increases cognitive errors.</li>
		<li>News inhibits thinking.</li>
		<li>News works like a drug.</li>
		<li>News wastes time.</li>
		<li>News makes us passive.</li>
		<li>News kills creativity.</li>
	</ul>
	HTML;



$div_Dobelli_website = new WebsiteContentSection();
$div_Dobelli_website->setTitleText("Dobelli's website ");
$div_Dobelli_website->setTitleLink('http://www.dobelli.com/en/');



$div_wikipedia_Rolf_Dobelli = new WikipediaContentSection();
$div_wikipedia_Rolf_Dobelli->setTitleText('Rolf Dobelli');
$div_wikipedia_Rolf_Dobelli->setTitleLink('https://en.wikipedia.org/wiki/Rolf_Dobelli');
$div_wikipedia_Rolf_Dobelli->content = <<<HTML
	<p>Rolf Dobelli is a Swiss author and entrepreneur.</p>
	HTML;

$body .= printPageSection('list_of_people.html');
$body .= printSection($div_stub);

$body .= $div_introduction->print();

$body .= $div_Dobelli_website->print();
$body .= $div_wikipedia_Rolf_Dobelli->print();

$body .= printPageSection('information_overload.html');


include('include/page.php');
