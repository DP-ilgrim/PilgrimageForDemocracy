<?php
include_once('section/features_of_democracy.php');
$h1['en'] = 'Tertiary feature of democracy &mdash; the Professionals';


//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('', '');


$h2_Making_it_work = newH2();
$h2_Making_it_work['en'] = 'Making it work';


$div_What_side_to_drive_in_England = newSection();
$div_What_side_to_drive_in_England['stars']   = -1;
$div_What_side_to_drive_in_England['class'][] = '';
$div_What_side_to_drive_in_England['en'] = <<<HTML
	<h3>What side to drive in England?</h3>
	HTML;




$h2_institutions = newH2();
$h2_institutions['en'] = 'Institutions';

$div_institutions = newSection();
$div_institutions['stars']   = -1;
$div_institutions['class'][] = '';
$div_institutions['en'] = <<<HTML
	<h3></h3>
	<p>
	</p>
	HTML;


$h2_professionals = newH2();
$h2_professionals['en'] = 'We, the Professionals';

$div_professionals = newSection();
$div_professionals['stars']   = -1;
$div_professionals['class'][] = '';
$div_professionals['en'] = <<<HTML
	<h3></h3>
	<p>
	</p>
	HTML;


$body .= printSection($div_section_what_is_democracy_primary_feature);
$body .= printSection($div_section_what_is_democracy_secondary_feature);

$body .= printH2($h2_Making_it_work);
$body .= printSection($div_What_side_to_drive_in_England);

$body .= printH2($h2_institutions);
$body .= printSection($div_institutions);

$body .= printH2($h2_professionals);
$body .= printSection($div_professionals);

$body .= printSection($div_section_what_is_democracy_quaternary_feature);

include('include/page.php');
