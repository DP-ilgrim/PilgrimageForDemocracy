<?php
include_once('section/books.php');
$h1['en'] = 'Twilight of Democracy';


$snp['description'] = "The Seductive Lure of Authoritarianism";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"<strong>Twilight of Democracy: The Seductive Lure of Authoritarianism</strong>
	is a 2020 book by ${'Anne Applebaum'}.</p>
	HTML;


$div_media_appearances = new ContentSection();
$div_media_appearances->content = <<<HTML
	<h3>Media appearances</h3>

	<p>Anne Applebaum made numerous media appearances to talk about her books.
	Here is a selected list of videos where Applebaum is a guest speaker:</p>

	<ul>
		<li>National Endowment for Democracy:
			<a href="https://www.youtube.com/watch?v=d8HDBQsmb7Y&ab_channel=NationalEndowmentforDemocracy">
			Lecture: "Autocracy Inc: How the World's Authoritarians Work Together" | 19th Annual Lipset Lecture</a></li>
		<li>University of Richmond:
			<a href="https://www.youtube.com/watch?v=NFKkFStIyjs&ab_channel=UniversityofRichmond">
			Autocracy and Democracy in an Era of Nationalism</a></li>
		<li>Johns Hopkins University School of Advanced International Studies (SAIS):
			<a href="https://www.youtube.com/watch?v=tdQfl3YVOuI&ab_channel=SAISEvents">
			Twilight of Democracy A Conversation with Author and Professor Anne Applebaum</a></li>
		<li>Ford School of Public Policy:
			<a href="https://www.youtube.com/watch?v=aRo5tNQVimM&ab_channel=FordSchoolofPublicPolicy">
			Anne Applebaum: Democracy in Crisis: The twilight of democracy</a></li>
		<li>The German Marshall Fund of the United States:
			<a href="https://www.youtube.com/watch?v=-7UpD39fvmk&ab_channel=TheGermanMarshallFundoftheUnitedStates">
			Twilight of Democracy: The Seductive Lure of Authoritarianism – A Book Talk with Anne Applebaum</a></li>
	</ul>
	HTML;


$div_wikipedia_Twilight_of_Democracy = new WikipediaContentSection();
$div_wikipedia_Twilight_of_Democracy->setTitleText('Twilight of Democracy');
$div_wikipedia_Twilight_of_Democracy->setTitleLink('https://en.wikipedia.org/wiki/Twilight_of_Democracy');
$div_wikipedia_Twilight_of_Democracy->content = <<<HTML
	<p>"Twilight of Democracy: The Seductive Lure of Authoritarianism is a 2020 book by Anne Applebaum
	that discusses democratic decline and the rise of right-wing populist politics with authoritarian tendencies,
	with three main case studies: Poland, the United Kingdom and the United States. The book also includes a discussion of Hungary.</p>
	HTML;

$body .= printPageSection('list_of_books.html');
$body .= printSection($div_stub);

$body .= $div_introduction->print();
$body .= $div_media_appearances->print();

$body .= printPageSection('anne_applebaum.html');
$body .= $div_wikipedia_Twilight_of_Democracy->print();


include('include/page.php');
