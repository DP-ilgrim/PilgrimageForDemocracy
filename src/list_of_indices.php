<?php
include_once('section/project.php');
$h1['en'] = 'list of indices';


$snp['description'] = "Indices about democracy, peace, freedom, justice, etc.";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Here are all kinds of rating and indices about democracy, peace, freedom, justice etc.
	published by a variety of organisations.</p>
	HTML;

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>List</h3>

	<ul>
	<li>${'Freedom House'}: for each country in the $world, we display the Freedom Index and the Internet freedom index published by the Freedom House.</li>
	<li>${'Global Peace Index'}, published by the ${'Institute for Economics and Peace'}.</li>
	</ul>
	HTML;



$div_wikipedia_Democracy_indices = new WikipediaContentSection();
$div_wikipedia_Democracy_indices->setTitleText('Democracy indices');
$div_wikipedia_Democracy_indices->setTitleLink('https://en.wikipedia.org/wiki/Democracy_indices');
$div_wikipedia_Democracy_indices->content = <<<HTML
	<p>Democracy indices are quantitative and comparative assessments of the state of democracy
	for different countries according to various definitions of democracy.</p>
	HTML;

$div_wikipedia_List_of_freedom_indices = new WikipediaContentSection();
$div_wikipedia_List_of_freedom_indices->setTitleText('List of freedom indices');
$div_wikipedia_List_of_freedom_indices->setTitleLink('https://en.wikipedia.org/wiki/List_of_freedom_indices');
$div_wikipedia_List_of_freedom_indices->content = <<<HTML
	<p>This article contains a list of freedom indices produced by several non-governmental organizations
	that publish and maintain assessments of the state of freedom in the world, according to their own various definitions of the term.</p>
	HTML;

$body .= printPageSection('lists.html');

$body .= $div_introduction->print();
$body .= $div_list->print();

$body .= $div_wikipedia_Democracy_indices->print();
$body .= $div_wikipedia_List_of_freedom_indices->print();


include('include/page.php');
