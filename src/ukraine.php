<?php
include_once('section/world.php');
$h1['en'] = 'Ukraine';

$snp['description'] = "The front line of the war for democracy.";
$snp['image'] = "/copyrighted/marjan-blan-UDdkJlfn7cU.1200-630.jpg";

$r1 = newRef('https://kyivindependent.com/judicial-reform-has-mixed-results-as-good-candidates-are-vetoed-without-clear-reasons/',
             "Ukraine's judicial reform has mixed reviews as it nears key point");

$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>Ukraine is literally the front line of the war for democracy.
	All democratic countries should continue supporting Ukraine in every possible way
	until the Russian invader is completely defeated.</p>
	HTML;


$h2_Democracy_vs_Totalitarianism = newH2();
$h2_Democracy_vs_Totalitarianism['en'] = 'Democracy vs Totalitarianism';


$div_democracy_vs_totalitarianism = newSection();
$div_democracy_vs_totalitarianism['stars']   = -1;
$div_democracy_vs_totalitarianism['class'][] = '';
$div_democracy_vs_totalitarianism['en'] = <<<HTML
	<p>With the Russian full scale invasion of Ukraine,
	together with the rise in power of the People's Republic of China and its increasingly aggressive assertiveness,
	this decade is marked by a direct confrontation between democracies and totalitarian regimes.</p>

	<p>Ukraine must win the war and Putin's regime must be thoroughly defeated.</p>
	HTML;



$h2_long_road_towards_independence_and_democracy = newH2();
$h2_long_road_towards_independence_and_democracy['en'] = "Ukraine's journey to independence and democracy";

$div_road_to_democracy = newSection();
$div_road_to_democracy['stars']   = -1;
$div_road_to_democracy['class'][] = '';
$div_road_to_democracy['en'] = <<<HTML
	<p>Ukraine's road towards independence and democracy has been very long and very painful,
	and Ukraine still has to defeat its most terrible enemy, the present day Russian Federation.</p>

	HTML;

$h2_Post_war_democracy = newH2();
$h2_Post_war_democracy['en'] = 'Post-war democracy';


$div_democratic_reforms = newSection();
$div_democratic_reforms['stars']   = -1;
$div_democratic_reforms['class'][] = '';
$div_democratic_reforms['en'] = <<<HTML
	<p>While we hope that Ukraine will quickly be able to defeat Russia and recover its 1991 borders,
	its democratization process will not end with a victory in the ongoing war.
	Already today, aside from the military confrontation, reformers are struggling to define
	the democratic nature of post-war Ukraine.</p>
	HTML;


$div_ukraine_NATO = newSection();
$div_ukraine_NATO['stars']   = -1;
$div_ukraine_NATO['class'][] = '';
$div_ukraine_NATO['en'] = <<<HTML
	<h3>Ukraine to join NATO</h3>

	<p>At the very beginning of Russia's full scale invasion,
	it was almost inconceivable that Ukraine would join NATO.
	At some stage, president Zelensky himself conceded that they had to give up such hope in order to secure peace.
	However, the subsequent events on the battlefield, and the scope of the crimes committed by Russia,
	dramatically changed the conditions.
	Today, not many people doubt that Ukraine will eventually join NATO,
	although it is clear that it shall happen only after the war.</p>

	<p>A few short years ago, NATO had almost lost its raison d'être.
	Today, NATO can be seen as the armed forces of democratic countries.
	Ukraine joining NATO will be beneficial for all.
	First, at long last, Ukraine's peace and military protection shall be assured.
	Also, NATO shall gain a new, powerful democratic ally.</p>
	HTML;

$div_Ukraine_European_Union = newSection();
$div_Ukraine_European_Union['stars']   = -1;
$div_Ukraine_European_Union['class'][] = '';
$div_Ukraine_European_Union['en'] = <<<HTML
	<h3>Ukraine to join the European Union</h3>

	<p>Ukraine shall join the European Union as certainly it will NATO.</p>

	<p>Ukraine is in the process of conducting reforms that are required to be admitted in the EU.</p>
	HTML;

$div_Judicial_reform = newSection();
$div_Judicial_reform['stars']   = -1;
$div_Judicial_reform['class'][] = '';
$div_Judicial_reform['en'] = <<<HTML
	<h3>Judicial reform</h3>

	<p>Right in the midst of a brutal war against Russia,
	Ukraine is battling on another front:
	its war against corruption.</p>

	<p>Judicial reforms are urgently needed; they have been ongoing for years, and the process has suffered many setbacks</p>

	<p>Among existing problems are: ${r1}</p>
	<ul>
	<li>The head of the Supreme Court, Vsevolod Kniaziev, being charged with bribery.</li>
	<li>Trusted and independent candidates have been excluded from competitions for key judicial posts, while tainted ones have been green-lighted.</li>
	<li>The independence of the judiciary from the executive branch is not yet fully established.</li>
	</ul>
	HTML;



$div_wikipedia_Russo_Ukrainian_War = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Russo-Ukrainian_War', 'Russo Ukrainian War');
$div_wikipedia_Russo_Ukrainian_War['stars']   = -1;
$div_wikipedia_Russo_Ukrainian_War['class'][] = '';
$div_wikipedia_Russo_Ukrainian_War['en'] = <<<HTML
	<p>This article is about the war ongoing since 2014.</p>
	HTML;

$div_wikipedia_Holodomor = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Holodomor', 'Holodomor');
$div_wikipedia_Holodomor['stars']   = -1;
$div_wikipedia_Holodomor['class'][] = '';
$div_wikipedia_Holodomor['en'] = <<<HTML
	<p>The Holodomor also known as the Terror-Famine or the Great Famine,
	was a man-made famine in Soviet Ukraine from 1932 to 1933 that killed millions of Ukrainians.</p>
	HTML;

$div_wikipedia_Orange_Revolution = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Orange_Revolution', 'Orange Revolution');
$div_wikipedia_Orange_Revolution['stars']   = -1;
$div_wikipedia_Orange_Revolution['class'][] = '';
$div_wikipedia_Orange_Revolution['en'] = <<<HTML
	<p>The Orange Revolution was a series of protests and political events that took place in Ukraine from late November 2004 to January 2005,
	in the immediate aftermath of the run-off vote of the 2004 Ukrainian presidential election,
	which was claimed to be marred by massive corruption, voter intimidation and electoral fraud.</p>
	HTML;

$div_wikipedia_Revolution_of_Dignity = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Revolution_of_Dignity', 'Revolution of Dignity');
$div_wikipedia_Revolution_of_Dignity['stars']   = -1;
$div_wikipedia_Revolution_of_Dignity['class'][] = '';
$div_wikipedia_Revolution_of_Dignity['en'] = <<<HTML
	<p>The Revolution of Dignity, also known as the Maidan Revolution or the Ukrainian Revolution,
	took place in Ukraine in February 2014 at the end of the Euromaidan protests,
	when deadly clashes between protesters and state forces in the capital Kyiv
	culminated in the ousting of elected President Viktor Yanukovych and a return to the 2004 Constitution.
	It also led to the outbreak of the Russo-Ukrainian War.</p>
	HTML;

$div_wikipedia_Judiciary_of_Ukraine = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Judiciary_of_Ukraine', 'Judiciary of Ukraine');
$div_wikipedia_Judiciary_of_Ukraine['stars']   = -1;
$div_wikipedia_Judiciary_of_Ukraine['class'][] = '';
$div_wikipedia_Judiciary_of_Ukraine['en'] = <<<HTML
	<p>Although judicial independence exists in principle,
	in practice there is little separation of juridical and political powers.
	Judges are subjected to pressure by political and business interests.
	Ukraine's court system is widely regarded as corrupt.</p>
	HTML;

$div_codeberg = newSection('codeberg', '32', 'Ukraine: road to democracy, and post-war democratic reforms');
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>Provide a short outline of Ukraine's road to independence and democracy,
	and document the democratic reforms that Ukraine will need to continue after its victory (hopefully soon) over Russia.</p>
	HTML;


$body .= printSection($div_section_democracy_world);
$body .= printSection($div_stub);
$body .= printCountryIndices('Ukraine');
$body .= printSection($div_introduction);
$body .= printSection($div_codeberg);


$body .= printH2($h2_Democracy_vs_Totalitarianism);
$body .= printSection($div_democracy_vs_totalitarianism);
$body .= printSection($div_wikipedia_Russo_Ukrainian_War);

$body .= printPageSection('taiwan_ukraine_relations.html');



$body .= printH2($h2_long_road_towards_independence_and_democracy);
$body .= printSection($div_road_to_democracy);
$body .= printSection($div_wikipedia_Holodomor);
$body .= printSection($div_wikipedia_Orange_Revolution);
$body .= printSection($div_wikipedia_Revolution_of_Dignity);


$body .= printH2($h2_Post_war_democracy);
$body .= printSection($div_democratic_reforms);
$body .= printSection($div_ukraine_NATO);
$body .= printSection($div_Ukraine_European_Union);
$body .= printSection($div_Judicial_reform);
$body .= printSection($div_wikipedia_Judiciary_of_Ukraine);


include('include/page.php');
