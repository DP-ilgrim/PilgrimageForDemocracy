<?php
include_once('section/taxes.php');
$h1['en'] = 'Taxes';

//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>It is said that the two things that one cannot avoid in life are: death, and taxes.</p>

	<p>In this section, we shall explore:
	the definition of taxes,
	the necessity of taxes,
	the amount of taxes,
	and the nature, good or bad, of taxes.</p>

	<p>The government needs the financial resources to perform its duties. Taxes are obviously necessary. However, not all taxes are created equal.</p>

	<p>What criteria can we use to evaluate the usefulness of a tax,
	and its contribution to running a balanced society?
	What taxes are harmful and should be abolished?
	What taxes would, overall, be more beneficial?
	What proportions do harmful taxes take in a government's budget, as compared to more benign taxes?</p>
	HTML;


$body .= printSection($div_stub);
$body .= printSection($div_introduction);


include('include/page.php');
