<?php
include_once('section/threats.php');
$h1['en'] = 'Cyberwarfare';



//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Cyberwarfare = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Cyberwarfare', 'Cyberwarfare');
$div_wikipedia_Cyberwarfare['stars']   = -1;
$div_wikipedia_Cyberwarfare['class'][] = '';
$div_wikipedia_Cyberwarfare['en'] = <<<HTML
	<p>Many countries including the United States, United Kingdom, Russia, China, Israel, Iran, and North Korea
	have active cyber capabilities for offensive and defensive operations.
	As states explore the use of cyber operations and combine capabilities, the likelihood of physical confrontation
	and violence playing out as a result of, or part of, a cyber operation is increased.</p>
	HTML;

$div_wikipedia_Cyberwarfare_by_China = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Cyberwarfare_by_China', 'Cyberwarfare by China');
$div_wikipedia_Cyberwarfare_by_China['stars']   = -1;
$div_wikipedia_Cyberwarfare_by_China['class'][] = '';
$div_wikipedia_Cyberwarfare_by_China['en'] = <<<HTML
	<p>In 2017, Foreign Policy provided an estimated range for China's "hacker army" personnel, anywhere from 50,000 to 100,000 individuals.</p>
	HTML;

$div_wikipedia_Cyberwarfare_by_Russia = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Cyberwarfare_by_Russia', 'Cyberwarfare by Russia');
$div_wikipedia_Cyberwarfare_by_Russia['stars']   = -1;
$div_wikipedia_Cyberwarfare_by_Russia['class'][] = '';
$div_wikipedia_Cyberwarfare_by_Russia['en'] = <<<HTML
	<p>Cyberwarfare by Russia includes denial of service attacks, hacker attacks, dissemination of disinformation and propaganda,
	participation of state-sponsored teams in political blogs, internet surveillance using SORM technology, persecution of cyber-dissidents and other active measures.</p>
	HTML;



$body .= printPageSection('democracy_under_attack.html');
$body .= printSection($div_stub);
$body .= printSection($div_introduction);

$body .= printSection($div_wikipedia_Cyberwarfare);
$body .= printSection($div_wikipedia_Cyberwarfare_by_China);
$body .= printSection($div_wikipedia_Cyberwarfare_by_Russia);


include('include/page.php');
