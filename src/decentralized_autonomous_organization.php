<?php
include_once('section/democracy.php');
$h1['en'] = 'decentralized autonomous organization';


$snp['description'] = "a blockchain-based democratic decision-making process";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Decentralized_autonomous_organization = new WikipediaContentSection();
$div_wikipedia_Decentralized_autonomous_organization->setTitleText('Decentralized autonomous organization');
$div_wikipedia_Decentralized_autonomous_organization->setTitleLink('https://en.wikipedia.org/wiki/Decentralized_autonomous_organization');
$div_wikipedia_Decentralized_autonomous_organization->content = <<<HTML
	<p>A decentralized autonomous organization (DAO), sometimes called a decentralized autonomous corporation (DAC),
	is an organization managed in whole or in part by decentralized computer program, with voting and finances handled through a blockchain.
	In general terms, DAOs are member-owned communities without centralized leadership.</p>
	HTML;

$body .= printSection($div_stub);

$body .= $div_introduction->print();
$body .= $div_wikipedia_Decentralized_autonomous_organization->print();
$body .= printPageSection('types_of_democracy.html');


include('include/page.php');
