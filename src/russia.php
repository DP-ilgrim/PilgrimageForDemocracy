<?php
include_once('section/world.php');
$h1['en'] = 'Russia';

$snp['description'] = "Russia, a threat to global peace and democracy.";
$snp['image'] = "/copyrighted/natalya-letunova-DLclPZyS_bs.1200-630.jpg";


$div_codeberg = newSection('codeberg', '27', 'Improve the article on Russia');
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>What should be our priority to develop this article about Russia?</p>
	HTML;

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Vladimir Putin changed the $constitution of Russia so that he could remain in power for life.</p>

	<p>Putin's Russia is a major <a href="/democracy_under_attack.html">threat to democracy</a>
	and is waging a war on $Ukraine.</p>
	HTML;


$div_wikipedia_human_rights_russia = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Human_rights_in_Russia', 'Human rights in Russia');
$div_wikipedia_human_rights_russia['stars']   = -1;
$div_wikipedia_human_rights_russia['class'][] = '';
$div_wikipedia_human_rights_russia['en'] = <<<HTML
	<p>Human rights violations in Russia have routinely been criticized
	by international organizations and independent domestic media outlets.</p>
	HTML;

$div_wikipedia_russian_war_crimes = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Russian_war_crimes', 'Russian war crimes');
$div_wikipedia_russian_war_crimes['stars']   = -1;
$div_wikipedia_russian_war_crimes['class'][] = '';
$div_wikipedia_russian_war_crimes['en'] = <<<HTML
	<p>Russian war crimes since 1991 are the violations of the law of war,
	including the Hague Conventions of 1899 and 1907 and the Geneva Conventions,
	consisting of war crimes, crimes against humanity, and the crime of genocide,
	which the official armed and paramilitary forces of the Russian Federation
	have been accused of committing since the dissolution of the Soviet Union.</p>
	HTML;

$div_wikipedia_memorial_russia = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Memorial_(society)', 'Memorial (society)');
$div_wikipedia_memorial_russia['stars']   = -1;
$div_wikipedia_memorial_russia['class'][] = '';
$div_wikipedia_memorial_russia['en'] = <<<HTML
	<p>Memorial is an international human rights organisation,
	founded in Russia during the fall of the Soviet Union
	to study and examine the human rights violations and other crimes committed under Joseph Stalin's reign.</p>
	HTML;

$div_wikipedia_russian_war_crimes_ukraine = newSection('wikipedia', 'https://en.wikipedia.org/wiki/War_crimes_in_the_Russian_invasion_of_Ukraine', 'War crimes in the Russian invasion of Ukraine');
$div_wikipedia_russian_war_crimes_ukraine['stars']   = -1;
$div_wikipedia_russian_war_crimes_ukraine['class'][] = '';
$div_wikipedia_russian_war_crimes_ukraine['en'] = <<<HTML
	<p>Since the beginning of the Russian invasion of Ukraine in 2022,
	Russian authorities and armed forces have committed multiple war crimes
	in the form of deliberate attacks against civilian targets, massacres of civilians,
	torture and rape of women and children,[4][5] and indiscriminate attacks in densely populated areas.</p>
	HTML;

$div_wikipedia_russia_traumazone = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Russia_1985%E2%80%931999:_TraumaZone', 'Russia 1985–1999: TraumaZone');
$div_wikipedia_russia_traumazone['stars']   = -1;
$div_wikipedia_russia_traumazone['class'][] = '';
$div_wikipedia_russia_traumazone['en'] = <<<HTML
	<p>Using stock footage shot by the BBC,
	the series chronicles the collapse of the Soviet Union, the rise of capitalist Russia and its oligarchs,
	and the effects of this on Russian people of all levels of society, leading to the rise to power of Vladimir Putin.</p>
	HTML;


$div_wikipedia_ICC_russia_ukraine = newSection('wikipedia', 'https://en.wikipedia.org/wiki/International_Criminal_Court_investigation_in_Ukraine',
                                                            'International Criminal Court investigation in Ukraine');
$div_wikipedia_ICC_russia_ukraine['stars']   = -1;
$div_wikipedia_ICC_russia_ukraine['class'][] = '';
$div_wikipedia_ICC_russia_ukraine['en'] = <<<HTML
	<p>The International Criminal Court investigation in Ukraine
	is an ongoing investigation by the Prosecutor of the International Criminal Court (ICC)
	into war crimes and crimes against humanity that may have occurred since 21 November 2013,
	during the Russo-Ukrainian War, including the 2014 annexation of Crimea by Russia, the war in Donbas and the 2022 Russian invasion of Ukraine.</p>
	HTML;

$div_wikipedia_legality_russian_war_ukraine = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Legality_of_the_Russian_invasion_of_Ukraine',
                                                                      'Legality of the Russian invasion of Ukraine');
$div_wikipedia_legality_russian_war_ukraine['stars']   = -1;
$div_wikipedia_legality_russian_war_ukraine['class'][] = '';
$div_wikipedia_legality_russian_war_ukraine['en'] = <<<HTML
	<p>The Russian invasion of Ukraine violated international law (including the Charter of the United Nations).
	The invasion has also been called a crime of aggression under international criminal law.</p>
	HTML;

$div_wikipedia_censorship_in_russia = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Censorship_in_the_Russian_Federation', 'Censorship in the Russian Federation');
$div_wikipedia_censorship_in_russia['stars']   = -1;
$div_wikipedia_censorship_in_russia['class'][] = '';
$div_wikipedia_censorship_in_russia['en'] = <<<HTML
	<p>Censorship is controlled by the Russian government and by civil society in the Russian Federation,
	applying to the content and the diffusion of information
	with the aim of limiting or preventing the dissemination of ideas and information that the Russian state considers to be a danger.</p>
	HTML;



$body .= printSection($div_section_democracy_world);
$body .= printSection($div_stub);
$body .= printSection($div_codeberg);
$body .= $div_introduction->print();
$body .= printCountryIndices('Russia');

$body .= printSection($div_wikipedia_human_rights_russia);
$body .= printSection($div_wikipedia_censorship_in_russia);
$body .= printSection($div_wikipedia_russian_war_crimes);
$body .= printSection($div_wikipedia_russian_war_crimes_ukraine);
$body .= printSection($div_wikipedia_ICC_russia_ukraine);
$body .= printSection($div_wikipedia_legality_russian_war_ukraine);
$body .= printSection($div_wikipedia_russia_traumazone);
$body .= printSection($div_wikipedia_memorial_russia);


include('include/page.php');
