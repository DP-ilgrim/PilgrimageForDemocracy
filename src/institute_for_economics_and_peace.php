<?php
include_once('section/institutions.php');
$h1['en'] = 'Institute for Economics and Peace';


$snp['description'] = "Creating a paradigm shift in the way the world thinks about peace.";
//$snp['image'] = "/copyrighted/";

$r1 = newRef('https://www.economicsandpeace.org/about/', 'About the Institute for Economics and Peace');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Institute for Economics and Peace aims to create a paradigm shift in the way the world thinks about peace.
	They do this by developing global and national indices, calculating the economic cost of violence,
	analysing country level risk and fragility, and understanding Positive Peace. ${r1}</p>

	<p>The IEP publishes the ${'Global Peace Index'}.</p>
	HTML;



$div_Institute_for_Economics_and_Peace = new WebsiteContentSection();
$div_Institute_for_Economics_and_Peace->setTitleText('Institute for Economics and Peace ');
$div_Institute_for_Economics_and_Peace->setTitleLink('https://www.economicsandpeace.org/');
$div_Institute_for_Economics_and_Peace->content = <<<HTML
	<p>IEP website.</p>
	HTML;



$div_wikipedia_Institute_for_Economics_and_Peace = new WikipediaContentSection();
$div_wikipedia_Institute_for_Economics_and_Peace->setTitleText('Institute for Economics and Peace');
$div_wikipedia_Institute_for_Economics_and_Peace->setTitleLink('https://en.wikipedia.org/wiki/Institute_for_Economics_and_Peace');
$div_wikipedia_Institute_for_Economics_and_Peace->content = <<<HTML
	<p>The Institute for Economics and Peace (IEP) is a global think tank headquartered in Sydney, Australia.
	IEP studies the relationship between peace, business, and prosperity,
	and seeks to promote understanding of the cultural, economic, and political factors that drive peacefulness.</p>
	HTML;


$body .= printPageSection('list_of_organisations.html');
$body .= printSection($div_stub);

$body .= $div_introduction->print();

$body .= $div_Institute_for_Economics_and_Peace->print();
$body .= $div_wikipedia_Institute_for_Economics_and_Peace->print();


include('include/page.php');
