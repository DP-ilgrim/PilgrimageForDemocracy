<?php
include_once('section/world.php');
$h1['en'] = 'Costa Rica';

$snp['description'] = "A democratic gem in Central America";
$snp['image'] = "/copyrighted/chalo-garcia-pnmp3SOTROg.1200-630.jpg";

$r1 = newRef('https://rsf.org/en/index?year=2022', 'RSF Press Freedom Index 2022');
$r2 = newRef('https://www.aljazeera.com/news/2023/4/14/criminalising-journalism-famous-salvadoran-paper-to-relocate', '‘Criminalising journalism’: Famous Salvadoran outlet to relocate');

$div_codeberg = newSection('codeberg', '23', 'Costa Rica');
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>How does Costa Rica have the highest press freedom score in the Americas?
	And why is it so different to its neighbours, which have much lower scores?</p>
	HTML;



$h2_costa_rica_gem = newH2();
$h2_costa_rica_gem['en'] = 'Costa Rica - a democratic gem in the Americas';


$div_costa_rica_gem = newSection();
$div_costa_rica_gem['stars']   = -1;
$div_costa_rica_gem['class'][] = '';
$div_costa_rica_gem['en'] = <<<HTML
	<h3>Press freedom</h3>

	<p>In the 2022 Reporters Without Borders Press Freedom index,
	Costa Rica is ranked 8th country in the world, with a score of 85.92 ("Good").
	This excellent score is even more noteworthy because neighbouring countries
	Panama is ranked 74th with a score of 62.78 ("Difficult")
	and Nicaragua is ranked 160th with a score of 37.09 ("Very serious"). ${r1}</p>

	<p>Costa Rica is seen as a safe haven for journalism.
	A Salvadoran independent media company
	decided in 2023 to move its administrative and legal operations to Costa Rica,
	in order to escape from what they called "a campaign of government harassment" in El Salvador. ${r2}</p>
	HTML;


$div_costa_rica_history = newSection();
$div_costa_rica_history['stars']   = -1;
$div_costa_rica_history['class'][] = '';
$div_costa_rica_history['en'] = <<<HTML
	<h3>Costa Rican history</h3>

	<p>The following Costa Rica articles are of particular interest (Wikipedia):</p>

	<ul>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/History_of_Costa_Rica">History of Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Politics_of_Costa_Rica">Politics of Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Legislative_Assembly_of_Costa_Rica">Legislative Assembly of Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Foreign_relations_of_Costa_Rica">Foreign relations of Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Immigration_to_Costa_Rica">Immigration to Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Human_rights_in_Costa_Rica">Human rights in Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Prostitution_in_Costa_Rica">Prostitution in Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Human_trafficking_in_Costa_Rica">Human trafficking in Costa Rica</a></li>
	</ul>

	HTML;

$body .= printSection($div_section_democracy_world);
$body .= printSection($div_stub);
$body .= printSection($div_codeberg);
$body .= printCountryIndices('Costa Rica');
$body .= printSection($div_costa_rica_gem);
$body .= printSection($div_costa_rica_history);

include('include/page.php');
