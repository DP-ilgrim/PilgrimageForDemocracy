<?php
include_once('section/all.php');
$h1['en'] = 'Lists and topics';

$snp['description'] = "an entrance into the rabbit holein";
//$snp['image'] = "/copyrighted/";

$h2_Lists = new h2HeaderContent('Lists');


$div_lists = new ContentSection();
$div_lists->content = <<<HTML
	<p>List of lists.</p>
	HTML;


$h2_Topics = new h2HeaderContent('Topics');

$div_topics = new ContentSection();
$div_topics->content = <<<HTML
	<p>Here are the main topical entry pages.</p>
	HTML;




$body .= printPageSection('menu.html');

$body .= $h2_Lists->print();
$body .= $div_lists->print();
$body .= printPageSection('list_of_books.html');
$body .= printPageSection('list_of_indices.html');
$body .= printPageSection('list_of_movies.html');
$body .= printPageSection('list_of_people.html');
$body .= printPageSection('list_of_organisations.html');
$body .= printPageSection('world.html');

$body .= $h2_Topics->print();
$body .= $div_topics->print();
$body .= printPageSection('global_issues.html');
$body .= printPageSection('politics.html');
$body .= printPageSection('political_discourse.html');
$body .= printPageSection('democracy.html');
$body .= printPageSection('types_of_democracy.html');

include('include/page.php');
