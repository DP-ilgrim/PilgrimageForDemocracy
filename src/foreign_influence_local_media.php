<?php
include_once('section/media.php');
$h1['en'] = 'Foreign influence in local media landscape';

$snp['description'] = "Should foreign influence in local media be restricted?";
$snp['image'] = "/copyrighted/killian-cartignies-lJk004r27no.1200-630.jpg";


$h2_introduction = newH2();
$h2_introduction['en'] = 'Introduction';

$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>No country is exempt from foreign influence in their local media environment.
	As shown below, a few countries have enacted legislation in order to identify and monitor foreign agents.</p>

	<p>Russia has used its "<em>foreign agent law</em>" to further its autocratic agenda,
	for example against the organization and Nobel Peace Prize laureate <em>Memorial</em>.
	The people from Georgia have successfully ralied against a similar law that was discussed in their country.
	At the same time, democratic countries like the USA and Australia have their own foreign agent laws.</p>


	<p>The BBC would be as much a foreign agent in Russia as RT (Russia Today) would be in Britain.
	Yet, from the point of view of our democratic values,
	we cannot put the influence of these two media conglomerates at the same level.</p>

	<p>When considering the influence that foreign agents, possibily backed by foreign governments,
	have on a country's local media landscape,
	one must take the following into account.</p>

	<p>Is the attempt at influence <strong>overt</strong> or <strong>covert</strong>?
	For example, authoritarian regimes are known to use troll farms to covertly influence elections in democratic countries.</p>


	<p>Is the influence <strong>benign</strong> or <strong>malignent</strong> in intent?
	Taiwan, a democratic jewel in Asia, has to resort to some ad-hoc legislation
	in order to curb the nefarious influence that the Chinese Communist Party would like to exert
	in the Taiwan media market.</p>

	<p>Is the foreign influence in opposition to the <strong>national interest</strong>?
	But then one should consider who defines what the national interest is and how.
	For example, the interest of the interests of the Iran autocratic regime
	may not be in alignment with the interests of the Iranian people.</p>

	<p>In view of the above, should democratic countries adopt legislation to identify and control foreign influence?
	Do the US and Australian legislation on foreign agents play a legitimate role in a democracy?
	Is the letter of the law acceptable?
	Does the way the law is enforced congruent with democratic principles?</p>

	<p>Maybe the problem is not that the influence is of foreign origin,
	but of the autocratic nature of some of the countries trying to exert influence.
	It may be preferable to judge local and foreign media by the same standards.
	However, those standards still remain to be defined, within the boundaries of freedom of expression.</p>
	HTML;

$h2_legislation = newH2();
$h2_legislation['en'] = 'Legislation';


$div_codeberg = newSection('codeberg', '28', 'Ban of Russian media in the EU and other countries');
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>What existing legislation was used to justify the ban of Russian media outlets and Russian propaganda sources?</p>
	HTML;

$div_foreign_agents = newSection();
$div_foreign_agents['stars']   = -1;
$div_foreign_agents['class'][] = '';
$div_foreign_agents['en'] = <<<HTML
	<p>Some countries have adopted legislation to control, regular, curtail or restrict foreign influence in their national media landscape.</p>
	HTML;



$div_foreign_agent_russia= newSection();
$div_foreign_agent_russia['stars']   = -1;
$div_foreign_agent_russia['class'][] = '';
$div_foreign_agent_russia['en'] = <<<HTML
	<h3>Russia</h3>

	<p>Russia's foreign agent laws has been used as a tool of autocratic suppression of democratic opposition to the regime.</p>
	HTML;

$div_foreign_agent_georgia = newSection();
$div_foreign_agent_georgia['stars']   = -1;
$div_foreign_agent_georgia['class'][] = '';
$div_foreign_agent_georgia['en'] = <<<HTML
	<h3>Georgia</h3>

	<p>The people of Georgia demonstrated in 2023 in opposition of a foreign agent law similar to the one in use in Russia.
	The demonstrations successfully defeated the bill which was ultimately withdrawn.</p>

	<p>See also:</p>
	<ul>
		<li><a href="/georgia.html">Georgia</a></li>
	</ul>
	HTML;

$r1 = newRef('https://www.indexoncensorship.org/2021/02/bbc-banned-in-mainland-china/', 'BBC banned in mainland China');
$r2 = newRef('https://www.cnbc.com/2021/02/16/china-blocks-bbc-world-news-after-uk-revokes-license-of-cgtn.html', 'Why China banned the BBC, and why it matters');

$div_foreign_agent_prc = newSection();
$div_foreign_agent_prc['stars']   = -1;
$div_foreign_agent_prc['class'][] = '';
$div_foreign_agent_prc['en'] = <<<HTML
	<h3>People's Republic of China</h3>

	<p>The People's Republic of China is known to routinely curtail the freedom of media from democratic countries.
	In 2021, China banned the BBC. {$r1}{$r2}</p>
	HTML;


$div_foreign_agent_taiwan = newSection();
$div_foreign_agent_taiwan['stars']   = -1;
$div_foreign_agent_taiwan['class'][] = '';
$div_foreign_agent_taiwan['en'] = <<<HTML
	<h3>Taiwan (Republic of China)</h3>

	<p>Taiwan must contend with pervasive aggression from the People's Republic of China,
	and for that reason must take a proactive stance against Chinese influence.</p>
	HTML;


$div_foreign_agent_usa = newSection();
$div_foreign_agent_usa['stars']   = -1;
$div_foreign_agent_usa['class'][] = '';
$div_foreign_agent_usa['en'] = <<<HTML
	<h3>United States</h3>

	<p>The United States and Australia are some of the democratic countries which have explicit foreign agent laws.</p>
	HTML;



$div_wikipedia_foreign_agent = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Foreign_agent', 'Foreign agent');
$div_wikipedia_foreign_agent['stars']   = -1;
$div_wikipedia_foreign_agent['class'][] = '';
$div_wikipedia_foreign_agent['en'] = <<<HTML
	<p>Some countries have formal procedures to legalize the activities of foreign agents acting overtly.
	Laws covering foreign agents vary widely from country to country, and selective enforcement may prevail within countries, based on perceived national interest.</p>
	HTML;

$div_wikipedia_foreign_agents_registration_act = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Foreign_Agents_Registration_Act', 'Foreign Agents Registration Act');
$div_wikipedia_foreign_agents_registration_act['stars']   = -1;
$div_wikipedia_foreign_agents_registration_act['class'][] = '';
$div_wikipedia_foreign_agents_registration_act['en'] = <<<HTML
	<p>The Foreign Agents Registration Act is a United States law that imposes public disclosure obligations on persons representing foreign interests.
	It requires "foreign agents" to register with the Department of Justice (DOJ) and disclose their activities.</p>
	HTML;

$div_wikipedia_foreign_influence_transparency_scheme_act_2018 = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Foreign_Influence_Transparency_Scheme_Act_2018',
                                                                                        'Foreign Influence Transparency Scheme Act 2018');
$div_wikipedia_foreign_influence_transparency_scheme_act_2018['stars']   = -1;
$div_wikipedia_foreign_influence_transparency_scheme_act_2018['class'][] = '';
$div_wikipedia_foreign_influence_transparency_scheme_act_2018['en'] = <<<HTML
	<p>The Foreign Influence Transparency Scheme Act 2018 is an Australian statute that creates a registration scheme for foreign agents in Australia.</p>
	HTML;

$div_wikipedia_russian_foreign_agent_law = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Russian_foreign_agent_law', 'Russian foreign agent law');
$div_wikipedia_russian_foreign_agent_law['stars']   = -1;
$div_wikipedia_russian_foreign_agent_law['class'][] = '';
$div_wikipedia_russian_foreign_agent_law['en'] = <<<HTML
	<p>The Russian foreign agent law requires anyone who receives "support" from outside Russia or is under "influence" from outside Russia
	to register and declare themselves as "foreign agents".
	Once registered, they are subject to additional audits
	and are obliged to mark all their publications with a 24-word disclaimer saying that they are being distributed by a "foreign agent".</p>
	HTML;



$r3 = newRef('https://www.aljazeera.com/news/2023/4/12/algeria-moves-to-further-curb-press-freedom-with-new-law', 'Algeria law ‘controlling media’ close to passing');
$r4 = newRef('https://www.jeuneafrique.com/1433048/politique/liberte-de-la-presse-nouveau-tour-de-vis-en-algerie/', 'En Algérie, des journalistes toujours plus sous pression');

$div_foreign_agent_algeria = newSection();
$div_foreign_agent_algeria['stars']   = -1;
$div_foreign_agent_algeria['class'][] = '';
$div_foreign_agent_algeria['en'] = <<<HTML
	<h3>Algeria</h3>

	<p>The Algerian parliament is voting on a new media law forcing journalists to reveal their sources,
	banning media organisations from receiving foreign funds.
	Reporters Without Borders has decried this bill and worries about the adverse effect on journalism in Algeria. {$r3}{$r4}</p>
	HTML;


$body .= printSection($div_stub);
$body .= printSection($div_section_media);

$body .= printH2($h2_introduction);
$body .= printSection($div_introduction);

$body .= printH2($h2_legislation);

$body .= printSection($div_foreign_agents);
$body .= printSection($div_wikipedia_foreign_agent);


$body .= printSection($div_foreign_agent_russia);
$body .= printSection($div_wikipedia_russian_foreign_agent_law);
$body .= printSection($div_codeberg);

$body .= printSection($div_foreign_agent_georgia);

$body .= printSection($div_foreign_agent_prc);

$body .= printSection($div_foreign_agent_taiwan);

$body .= printSection($div_foreign_agent_usa);
$body .= printSection($div_wikipedia_foreign_agents_registration_act);
$body .= printSection($div_wikipedia_foreign_influence_transparency_scheme_act_2018);

$body .= printSection($div_foreign_agent_algeria);

include('include/page.php');
