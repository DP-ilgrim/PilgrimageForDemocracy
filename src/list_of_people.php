<?php
include_once('section/all.php');
$h1['en'] = 'List of People';



//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>For each person that shall be featured below,
	we include only the information that is directly relevant to the project.
	In particular, we shall not cover personal information, biography, controversies, etc.
	unless such information has particular significance for the topics we aim to cover.
	To the extent that a person is notable enough to have a wikipedia article,
	we shall link to it:
	interested readers shall find more information there.</p>
	HTML;


$div_list = new ContentSection();

$div_list->content = <<<HTML
	<h3>list</h3>

	<p>Listed in alphabetical order by surname.</p>

	<ul>
		<li>${'Anne Applebaum'}</li>
		<li>${'Rolf Dobelli'}</li>
		<li>${'Denise Dresser'}</li>
		<li><a href="/staffan_lindberg.html">Staffan I. Lindberg</a></li>
		<li>${'Maria Ressa'}</li>
		<li>${'Karoline Wiesner'}</li>
	</ul>
	HTML;



$body .= printPageSection('lists.html');
$body .= printSection($div_stub);

$body .= $div_introduction->print();
$body .= $div_list->print();


include('include/page.php');
