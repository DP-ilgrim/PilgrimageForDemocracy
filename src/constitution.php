<?php
include_once('section/all.php');
$h1['en'] = 'Constitution';



//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>A constitution of a country is the core legal document superseding all other laws and legal documents.</p>

	<p>A constitution has two important functions.
	First, it provides the ground rules for government and the country to operate.
	Second, it should provide stability for the democracy by being resilient to subversion by would-be autocrats.</p>
	HTML;


$h2_Constitutional_amendment = new h2HeaderContent('Constitutional amendment');

$div_constitutional_amendment = new ContentSection();
$div_constitutional_amendment->content = <<<HTML
	<p>Nothing is immutable and nothing is conceived perfect.
	For these reasons, a constitution should be amendable.
	A constitution that can be amended only with great difficulty risks of getting ossified and inadequate for modern times.
	The constitution of the United States sets very difficult hurdles to pass in order to amend it.</p>

	<p>On the other hand, a constitution should not be too easily amended to prevent bad actors from subverting the democracy.</p>

	<p>Several autocratic leaders have had constitutional amendments passed so that they could stay in power essentially for life.</p>

	<ul>
		<li>Vladimir Putin changed the constitution of $Russia.</li>
		<li>Recep Tayyip Erdoğan changed the constitution of $Turkey.</li>
		<li>Xi Jinping changed the charter of the Chinese Communist Party.</li>
		<li>See also the 2023 referendum in $Mali.</li>
	</ul>
	HTML;



$div_wikipedia_Constitution = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Constitution', 'Constitution');
$div_wikipedia_Constitution['stars']   = -1;
$div_wikipedia_Constitution['class'][] = '';
$div_wikipedia_Constitution['en'] = <<<HTML
	<p>A constitution is the aggregate of fundamental principles or established precedents that constitute the legal basis of a polity,
	organisation or other type of entity and commonly determine how that entity is to be governed.</p>
	HTML;

$div_wikipedia_Constitutional_amendment = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Constitutional_amendment', 'Constitutional amendment');
$div_wikipedia_Constitutional_amendment['stars']   = -1;
$div_wikipedia_Constitutional_amendment['class'][] = '';
$div_wikipedia_Constitutional_amendment['en'] = <<<HTML
	<p>Most constitutions require that amendments cannot be enacted unless they have passed a special procedure
	that is more stringent than that required of ordinary legislation.</p>
	HTML;



$body .= printSection($div_section_institutions);
$body .= printSection($div_stub);
$body .= printSection($div_introduction);

$body .= $h2_Constitutional_amendment->print();
$body .= $div_constitutional_amendment->print();

$body .= printSection($div_wikipedia_Constitution);
$body .= printSection($div_wikipedia_Constitutional_amendment);


include('include/page.php');
