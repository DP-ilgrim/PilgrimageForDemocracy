<?php
include_once('section/duverger.php');
$h1['en'] = 'Duverger Syndrome';

//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>
	</p>
	HTML;

$h2_duverger_syndrome = newH2();
$h2_duverger_syndrome['en'] = 'Duverger symptoms';

$h2_duverger_theory = newH2();
$h2_duverger_theory['en'] = "Duverger's Law";

$h2_duverger_examples = newH2();
$h2_duverger_examples['en'] = 'Historical examples';


$body .= printSection($div_introduction);

$body .= printH2($h2_duverger_syndrome);
$body .= printSection($div_section_symptom_dualism);
$body .= printSection($div_section_symptom_alternance);
$body .= printSection($div_section_symptom_party_politics);
$body .= printSection($div_section_symptom_negative_campaigning);
$body .= printSection($div_section_symptom_lack_choice_good_candidates);

$body .= printH2($h2_duverger_theory);
$body .= printSection($div_section_duverger_law);

$body .= printH2($h2_duverger_examples);
$body .= printSection($div_section_example_usa_19th_century);
$body .= printSection($div_section_example_taiwan_2000);
$body .= printSection($div_section_example_france_alternance);





include('include/page.php');
