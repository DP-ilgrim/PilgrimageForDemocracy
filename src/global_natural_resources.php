<?php
include_once('section/global_issues.php');
$h1['en'] = 'Global Natural Resources';

$snp['description'] = "Sharing the limited resources of a finite world.";
$snp['image'] = "/copyrighted/anne-nygard-v5mEr9CfG18.1200-630.jpg";


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>We have no other choice but to learn how to share the limited resources of a finite world.</p>

	<p>Many conflicts could be avoided if we used our natural resources sustainably.</p>
	HTML;



$div_wikipedia_Resource_war = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Resource_war', 'Resource war');
$div_wikipedia_Resource_war['stars']   = -1;
$div_wikipedia_Resource_war['class'][] = '';
$div_wikipedia_Resource_war['en'] = <<<HTML
	<p>In a resource war, there is typically a nation or group that controls the resource
	and an aggressor that wishes to seize control over said resource.
	This power dynamic between nations has been a significant underlying factor in conflicts since the late 19th century.</p>
	HTML;

$div_wikipedia_Non_renewable_resource = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Non-renewable_resource', 'Non-renewable resource');
$div_wikipedia_Non_renewable_resource['stars']   = -1;
$div_wikipedia_Non_renewable_resource['class'][] = '';
$div_wikipedia_Non_renewable_resource['en'] = <<<HTML
	<p>Earth minerals and metal ores, fossil fuels (coal, petroleum, natural gas)
	and groundwater in certain aquifers are all considered non-renewable resources.</p>
	HTML;

$div_wikipedia_Water_conflict = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Water_conflict', 'Water conflict');
$div_wikipedia_Water_conflict['stars']   = -1;
$div_wikipedia_Water_conflict['class'][] = '';
$div_wikipedia_Water_conflict['en'] = <<<HTML
	<p>Water has long been a source of tension and one of the causes for conflicts.
	Water conflicts arise for several reasons, including territorial disputes, a fight for resources, and strategic advantage.</p>
	HTML;


$body .= printSection($div_section_global_issues);
$body .= printSection($div_stub);

$body .= printSection($div_introduction);

$body .= printSection($div_wikipedia_Resource_war);
$body .= printSection($div_wikipedia_Non_renewable_resource);
$body .= printSection($div_wikipedia_Water_conflict);

include('include/page.php');
