<?php
include_once('section/world.php');
$h1['en'] = 'Philippines';



//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>See also: ${'Maria Ressa'}.</p>
	HTML;


$div_wikipedia_Human_rights_in_the_Philippines = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Human_rights_in_the_Philippines', 'Human rights in the Philippines');
$div_wikipedia_Human_rights_in_the_Philippines['stars']   = -1;
$div_wikipedia_Human_rights_in_the_Philippines['class'][] = '';
$div_wikipedia_Human_rights_in_the_Philippines['en'] = <<<HTML
	<p>The concept and practice of human rights within the Philippines is defined by Article III of the Philippine Constitution,
	as well as the United Nations' International Bill of Human Rights, to which the Philippines is a signatory.</p>
	HTML;

$div_wikipedia_Commission_on_Human_Rights_Philippines = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Commission_on_Human_Rights_(Philippines)', 'Commission on Human Rights (Philippines)');
$div_wikipedia_Commission_on_Human_Rights_Philippines['stars']   = -1;
$div_wikipedia_Commission_on_Human_Rights_Philippines['class'][] = '';
$div_wikipedia_Commission_on_Human_Rights_Philippines['en'] = <<<HTML
	<p>The Commission on Human Rights is an independent constitutional office created under the 1987 Constitution of the Philippines,
	with the primary function of investigating all forms of human rights violations involving civil and political rights in the Philippines.</p>
	HTML;

$div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Campaign_for_Human_Rights_in_the_Philippines', 'Campaign for Human Rights in the Philippines');
$div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines['stars']   = -1;
$div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines['class'][] = '';
$div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines['en'] = <<<HTML
	<p>The Campaign for Human Rights in the Philippines (CHRP) is a small but highly active human rights watchdog based in the United Kingdom.
	It has the backing of the British T.U.C, Amnesty International,
	and several educational institutions including a very close relationship with the School of Oriental and African Studies.</p>
	HTML;

$div_wikipedia_Philippine_Human_Rights_Information_Center = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Philippine_Human_Rights_Information_Center', 'Philippine Human Rights Information Center');
$div_wikipedia_Philippine_Human_Rights_Information_Center['stars']   = -1;
$div_wikipedia_Philippine_Human_Rights_Information_Center['class'][] = '';
$div_wikipedia_Philippine_Human_Rights_Information_Center['en'] = <<<HTML
	<p>The Philippine Human Rights Information Center (PhilRights) is a non-profit, national human rights organization in the Philippines, Manila.</p>
	HTML;

$div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Extrajudicial_killings_and_forced_disappearances_in_the_Philippines', 'Extrajudicial killings and forced disappearances in the Philippines');
$div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines['stars']   = -1;
$div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines['class'][] = '';
$div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines['en'] = <<<HTML
	<p>The Philippine Alliance of Human Rights Advocates (PAHRA) is a non-profit, national human rights organization in the Philippines, Manila.</p>
	HTML;


$body .= printPageSection('world.html');
$body .= printSection($div_stub);
$body .= printSection($div_introduction);
$body .= printCountryIndices('Philippines');
$body .= printPageSection('maria_ressa.html');

$body .= printSection($div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines);
$body .= printSection($div_wikipedia_Human_rights_in_the_Philippines);
$body .= printSection($div_wikipedia_Philippine_Human_Rights_Information_Center);
$body .= printSection($div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines);
$body .= printSection($div_wikipedia_Commission_on_Human_Rights_Philippines);

include('include/page.php');
