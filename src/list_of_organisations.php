<?php
include_once('section/all.php');
$h1['en'] = 'List of organisations';


//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Below is a list of organizations that are covered in this website so far.</p>
	HTML;


$div_list = new ContentSection();

$div_list->content = <<<HTML
	<h3>list</h3>

	<p>Listed in alphabetical order.</p>

	<ul>
		<li>${'Institute for Economics and Peace'}</li>
		<li>${'V-Dem Institute'}</li>
	</ul>
	HTML;



$body .= printPageSection('lists.html');

$body .= $div_introduction->print();
$body .= $div_list->print();


include('include/page.php');
