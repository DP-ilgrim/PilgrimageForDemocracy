<?php
include_once('section/all.php');
$h1['en'] = 'Karoline Wiesner';


//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

$r1 = newRef('https://www.csh.ac.at/researcher/karoline-wiesner/', 'Karoline Wiesner, University of Potsdam & CSH External Faculty');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Karoline Wiesner is Professor of Complexity Sciences at the University of Potsdam.
	She obtained a PhD in physics from Uppsala University in 2004.
	Wiesner's research focuses on the use of information theory in the study of formation, maintenance and stability of complex systems.
	Current topics include philosophical and mathematical foundations of complexity, complexity in climate systems, stability of democracy. ${r1}</p>

	<p>She is a collaborator at the ${'V-Dem Institute'}.</p>
	HTML;



$div_Karoline_Wiesner = new WebsiteContentSection();
$div_Karoline_Wiesner->setTitleText('Karoline Wiesner ');
$div_Karoline_Wiesner->setTitleLink('https://www.karowiesner.org/');
$div_Karoline_Wiesner->content = <<<HTML
	<p>Karoline Wiesner's website.</p>
	HTML;



$div_googlescholar_Karoline_Wiesner = new GoogleScholarContentSection();
$div_googlescholar_Karoline_Wiesner->setTitleText('Karoline Wiesner');
$div_googlescholar_Karoline_Wiesner->setTitleLink('https://scholar.google.com/citations?user=TUL8h7QAAAAJ&hl=en');
$div_googlescholar_Karoline_Wiesner->content = <<<HTML
	<p>Professor of Complexity Science, University of Potsdam.</p>
	HTML;




$body .= printPageSection('list_of_people.html');
$body .= printSection($div_stub);
$body .= $div_introduction->print();

$body .= $div_Karoline_Wiesner->print();
$body .= $div_googlescholar_Karoline_Wiesner->print();
$body .= printPageSection('the_hidden_dimension_in_democracy.html');


include('include/page.php');
