<?php
include_once('section/all.php');
$h1['en'] = 'Politics';

//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>Politics has a bad reputation.
	However, properly seen and properly practised, politics is a noble art.</p>

	<p>We, humanity, share the same geographical space (our only planet), and the same chronological time.
	Politics is the art of elaborating a framework thanks to which we can live in peace and harmony.</p>
	HTML;



$div_wikipedia_Politics = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Politics', 'Politics');
$div_wikipedia_Politics['stars']   = -1;
$div_wikipedia_Politics['class'][] = '';
$div_wikipedia_Politics['en'] = <<<HTML
	<p>Politics is the set of activities that are associated with making decisions in groups, or other forms of power relations among individuals,
	such as the distribution of resources or status. The branch of social science that studies politics and government is referred to as political science.</p>
	HTML;

$div_wikipedia_Political_science = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Political_science', 'Political science');
$div_wikipedia_Political_science['stars']   = -1;
$div_wikipedia_Political_science['class'][] = '';
$div_wikipedia_Political_science['en'] = <<<HTML
	<p>Political science is the scientific study of politics.
	It is a social science dealing with systems of governance and power, and the analysis of political activities,
	political institutions, political thought and behavior, and associated constitutions and laws.</p>
	HTML;

$div_wikipedia_List_of_political_theorists = newSection('wikipedia', 'https://en.wikipedia.org/wiki/List_of_political_theorists', 'List of political theorists');
$div_wikipedia_List_of_political_theorists['stars']   = -1;
$div_wikipedia_List_of_political_theorists['class'][] = '';
$div_wikipedia_List_of_political_theorists['en'] = <<<HTML
	<p>A political theorist is someone who engages in constructing or evaluating political theory, including political philosophy.
	Theorists may be academics or independent scholars. Here is a list of the most notable political theorists.</p>
	HTML;


$body .= printSection($div_stub);
$body .= printSection($div_introduction);

$body .= printPageSection('institutions.html');
$body .= printSection($div_section_political_discourse);

$body .= printSection($div_wikipedia_Politics);
$body .= printSection($div_wikipedia_Political_science);
$body .= printSection($div_wikipedia_List_of_political_theorists);


include('include/page.php');
