<?php
include_once('section/indices.php');
$h1['en'] = 'Global Peace Index';


$snp['description'] = "Published by the IEP, the GPI measures the relative peacefulness of each country.";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Global Peace Index is published by the ${'Institute for Economics and Peace'}.</p>
	HTML;



$div_wikipedia_Global_Peace_Index = new WikipediaContentSection();
$div_wikipedia_Global_Peace_Index->setTitleText('Global Peace Index');
$div_wikipedia_Global_Peace_Index->setTitleLink('https://en.wikipedia.org/wiki/Global_Peace_Index');
$div_wikipedia_Global_Peace_Index->content = <<<HTML
	<p>Global Peace Index (GPI) is a report produced by the Institute for Economics & Peace (IEP)
	which measures the relative position of nations' and regions' peacefulness.</p>
	HTML;

$body .= printPageSection('list_of_indices.html');
$body .= printSection($div_stub);

$body .= $div_introduction->print();
$body .= $div_wikipedia_Global_Peace_Index->print();


include('include/page.php');
