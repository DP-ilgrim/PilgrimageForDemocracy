<?php
include_once('section/project.php');

$h1['en'] = 'Pilgrimage for Democracy and Social Justice';

$div_quote_purpose = newSection();
$div_quote_purpose['stars']   = -1;
$div_quote_purpose['class'][] = '';
$div_quote_purpose['en'] = <<<HTML
	<blockquote>
	"<strong>A pilgrim is a wanderer with a purpose.</strong>
	A pilgrimage can be to a place — that's the best-known kind — but it can also be for a thing.
	Mine is for peace, and that is why I am a Peace Pilgrim."
	<br>
	— Peace Pilgrim (1908 – 1981)
	</blockquote>
	HTML;

$div_democracy_and_social_justice = newSection();
$div_democracy_and_social_justice['stars']   = -1;
$div_democracy_and_social_justice['class'][] = '';
$div_democracy_and_social_justice['en'] = <<<HTML
	<h3>Democracy and social justice are two critical topics for our times</h3>

	<p>There cannot be true, lasting peace without democracy.
	<br>There cannot be true, lasting peace without social justice.
	</p>

	<p>The latest electronic gadgets, fancy cars, big houses, fashionable clothes, etc., are not critical necessities.
	But simple vegetables or a simple bowl of rice are essential items for life.
	<br><strong>A world where many have much too much
	while too many still struggle to survive with the barest necessities of life,
	cannot achieve global peace.</strong>
	</p>

	<p>Democracy and social justice are the two sides of the same coin.
	</p>
	HTML;

$div_improve_ourselves_institutions_leaders = newSection();
$div_improve_ourselves_institutions_leaders['stars']   = -1;
$div_improve_ourselves_institutions_leaders['class'][] = '';
$div_improve_ourselves_institutions_leaders['en'] = <<<HTML
	<blockquote>
	"<strong>In order for the world to become peaceful, people must become more peaceful</strong>.
	Among mature people war would not be a problem — it would be impossible.
	In their immaturity people want, at the same time, peace and the things which make war.
	However, people can mature just as children grow up.
	Yes, our institutions and our leaders reflect our immaturity,
	but <strong>as we mature we will elect better leaders and set up better institutions</strong>.
	It always comes back to the thing so many of us wish to avoid: working to improve ourselves.
	<br>
	— Peace Pilgrim (1908 – 1981), Harmonious Principles for Human Living.
	</blockquote>

	<br>

	<blockquote>
	"<strong>No man is an island entire of itself</strong>;<br>
	every man is a piece of the continent,<br>
	a part of the main;<br>
	<br>
	if a clod be washed away by the sea,<br>
	Europe is the less, as well as if a promontory were,<br>
	as well as any manner of thy friends or of thine own were;</br>
	<br>
	<strong>any man's death diminishes me,<br>
	because I am involved in mankind.<br>
	And therefore never send to know for whom the bell tolls;<br>
	it tolls for thee.</strong>"
	<br>
	— John Donne (1571 – 1631), English poet.
	</blockquote>
	HTML;

$h2_menu = newH2();
$h2_menu['en'] = 'Come on in!';


$body .= printSection($div_quote_purpose);
$body .= printSection($div_democracy_and_social_justice);
$body .= printSection($div_improve_ourselves_institutions_leaders);

$body .= printH2($h2_menu);
$body .= printPageSection('menu.html');
$body .= printSection($div_section_participate);
$body .= printSection($div_section_copyright);
$body .= printSection($div_section_updates);

include('include/page.php');
