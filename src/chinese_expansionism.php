<?php
include_once('include/init.php');
$h1['en'] = 'Chinese expansionism';


$h2_introduction['en'] = '';

$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML

	<p>The <a href="/prc_china.html">People's Republic of China</a> has territorial disputes with most of its neighbors.
	There are unresolved conflicting territorial claims between China
	and practically all of its terrestrial and maritime neighbours.
	The PRC government has a long-term policy to gain control over territories it considers its own.</p>

	<p>It is commonly known that PRC claims Taiwan as its own
	as well as all the islands currently controlled by the Republic of China (Taiwan).<p>

	<p>Another bold territorial claim is represented by a map of the South China sea
	on which are drawn nine dashes encompassing most of the sea,
	putting the PRC in conflict with all of its maritime neighbours.</p>
	HTML;


$h2_philippines = newH2();
$h2_philippines['en'] = 'The PRC and the Philippines';

$r1 = newRef('https://www.taipeitimes.com/News/front/archives/2022/12/22/2003791155', 'Manila frets over China’s island activity');

$div_philippines = newSection();
$div_philippines['stars']   = -1;
$div_philippines['class'][] = '';
$div_philippines['en'] = <<<HTML
	<p>The People's Republic of China's claims over the South China Sea has put it
	at odds with the Philippines.
	In recent years, conflicts between the two countries have increased.</p>

	<p>For the last few years, the People's Republic of China has been building military runways and facilities
	on otherwise unhabitable islets parts of the  Spratly Islands.
	This way, the PRC is able to build advanced military bases,
	and boost its claims to the South China Sea.</p>

	<p>In December 2022, news satellite images from US officials show new land formations in the Spratly Islands,
	with Chinese vessels seen working there. The Philippine Ministry of Foreign Affairs said that
	they were "seriously concerned, as such activities contravene the
	Declaration of Conduct on the South China Sea’s undertaking on self-restraint and the 2016 Arbitral Award." {$r1}</p>

	HTML;

$div_wikipedia_prc_philippines = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Philippines_v._China', 'Philippines v. China');
$div_wikipedia_prc_philippines['stars']   = -1;
$div_wikipedia_prc_philippines['class'][] = '';
$div_wikipedia_prc_philippines['en'] = <<<HTML
	<p>In 2016, an arbitration under the <em>United Nations Convention on Law of the Sea (UNCLOS)</em> ruled in favour of the Philippines
	in a maritime border dispute against the PRC.
	China issued a statement stating it would not abide by the arbitral tribunal's decision and that it will "ignore the ruling".</p>
	HTML;




$div_wikipedia = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Chinese_expansionism', 'Chinese expansionism');
$div_wikipedia['stars']   = -1;
$div_wikipedia['class'][] = '';
$div_wikipedia['en'] = <<<HTML
	<p>This wikipedia article includes historical occurrences of Chinese expansionism.
	There is a list of disputed islands in the South China Sea.</p>
	HTML;

$div_wikipedia_9_dash_line = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Nine-dash_line', 'Nine-dash line');
$div_wikipedia_9_dash_line['stars']   = -1;
$div_wikipedia_9_dash_line['class'][] = '';
$div_wikipedia_9_dash_line['en'] = <<<HTML
	<p>Today, the People's Republic of China still claims
	territories encompassed by nine dashes drawn on a map of the South China Sea.
	These claims puts the PRC on a collision course with all its maritime neighbors.</p>
	HTML;

$div_wikipedia_south_china_sea = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Territorial_disputes_in_the_South_China_Sea', 'Territorial disputes in the South China Sea');
$div_wikipedia_south_china_sea['stars']   = -1;
$div_wikipedia_south_china_sea['class'][] = '';
$div_wikipedia_south_china_sea['en'] = <<<HTML
	<p>Territorial disputes in the South China Sea involve conflicting island and maritime claims in the region by several sovereign states,
	namely Brunei, the People's Republic of China (PRC), Taiwan (Republic of China/ROC), Indonesia, Malaysia, the Philippines, and Vietnam.</p>
	HTML;


$div_codeberg = newSection('codeberg', '11', "Research the People's Republic of China's territorial claims");
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>Make list of current territorial claims and how they affect the relationship between the PRC and its neighbors.</p>
	HTML;




$body .= printSection($div_stub);

$body .= printSection($div_introduction);

$body .= printPageSection('china_and_taiwan.html');
$body .= printH2($h2_philippines);
$body .= printSection($div_philippines);
$body .= printSection($div_wikipedia_prc_philippines);

$body .= printSection($div_codeberg);
$body .= printSection($div_wikipedia);
$body .= printSection($div_wikipedia_9_dash_line);
$body .= printSection($div_wikipedia_south_china_sea);


include('include/page.php');
