<?php
include_once('section/all.php');
$h1['en'] = 'Standing on the shoulders of giants';

//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>Our nascent project is still tiny, but will achieve its goals by building up on the work of very large organizations.</p>
	HTML;

$div_Connecting_the_dots_and_filling_the_gaps = newSection();
$div_Connecting_the_dots_and_filling_the_gaps['stars']   = -1;
$div_Connecting_the_dots_and_filling_the_gaps['class'][] = '';
$div_Connecting_the_dots_and_filling_the_gaps['en'] = <<<HTML
	<h3>Connecting the dots and filling the gaps</h3>

	<p>We would be wasting our time if we were merely trying to replicate the excellent work already done
	by organizations of all kinds and which have much more financial and human resources than we do.</p>

	<p>Yet, we notice many organizations which have identical aims,
	and which work separately from each other, in some cases even in competition with one another.
	We wish to acknowledge the immense contributions made by those organizations.
	Everywhere appropriate we shall refer their work to our readers.</p>

	<p>We are connecting the dots,
	highlighting similarities in action and in purpose between organizations, between our work and theirs.</p>

	<p>At the same time, we notice some lacking perspectives and missing elements.
	Our work aims to fill the gaps.</p>
	HTML;


$h2_Giants = newH2();
$h2_Giants['en'] = 'Giants';

$div_giants = newSection();
$div_giants['stars']   = -1;
$div_giants['class'][] = '';
$div_giants['en'] = <<<HTML
	<p>The following list only present the main organizations that we rely on.
	A full list of people and organizations on whose work we build our own could never be compiled.</p>

	<p>The list below is presented <strong>in alphabetical order</strong>.</p>
	HTML;



$div_Freedom_House = newSection();
$div_Freedom_House['stars']   = -1;
$div_Freedom_House['class'][] = '';
$div_Freedom_House['en'] = <<<HTML
	<h3>Freedom House</h3>

	<p>
	</p>
	HTML;



$div_Wikipedia = newSection();
$div_Wikipedia['stars']   = -1;
$div_Wikipedia['class'][] = '';
$div_Wikipedia['en'] = <<<HTML
	<h3>Wikipedia</h3>

	<p>The size and the human resources of the Wikipedia project allow it to cover almost any topic in much more depth than we could ever wish to accomplish.</p>

	<p>Many of our articles prominently link to relevant Wikipedia articles, so that it saves us the time to replicate the same work.
	This way, we can focus on our own analysis, editorializing and original research,
	all of which is forbidden to do on Wikipedia.
	See the article below for further discussion on the similarities and differences between Wikipedia and our project.</p>
	HTML;



$div_wikipedia_Standing_on_the_shoulders_of_giants = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Standing_on_the_shoulders_of_giants', 'Standing on the shoulders of giants');
$div_wikipedia_Standing_on_the_shoulders_of_giants['stars']   = -1;
$div_wikipedia_Standing_on_the_shoulders_of_giants['class'][] = '';
$div_wikipedia_Standing_on_the_shoulders_of_giants['en'] = <<<HTML
	<p>The phrase "standing on the shoulders of giants" is a metaphor which means
	"using the understanding gained by major thinkers who have gone before in order to make intellectual progress".
	It is a metaphor of dwarfs standing on the shoulders of giants and expresses the meaning of "discovering truth by building on previous discoveries".
	Famously, Issac Newton wrote in a 1675 letter: "<em>if I have seen further [than others], it is by standing on the shoulders of giants.</em>"</p>
	HTML;


$body .= printPageSection('project/index.html');

$body .= printSection($div_introduction);
$body .= printSection($div_Connecting_the_dots_and_filling_the_gaps);

$body .= printH2($h2_Giants);
$body .= printSection($div_giants);

$body .= printSection($div_Freedom_House);
$body .= printPageSection('freedom_house.html');

$body .= printSection($div_Wikipedia);
$body .= printPageSection('project/wikipedia.html');

$body .= printSection($div_wikipedia_Standing_on_the_shoulders_of_giants);


include('include/page.php');
