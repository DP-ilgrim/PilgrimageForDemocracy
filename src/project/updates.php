<?php
include_once('section/project.php');
$h1['en'] = 'Updates';

$h2_code = newH2();
$h2_code['en'] = 'Code update';

$div_code = newSection();
$div_code['stars'] = -1;
$div_code['class'][] = '';
$div_code['en'] = <<<HTML
	<p>This website is open source.
	The whole source code is published within the <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy">Pilgrimage for Democracy</a> at Codeberg.
	If you are interested in following every minor change in the website,
	you can browse the project's code repository and <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master">check every single commit</a>.
	</p>
	<p>See below for the most important content updates, with new articles, expanded articles and expanded sections.</p>
	HTML;

$h2_content = newH2();
$h2_content['en'] = 'Content update';

$div_update = newSection();
$div_update['en'] = <<<HTML
	<p>See below the list of <em>major</em> updates since your last visit:</p>
	<ul>
	HTML;

// function addUpdate(&$div_update, $date, $stars_from, $stars_to, $link, $title_en, $text_en = '')
//addUpdate($div_update, '2023-07-0', 0, 1, '/.html', '', '');
addUpdate($div_update, '2023-07-03', 0, 1, '/taiwan.html', 'Taiwan', '');
addUpdate($div_update, '2023-07-02', 0, 1, '/political_discourse.html', 'Political discourse', '');
addUpdate($div_update, '2023-07-02', 0, 1, '/denise_dresser.html', 'Denise Dresser', '');
addUpdate($div_update, '2023-06-30', 0, 3, '/the_remains_of_the_day.html', 'The Remains of the Day', 'Novel and film');
addUpdate($div_update, '2023-06-29', 0, 1, '/maria_ressa.html', 'Maria Ressa', 'Journalist from the Philippines and Nobel Peace Prize laureate.');
addUpdate($div_update, '2023-06-29', 0, 2, '/project/codeberg.html', 'Codeberg', 'Participate');
addUpdate($div_update, '2023-06-28', 0, 1, '/ground_news.html', 'Ground News', '');
addUpdate($div_update, '2023-06-28', 1, 2, '/information_overload.html', 'Information overload', '');
addUpdate($div_update, '2023-06-27', 0, 1, '/information_overload.html', 'Information overload', '');
addUpdate($div_update, '2023-06-26', 0, 1, '/karoline_wiesner.html', 'Karoline Wiesner', '');
addUpdate($div_update, '2023-06-26', 0, 1, '/staffan_lindberg.html', 'Staffan Lindberg', '');
addUpdate($div_update, '2023-06-26', 0, 1, '/v_dem_institute.html', 'V-Dem Institute', '');
addUpdate($div_update, '2023-06-23', 0, 1, '/constitution.html', 'Constitution', '');
addUpdate($div_update, '2023-06-20', 0, 1, '/mali.html', 'Mali', '');
addUpdate($div_update, '2023-06-19', 0, 2, '/transnational_authoritarianism.html', 'Transnational authoritarianism', '');
addUpdate($div_update, '2023-06-17', 1, 2, '/freedom_house.html', 'Freedom House', 'Policy recommendations and research methodology.');
addUpdate($div_update, '2023-06-15', 0, 1, '/philippines.html', 'Philippines', '');
addUpdate($div_update, '2023-06-14', 1, 1, '/world.html', 'Country profiles', 'Added Freedom House Internet freedom profile and index to each country.');
addUpdate($div_update, '2023-06-14', 0, 1, '/professionalism_without_elitism.html', 'Professionalism without Elitism', '');
addUpdate($div_update, '2023-06-12', 0, 0, '/lawmaking.html', 'Lawmaking', 'New stub');
addUpdate($div_update, '2023-06-11', 1, 1, '/world.html', 'Country profiles', 'Added Freedom House Index to each country.');
addUpdate($div_update, '2023-06-10', 0, 1, '/world.html', 'Country profiles', 'Added Freedom House country profiles to each country.');
addUpdate($div_update, '2023-06-08', 0, 1, '/beliefs.html', 'Beliefs', '');
addUpdate($div_update, '2023-06-07', 0, 1, '/freedom_house.html', 'Freedom House', '');
addUpdate($div_update, '2023-06-06', 3, 4, '/secondary_features_of_democracy.html', 'Secondary features of democracy — We, the Society', 'minor update');
addUpdate($div_update, '2023-06-05', 0, 1, '/project/standing_on_the_shoulders_of_giants.html', 'Standing on the shoulders of giants', '');
addUpdate($div_update, '2023-06-04', 1, 2, '/project/wikipedia.html', 'Wikipedia', 'Similarities and differences.');
addUpdate($div_update, '2023-06-02', 0, 2, '/ukraine.html', 'Ukraine', '');
addUpdate($div_update, '2023-06-01', 0, 1, '/duverger_syndrome.html', 'Duverger syndrome', 'Section outline');
addUpdate($div_update, '2023-05-31', 0, 1, '/project/wikipedia.html', 'Wikipedia', 'Similarities and differences.');
addUpdate($div_update, '2023-05-30', 0, 1, '/peaceful_resistance_in_times_of_war.html', 'Peaceful resistance in times of war', '');
addUpdate($div_update, '2023-05-29', 0, 1, '/global_issues.html', 'Global issues', '');
addUpdate($div_update, '2023-05-29', 0, 1, '/hunger.html', 'Hunger', '');
addUpdate($div_update, '2023-05-24', 0, 2, '/living_together.html', 'Living Together', '');
addUpdate($div_update, '2023-05-23', 1, 3, '/democracy_wip.html', 'Democracy — a work in progress', '');
addUpdate($div_update, '2023-05-22', 0, 1, '/democracy_wip.html', 'Democracy — a work in progress', '');
addUpdate($div_update, '2023-05-12', 0, 1, '/costa_rica.html', 'Costa Rica', '');
addUpdate($div_update, '2023-05-11', 0, 3, '/secondary_features_of_democracy.html', 'Secondary features of democracy — We, the Society', '');
addUpdate($div_update, '2023-04-15', 0, 1, '/reporters_without_borders.html', 'Reporters Without Borders', '');
addUpdate($div_update, '2023-04-12', 0, 1, '/media.html', 'Media', '');
addUpdate($div_update, '2023-04-12', 0, 2, '/foreign_influence_local_media.html', 'Foreign influence in local media landscape', '');
addUpdate($div_update, '2023-04-11', 0, 1, '/russia.html', 'Russia', '');
addUpdate($div_update, '2023-04-08', 0, 1, '/project/development_website.html', 'Development website', 'How to set up a copy of this website on your computer.');
addUpdate($div_update, '2023-04-07', 0, 1, '/project/git.html', 'Git', 'Using git to contribute to the project.');
addUpdate($div_update, '2023-03-13', 0, 1, '/georgia.html', 'Georgia', '');
addUpdate($div_update, '2023-01-28', 0, 0, '/osce.html', 'OSCE (Organization for Security and Co-operation in Europe)', '');
addUpdate($div_update, '2023-01-27', 0, 2, '/united_nations.html', 'The United Nations', '');
addUpdate($div_update, '2023-01-22', 0, 1, '/iran.html', 'Iran', 'Start with resources from wikipedia.');
addUpdate($div_update, '2022-12-26', 0, 1, '/chinese_expansionism.html', 'Chinese expansionism', '');
addUpdate($div_update, '2022-12-12', 1, 3, '/project/participate.html', 'Participate', 'Content');
addUpdate($div_update, '2022-12-08', 1, 3, '/primary_features_of_democracy.html', 'Primary features of democracy', '');
addUpdate($div_update, '2022-11-26', 0, 0, '/justice.html', 'Justice', '');
addUpdate($div_update, '2022-11-25', 0, 0, '/media.html', 'Media', '');
addUpdate($div_update, '2022-11-25', 0, 1, '/corruption.html', 'Corruption', '');
addUpdate($div_update, '2022-11-23', 0, 1, '/fair_share.html', 'Fair share', '');
addUpdate($div_update, '2022-11-20', 0, 1, '/integrity.html', 'Election integrity', '');
addUpdate($div_update, '2022-11-14', 0, 1, '/primary_features_of_democracy.html', 'Primary features of democracy', '');
addUpdate($div_update, '2022-11-13', 0, 1, '/project/participate.html', 'Participate', '');
addUpdate($div_update, '2022-11-12', 0, 3, '/project/copyright.html', 'Copyright?', '');
addUpdate($div_update, '2022-11-11', 0, 1, '/project/updates.html', 'Updates');
addUpdate($div_update, '2022-11-09', 0, 1, '/index.html', 'Front page');
addUpdate($div_update, '2022-11-05', -1, -1, '', '', 'Beginning of the project. Resgistration of the domain pildem.org.');

$div_update['en'] .= "</ul>";




$body .= printH2($h2_code);
$body .= printSection($div_code);

$body .= printH2($h2_content);
$body .= printSection($div_stars);
$body .= printSection($div_update);

$body .= printSection($div_section_project);

include('include/page.php');
