<?php
include_once('section/project.php');
$h1['en'] = 'Project: Pilgrimage for Democracy and Social Justice';

$snp['description'] = "Description of the project, contributing tips, etc.";
$snp['image'] = "/copyrighted/marvin-meyer-SYTO3xs06fU.1200-630.jpg";


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>The following pages introduce the <em>Pilgrimage for Democracy and Social Justice</em> project.</p>
	HTML;

$h2_Presentation = newH2();
$h2_Presentation['en'] = 'Presentation';

$h2_Community = newH2();
$h2_Community['en'] = 'Community';

$h2_Technical_aspects = newH2();
$h2_Technical_aspects['en'] = 'Technical aspects';





$body .= printSection($div_introduction);

$body .= printH2($h2_Presentation);
$body .= printPageSection('menu.html');
$body .= printSection($div_section_updates);
$body .= printPageSection('project/copyright.html');

$body .= printH2($h2_Community);
$body .= printSection($div_section_participate);
$body .= printPageSection('project/standing_on_the_shoulders_of_giants.html');
$body .= printPageSection('project/wikipedia.html');
$body .= printPageSection('project/codeberg.html');

$body .= printH2($h2_Technical_aspects);
$body .= printPageSection('project/git.html');
$body .= printPageSection('project/development_website.html');

include('include/page.php');
