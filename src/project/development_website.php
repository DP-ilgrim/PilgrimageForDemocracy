<?php
include_once('section/project.php');
$h1['en'] = 'Development website';

$snp['description'] = "How to set up a development website on your own computer.";
$snp['image'] = "/copyrighted/domenico-loia-hGV2TfOh0ns.1200-630.jpg";

$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>Below are basic instructions on how to install a copy of the website <a href="https://pildem.org/">pildem.org</a> on your own computer.</p>
	HTML;


$div_codeberg = newSection('codeberg', '', 'Get help at Codeberg');
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>If the instructions in this page are unclear or incomplete,
	open a new issue in our issue tracker at Codeberg, and we shall assist you.</p>
	HTML;

$h2_source = newH2();
$h2_source['en'] = 'Source code';

$div_source = newSection();
$div_source['stars']   = -1;
$div_source['class'][] = '';
$div_source['en'] = <<<HTML
	<p>The first step is to get a full copy of the source code of the project.</p>

	<p>Clone the repository.</p>

	<code>$ git clone https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy.git</code>

	<p>Make note of where the sourced get cloned. The precise location will be needed when configuring Apache below.</p>

	<p>See also the page about the git workflow.</p>
	HTML;

$h2_apache_webserver = newH2();
$h2_apache_webserver['en'] = 'Apache web server';

$div_apache_webserver = newSection();
$div_apache_webserver['stars']   = -1;
$div_apache_webserver['class'][] = '';
$div_apache_webserver['en'] = <<<HTML
	<p>Install a web server. Here we recommend the Apache web server, but other web servers like nginx would do too.</p>

	<h3>Linux</h3>

	<p>Use the regular package manager tool used in your Linux distribution. Here is the command for Gentoo:</p>

	<code># emerge --ask www-servers/apache</code>

	<p>Edit the file "<em>/etc/hosts</em>" to register the local domain name under which you will run the website:</p>

	<code>127.0.0.1       pildem-development</code>

	<p>In the Apache vhost directory (on Gentoo: "<em>/etc/apache2/vhosts.d/</em>"),
	create a new configuration file (for example named: "<em>pildem.conf</em>") based on the following one.
	Adjust the settings based on the server name that you have configured, and the directory in which you installed the source code.</p>

	<code>&lt;VirtualHost *:80&gt;
		ServerAdmin webmaster@127.0.0.1
		ServerName pildem-development
		DirectoryIndex index.html
		DocumentRoot /home/theo/PilgrimageForDemocracy/http/
	&lt;/VirtualHost&gt;
	</code>

	<h3>Windows</h3>

	<p>Check the following official documentation for Apache.
	You can also find many handy tutorials on the internet by searching "<em>install Apache on Windows</em>".</p>

	<ul>
	<li><a href="https://httpd.apache.org/docs/2.4/platform/windows.html">Using Apache HTTP Server on Microsoft Windows</a></li>
	</ul>
	HTML;

$h2_php = newH2();
$h2_php['en'] = 'PHP';

$div_php = newSection();
$div_php['stars']   = -1;
$div_php['class'][] = '';
$div_php['en'] = <<<HTML
	<h3>Linux</h3>

	<p>Use the regular package manager tool used in your Linux distribution. Here is the command for Gentoo:</p>

	<code># emerge --ask dev-lang/php</code>

	<h3>Windows</h3>

	<p>Check the following official documentation for Apache.
	You can also find many handy tutorials on the internet by searching "<em>install Apache on Windows</em>".</p>

	<ul>
	<li><a href="https://windows.php.net/download">Download PHP For Windows</a></li>
	</ul>
	HTML;



$body .= printSection($div_introduction);
$body .= printSection($div_codeberg);

$body .= printH2($h2_source);
$body .= printSection($div_source);
$body .= printSection($div_section_git);

$body .= printH2($h2_apache_webserver);
$body .= printSection($div_apache_webserver);

$body .= printH2($h2_php);
$body .= printSection($div_php);

$body .= printSection($div_section_project);


include('include/page.php');
