<?php
include_once('section/democracy.php');
$h1['en'] = 'Types of democracy';


//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See the $Pilgrimage five-levelled features of democracies.</p>

	<p>See also the ${'V-Dem Institute'}'s types of democracies.</p>
	HTML;



$div_wikipedia_Types_of_democracy = new WikipediaContentSection();
$div_wikipedia_Types_of_democracy->setTitleText('Types of democracy');
$div_wikipedia_Types_of_democracy->setTitleLink('https://en.wikipedia.org/wiki/Types_of_democracy');
$div_wikipedia_Types_of_democracy->content = <<<HTML
	<p>Types of democracy refers to pluralism of governing structures such as governments (local through to global)
	and other constructs like workplaces, families, community associations, and so forth.
	Types of democracy can cluster around values.</p>
	HTML;

$body .= printSection($div_stub);

$body .= $div_introduction->print();
$body .= printPageSection('decentralized_autonomous_organization.html');

$body .= $div_wikipedia_Types_of_democracy->print();


include('include/page.php');
