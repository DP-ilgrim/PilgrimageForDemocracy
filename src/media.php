<?php
include_once('section/media.php');
$h1['en'] = 'Media';

$snp['description'] = "Media and democracy";
$snp['image'] = "/copyrighted/brotin-biswas-518543.1200-630.jpg";

$h2_introduction = newH2();
$h2_introduction['en'] = 'Introduction';

$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>The quality of our knowledge of public matters is commensurate with the quality of the media that deliver us the information
	upon which we rely to create our own opinion of what is right and what is wrong, whom to vote for or against, etc.</p>

	<p>On one side, we support free speech, the plurality of opinions and systemic checks and balances,
	with the media acting as a counterweight to political and economic powers.<p>

	<p>On the other side, we must find remedies against the excesses of unrestrained free speech, fake news, manipulative media,
	money politics, and media controlled by powerful vested interests.</p>

	<p>We must find a way to strike a balance between these apparently contradictory injunctions.</p>

	<p>How can we, collectively, become media-savvy, foster reliable and trustworthy media, etc.?</p>
	HTML;





$div_wikipedia_media_democracy = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Media_democracy', 'Media democracy');
$div_wikipedia_media_democracy['stars']   = -1;
$div_wikipedia_media_democracy['class'][] = '';
$div_wikipedia_media_democracy['en'] = <<<HTML
	<p>Media democracy is a democratic approach to media studies that advocates for the reform of mass media
	to strengthen public service broadcasting and develop participation in alternative media and citizen journalism
	in order to create a mass media system that informs and empowers all members of society and enhances democratic values.</p>
	HTML;


$div_wikipedia_social_media_politics= newSection('wikipedia', 'https://en.wikipedia.org/wiki/Social_media_use_in_politics', 'Social media use in politics');
$div_wikipedia_social_media_politics['stars']   = -1;
$div_wikipedia_social_media_politics['class'][] = '';
$div_wikipedia_social_media_politics['en'] = <<<HTML
	<p>Social media use in politics refers to the use of online social media platforms in political processes and activities.</p>
	HTML;


$div_wikipedia_center_media_democracy= newSection('wikipedia', 'https://en.wikipedia.org/wiki/Center_for_Media_and_Democracy', 'Center for Media and Democracy');
$div_wikipedia_center_media_democracy['stars']   = -1;
$div_wikipedia_center_media_democracy['class'][] = '';
$div_wikipedia_center_media_democracy['en'] = <<<HTML
	<p>The Center for Media and Democracy (CMD) is a progressive nonprofit watchdog and advocacy organization based in Madison, Wisconsin.</p>
	HTML;


$div_wikipedia_media_ethics= newSection('wikipedia', 'https://en.wikipedia.org/wiki/Media_ethics', 'Media ethics');
$div_wikipedia_media_ethics['stars']   = -1;
$div_wikipedia_media_ethics['class'][] = '';
$div_wikipedia_media_ethics['en'] = <<<HTML
	<p>Media ethics is the subdivision dealing with the specific ethical principles and standards of media, including broadcast media, film, theatre, the arts, print media and the internet.
	Media ethics promotes and defends values such as a universal respect for life and the rule of law and legality.</p>
	HTML;


$div_wikipedia_schuman_center_media_democracy= newSection('wikipedia', 'https://en.wikipedia.org/wiki/Schumann_Center_for_Media_and_Democracy', 'Schumann Center for Media and Democracy');
$div_wikipedia_media_deschuman_center_media_democracy['stars']   = -1;
$div_wikipedia_schuman_center_media_democracy['class'][] = '';
$div_wikipedia_schuman_center_media_democracy['en'] = <<<HTML
	<p>The Schumann Center for Media and Democracy was established in 1961, by Florence Ford and John J. Schumann Jr.
	The foundation states that its purpose is to renew the democratic process through cooperative acts of citizenship, especially as they apply to governance, and the environment.</p>
	HTML;





$body .= printSection($div_stub);

$body .= printH2($h2_introduction);
$body .= printSection($div_introduction);

$body .= printSection($div_section_foreign_influence_local_media);
$body .= printSection($div_section_reporters_without_borders);

$body .= printPageSection('information_overload.html');
$body .= printPageSection('ground_news.html');

$body .= printSection($div_wikipedia_media_democracy);
$body .= printSection($div_wikipedia_social_media_politics);
$body .= printSection($div_wikipedia_media_ethics);
$body .= printSection($div_wikipedia_center_media_democracy);
$body .= printSection($div_wikipedia_schuman_center_media_democracy);

include('include/page.php');
