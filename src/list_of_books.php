<?php
include_once('section/project.php');
$h1['en'] = 'List of books';


$snp['description'] = "Books about democracy, social justice, history, global issues...";
//$snp['image'] = "/copyrighted/";


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Books about democracy, social justice, history, global issues...</p>
	HTML;


$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>List</h3>

	<ul>
		<li>${'The Art of Thinking Clearly'}</li>
		<li>${'The Remains of the Day'}</li>
		<li>${'Twilight of Democracy'}</li>
	</ul>
	HTML;

$body .= printPageSection('lists.html');

$body .= $div_introduction->print();
$body .= $div_list->print();

// Featured
$body .= printPageSection('the_remains_of_the_day.html');


include('include/page.php');
