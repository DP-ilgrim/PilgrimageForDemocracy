<?php
include_once('section/global_issues.php');
$h1['en'] = 'Peaceful resistance in times of war';

$snp['description'] = "Stories of peaceful resistance to bellicose autocratic regimes.";
$snp['image'] = "/copyrighted/miha-rekar-7B_u675zmLU.1200-630.jpg";


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>Throughout history and across the globe, people have demonstrated against war.
	It takes however a different kind of courage to do so when living under an autocratic regime
	that is hell-bent on pursuing a path of oppression and war.</p>
	HTML;

$div_codeberg = newSection('codeberg', '31', 'Examples of anti-war peaceful resistance');
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>List of examples of anti-war resistance through time and across nations
	published by an anti-war feminist group.</p>
	HTML;

$h2_Stories_of_peaceful_resistance = newH2();
$h2_Stories_of_peaceful_resistance['en'] = 'Stories of peaceful resistance';


$div_Russia_occupied_Ukraine = newSection();
$div_Russia_occupied_Ukraine['stars']   = -1;
$div_Russia_occupied_Ukraine['class'][] = '';
$div_Russia_occupied_Ukraine['en'] = <<<HTML
	<h3>Russia occupied Ukraine</h3>

	<p>Ever since Russia launched its full scale invasion of Ukraine,
	the Ukrainian population in territories occupied by Russia have been resisting the invaders.
	In 2022, the resistance took the form of large scale protests.
	As Russia increased its oppressive measures, including arrests, torture and deportation,
	Ukrainian resistance took more subtle and covert forms,
	like painting or leaving Ukrainian symbols in public places under the cover of the night.</p>
	HTML;

$div_wikipedia_2022_protests_in_Russian_occupied_Ukraine = newSection('wikipedia', 'https://en.wikipedia.org/wiki/2022_protests_in_Russian-occupied_Ukraine',
                                                                                   '2022 protests in Russian occupied Ukraine');
$div_wikipedia_2022_protests_in_Russian_occupied_Ukraine['stars']   = -1;
$div_wikipedia_2022_protests_in_Russian_occupied_Ukraine['class'][] = '';
$div_wikipedia_2022_protests_in_Russian_occupied_Ukraine['en'] = <<<HTML
	<p>During the 2022 Russian invasion of Ukraine and the resulting Russian occupation of multiple Ukrainian towns and cities,
	numerous cases of non-violent resistance against the invasion took place.
	Local residents organised protests against the invasion and blocked the movement of Russian military equipment.
	The Russian military dispersed the protests, sometimes with live fire, injuring many and killing some.
	Most of the large protests ended in March.</p>
	HTML;


$div_Russia_under_Putin = newSection();
$div_Russia_under_Putin['stars']   = -1;
$div_Russia_under_Putin['class'][] = '';
$div_Russia_under_Putin['en'] = <<<HTML
	<h3>Russia under Putin</h3>

	<p>
	</p>
	HTML;

$div_wikipedia_Anti_war_protests_in_Russia_2022_present = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Anti-war_protests_in_Russia_(2022–present)',
                                                                                  'Anti war protests in Russia 2022 present');
$div_wikipedia_Anti_war_protests_in_Russia_2022_present['stars']   = -1;
$div_wikipedia_Anti_war_protests_in_Russia_2022_present['class'][] = '';
$div_wikipedia_Anti_war_protests_in_Russia_2022_present['en'] = <<<HTML
	<p>Following the Russian invasion of Ukraine on 24 February 2022, anti-war demonstrations and protests broke out across Russia.
	The protests have been met with widespread repression by the Russian authorities.</p>
	HTML;

$div_wikipedia_Feminist_Anti_War_Resistance = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Feminist_Anti-War_Resistance', 'Feminist Anti War Resistance');
$div_wikipedia_Feminist_Anti_War_Resistance['stars']   = -1;
$div_wikipedia_Feminist_Anti_War_Resistance['class'][] = '';
$div_wikipedia_Feminist_Anti_War_Resistance['en'] = <<<HTML
	<p>Feminist Anti-War Resistance is a group of Russian feminists founded in February 2022
	to protest against the 2022 Russian invasion of Ukraine.</p>
	HTML;





$div_Nazi_Germany = newSection();
$div_Nazi_Germany['stars']   = -1;
$div_Nazi_Germany['class'][] = '';
$div_Nazi_Germany['en'] = <<<HTML
	<h3>Nazi Germany</h3>

	<p>
	</p>
	HTML;


$div_wikipedia_White_Rose = newSection('wikipedia', 'https://en.wikipedia.org/wiki/White_Rose', 'White Rose');
$div_wikipedia_White_Rose['stars']   = -1;
$div_wikipedia_White_Rose['class'][] = '';
$div_wikipedia_White_Rose['en'] = <<<HTML
	<p>The White Rose was a non-violent, intellectual resistance group in Nazi Germany which was led by five students and one professor at the University of Munich.
	The group conducted an anonymous leaflet and graffiti campaign that called for active opposition to the Nazi regime.
	Their activities started in Munich on 27 June 1942; they ended with the arrest of the core group by the Gestapo on 18 February 1943.</p>
	HTML;

$div_wikipedia_Hans_and_Sophie_Scholl = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Hans_and_Sophie_Scholl', 'Hans and Sophie Scholl');
$div_wikipedia_Hans_and_Sophie_Scholl['stars']   = -1;
$div_wikipedia_Hans_and_Sophie_Scholl['class'][] = '';
$div_wikipedia_Hans_and_Sophie_Scholl['en'] = <<<HTML
	<p>Hans and Sophie Scholl were a brother and sister who were members of the White Rose,
	a student group in Munich that was active in the non-violent resistance movement in Nazi Germany,
	especially in distributing flyers against the war and the dictatorship of Adolf Hitler.
	In post-war Germany, Hans and Sophie Scholl are recognized as symbols of German resistance against the totalitarian Nazi regime.</p>
	HTML;




$h2_Other_resources = newH2();
$h2_Other_resources['en'] = 'Other resources';



$div_wikipedia_Nonviolent_resistance = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Nonviolent_resistance', 'Nonviolent resistance');
$div_wikipedia_Nonviolent_resistance['stars']   = -1;
$div_wikipedia_Nonviolent_resistance['class'][] = '';
$div_wikipedia_Nonviolent_resistance['en'] = <<<HTML
	<p>Nonviolent resistance, or nonviolent action, sometimes called civil resistance,
	is the practice of achieving goals such as social change through symbolic protests, civil disobedience,
	economic or political noncooperation, satyagraha, constructive program, or other methods,
	while refraining from violence and the threat of violence.</p>
	HTML;

$div_wikipedia_Anti_war_movement = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Anti-war_movement', 'Anti war movement');
$div_wikipedia_Anti_war_movement['stars']   = -1;
$div_wikipedia_Anti_war_movement['class'][] = '';
$div_wikipedia_Anti_war_movement['en'] = <<<HTML
	<p>Anti-war activists work through protest and other grassroots means to attempt to pressure a government (or governments)
	to put an end to a particular war or conflict or to prevent it in advance.</p>
	HTML;

$div_wikipedia_List_of_peace_activists = newSection('wikipedia', 'https://en.wikipedia.org/wiki/List_of_peace_activists', 'List of peace activists');
$div_wikipedia_List_of_peace_activists['stars']   = -1;
$div_wikipedia_List_of_peace_activists['class'][] = '';
$div_wikipedia_List_of_peace_activists['en'] = <<<HTML
	<p>This list of peace activists includes people who have proactively advocated
	diplomatic, philosophical, and non-military resolution of major territorial or ideological disputes
	through nonviolent means and methods.</p>
	HTML;

$body .= printSection($div_section_global_issues);

$body .= printSection($div_stub);

$body .= printSection($div_introduction);
$body .= printSection($div_codeberg);

$body .= printH2($h2_Stories_of_peaceful_resistance);

$body .= printSection($div_Russia_occupied_Ukraine);
$body .= printSection($div_wikipedia_2022_protests_in_Russian_occupied_Ukraine);

$body .= printSection($div_Russia_under_Putin);
$body .= printSection($div_wikipedia_Anti_war_protests_in_Russia_2022_present);
$body .= printSection($div_wikipedia_Feminist_Anti_War_Resistance);

$body .= printSection($div_Nazi_Germany);
$body .= printSection($div_wikipedia_Hans_and_Sophie_Scholl);
$body .= printSection($div_wikipedia_White_Rose);

$body .= printH2($h2_Other_resources);
$body .= printSection($div_wikipedia_List_of_peace_activists);
$body .= printSection($div_wikipedia_Anti_war_movement);
$body .= printSection($div_wikipedia_Nonviolent_resistance);

include('include/page.php');
