<?php
include_once('section/institutions.php');
$h1['en'] = 'Institutions';

$snp['description'] = "The pillars of democracy";
$snp['image'] = "/copyrighted/colin-lloyd-_JEiyOfC2y8.1200-630.jpg";


$div_institutions = newSection();
$div_institutions['stars']   = -1;
$div_institutions['class'][] = '';
$div_institutions['en'] = <<<HTML
	<p>Healthy institutions are the most critical parts to safeguard democracy.</p>

	<p>Topics to be covered include:</p>

	<ul>
	<li>Branches of powers</li>
	<li>Separation of powers</li>
	<li>Electoral process and electoral institutions</li>
	<li>Corruption</li>
	<li>Judicial independence</li>
	<li>Accountability</li>
	</ul>
	HTML;

$body .= printSection($div_stub);
$body .= printSection($div_institutions);

$body .= printSection($div_section_constitution);
$body .= printPageSection('politics.html');
$body .= printPageSection('corruption.html');
$body .= printPageSection('lawmaking.html');
$body .= printPageSection('world_future_council.html');
$body .= printPageSection('professionalism_without_elitism.html');

include('include/page.php');
