<?php
include_once('section/world.php');

$h1['en'] = 'Democracy and social justice in the world';


$body .= printPageSection('lists.html');

$body .= printSection($div_section_united_nations);
$body .= printSection($div_section_osce);

$body .= printSection($div_section_costa_rica);
$body .= printSection($div_section_prc_china);
$body .= printSection($div_section_georgia);
$body .= printSection($div_section_iran);
$body .= printSection($div_section_russia);
$body .= printSection($div_section_ukraine);


$div_other_countries = newSection();
$div_other_countries['stars']   = -1;
$div_other_countries['class'][] = '';
$div_other_countries['en'] = <<<HTML
	<p>Other countries:</p>

	<ul>
	<li><a href="/germany.html">Germany</a></li>
	<li><a href="/mali.html">Mali</a></li>
	<li><a href="/philippines.html">Philippines</a></li>
	<li>$Taiwan</li>
	<li><a href="/turkey.html">Turkey</a></li>
	</ul>
	HTML;

$body .= printSection($div_other_countries);

include('include/page.php');
