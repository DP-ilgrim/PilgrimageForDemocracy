<?php
include_once('section/features_of_democracy.php');
$h1['en'] = 'Democracy';



//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";

//$r1 = newRef('https://www.example.com/', 'Title');
//$r1 = newRef('', '');


$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Democracy = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Democracy', 'Democracy');
$div_wikipedia_Democracy['stars']   = -1;
$div_wikipedia_Democracy['class'][] = '';
$div_wikipedia_Democracy['en'] = <<<HTML
	<p>Democracy is a form of government in which the people have the authority to deliberate and decide legislation ("direct democracy"),
	or to choose governing officials to do so ("representative democracy").
	Who is considered part of "the people" and how authority is shared among or delegated by the people has changed over time
	and at different rates in different countries.
	Features of democracy often include freedom of assembly, association, property rights, freedom of religion and speech, citizenship,
	consent of the governed, voting rights, freedom from unwarranted governmental deprivation of the right to life and liberty, and minority rights.</p>
	HTML;

$body .= printSection($div_stub);
$body .= printSection($div_introduction);

$body .= printSection($div_section_what_is_democracy_primary_feature);
$body .= printSection($div_section_what_is_democracy_secondary_feature);
$body .= printSection($div_section_what_is_democracy_tertiary_feature);
$body .= printSection($div_section_what_is_democracy_quaternary_feature);
$body .= printSection($div_section_what_is_democracy_quinary_feature);
$body .= printPageSection('types_of_democracy.html');
$body .= printSection($div_wikipedia_Democracy);


include('include/page.php');
