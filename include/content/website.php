<?php
require_once('include/content/section.php');

class WebsiteContentSection extends ContentSection {
	public function __construct($country = '') {
		$this->classes[] = 'website';
		$this->title_classes .= ' website ';
		parent::__construct();
		$this->stars(NULL);
	}

	protected function printLogo() {
			return '<i class="section-icon share square large orange icon"></i>';
	}
}
