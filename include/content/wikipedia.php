<?php
require_once('include/content/section.php');

class WikipediaContentSection extends ContentSection {
	public function __construct($country = '') {
		$this->classes[] = 'wikipedia';
		$this->title_classes .= ' wikipedia ';
		parent::__construct();
		$this->stars(NULL);
	}

	protected function printLogo() {
			return '<i class="wikipedia w large icon"></i>';
	}
}
