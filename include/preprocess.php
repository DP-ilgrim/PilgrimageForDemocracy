<?php
require_once('include/preprocess_index.php');

function update_preprocess_index() {
	global $preprocess_index_src;
	ksort ($preprocess_index_src);

	$content  = "<?php\n\n\n";
	$content .= "\$preprocess_index_src = array(\n";
	// ["src/taiwan.php"] => Array (
	//                     ["dest"] => "http/taiwan.html",
	// )

	foreach ($preprocess_index_src as $src => $data) {
		$content .= "\t'${src}' => array(\n";
			$content .= "\t\t'dest'             => '${data["dest"]}',\n";
		$content .= "\t),\n";
	}
	$content .= ");\n\n\n";

	global $preprocess_index_dest;
	ksort ($preprocess_index_dest);
	$content .= "\$preprocess_index_dest = array(\n";
	foreach ($preprocess_index_dest as $dest => $data) {
		$content .= "\t'${dest}' => array(\n";
		foreach ($data as $key => $value) {
			$content .= "\t\t'${key}'\t => '${value}',\n";
		}
		$content .= "\t),\n";
	}
	$content .= ");\n";

	file_put_contents('include/preprocess_index.php', $content);
}

function create_auto_keyword($dest) {
	global $preprocess_index_dest;
	$out = '';

	$item     = $preprocess_index_dest[$dest];
	$keyword  = $item['keyword'];
	if (strstr($keyword, ' ')) {
		$variable = str_replace(' ', '_', $keyword);
		$variable = str_replace('-', '_', $variable);
		// e.g.: $Freedom_House = 'Freedom House';
		$out .= "\$${variable} = '${keyword}';\n";
		// e.g.: $$Freedom_House = '<a href="/freedom_house.html">Freedom House</a>';
		$out .= "\$\$${variable} = '<a href=\\'/${dest}\\'>${keyword}</a>';\n";
	}
	else {
		$variable = str_replace('-', '_', $keyword);
		// e.g.: $politics = '<a href='/politics.html'>politics</a>';
		$out .= "\$${variable} = '<a href=\\'/${dest}\\'>${keyword}</a>';\n";
	}
	return $out;
}
