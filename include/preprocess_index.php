<?php


$preprocess_index_src = array(
	'src/anne_applebaum.php' => array(
		'dest'             => 'http/anne_applebaum.html',
	),
	'src/beliefs.php' => array(
		'dest'             => 'http/beliefs.html',
	),
	'src/china_and_taiwan.php' => array(
		'dest'             => 'http/china_and_taiwan.html',
	),
	'src/chinese_expansionism.php' => array(
		'dest'             => 'http/chinese_expansionism.html',
	),
	'src/constitution.php' => array(
		'dest'             => 'http/constitution.html',
	),
	'src/corruption.php' => array(
		'dest'             => 'http/corruption.html',
	),
	'src/costa_rica.php' => array(
		'dest'             => 'http/costa_rica.html',
	),
	'src/cyberwarfare.php' => array(
		'dest'             => 'http/cyberwarfare.html',
	),
	'src/decentralized_autonomous_organization.php' => array(
		'dest'             => 'http/decentralized_autonomous_organization.html',
	),
	'src/democracy.php' => array(
		'dest'             => 'http/democracy.html',
	),
	'src/democracy_under_attack.php' => array(
		'dest'             => 'http/democracy_under_attack.html',
	),
	'src/democracy_wip.php' => array(
		'dest'             => 'http/democracy_wip.html',
	),
	'src/denise_dresser.php' => array(
		'dest'             => 'http/denise_dresser.html',
	),
	'src/duverger_france_alternance_danger.php' => array(
		'dest'             => 'http/duverger_france_alternance_danger.html',
	),
	'src/duverger_law.php' => array(
		'dest'             => 'http/duverger_law.html',
	),
	'src/duverger_syndrome.php' => array(
		'dest'             => 'http/duverger_syndrome.html',
	),
	'src/duverger_syndrome_alternance.php' => array(
		'dest'             => 'http/duverger_syndrome_alternance.html',
	),
	'src/duverger_syndrome_duality.php' => array(
		'dest'             => 'http/duverger_syndrome_duality.html',
	),
	'src/duverger_syndrome_lack_choice.php' => array(
		'dest'             => 'http/duverger_syndrome_lack_choice.html',
	),
	'src/duverger_syndrome_negative_campaigning.php' => array(
		'dest'             => 'http/duverger_syndrome_negative_campaigning.html',
	),
	'src/duverger_syndrome_party_politics.php' => array(
		'dest'             => 'http/duverger_syndrome_party_politics.html',
	),
	'src/duverger_taiwan_2000_2004.php' => array(
		'dest'             => 'http/duverger_taiwan_2000_2004.html',
	),
	'src/duverger_usa_19_century.php' => array(
		'dest'             => 'http/duverger_usa_19_century.html',
	),
	'src/fair_share.php' => array(
		'dest'             => 'http/fair_share.html',
	),
	'src/foreign_influence_local_media.php' => array(
		'dest'             => 'http/foreign_influence_local_media.html',
	),
	'src/freedom_house.php' => array(
		'dest'             => 'http/freedom_house.html',
	),
	'src/freedom_of_speech.php' => array(
		'dest'             => 'http/freedom_of_speech.html',
	),
	'src/georgia.php' => array(
		'dest'             => 'http/georgia.html',
	),
	'src/germany.php' => array(
		'dest'             => 'http/germany.html',
	),
	'src/global_issues.php' => array(
		'dest'             => 'http/global_issues.html',
	),
	'src/global_natural_resources.php' => array(
		'dest'             => 'http/global_natural_resources.html',
	),
	'src/global_peace_index.php' => array(
		'dest'             => 'http/global_peace_index.html',
	),
	'src/ground_news.php' => array(
		'dest'             => 'http/ground_news.html',
	),
	'src/hunger.php' => array(
		'dest'             => 'http/hunger.html',
	),
	'src/index.php' => array(
		'dest'             => 'http/index.html',
	),
	'src/information_overload.php' => array(
		'dest'             => 'http/information_overload.html',
	),
	'src/institute_for_economics_and_peace.php' => array(
		'dest'             => 'http/institute_for_economics_and_peace.html',
	),
	'src/institutions.php' => array(
		'dest'             => 'http/institutions.html',
	),
	'src/integrity.php' => array(
		'dest'             => 'http/integrity.html',
	),
	'src/iran.php' => array(
		'dest'             => 'http/iran.html',
	),
	'src/justice.php' => array(
		'dest'             => 'http/justice.html',
	),
	'src/karoline_wiesner.php' => array(
		'dest'             => 'http/karoline_wiesner.html',
	),
	'src/lawmaking.php' => array(
		'dest'             => 'http/lawmaking.html',
	),
	'src/list_of_books.php' => array(
		'dest'             => 'http/list_of_books.html',
	),
	'src/list_of_indices.php' => array(
		'dest'             => 'http/list_of_indices.html',
	),
	'src/list_of_movies.php' => array(
		'dest'             => 'http/list_of_movies.html',
	),
	'src/list_of_organisations.php' => array(
		'dest'             => 'http/list_of_organisations.html',
	),
	'src/list_of_people.php' => array(
		'dest'             => 'http/list_of_people.html',
	),
	'src/lists.php' => array(
		'dest'             => 'http/lists.html',
	),
	'src/living_together.php' => array(
		'dest'             => 'http/living_together.html',
	),
	'src/mali.php' => array(
		'dest'             => 'http/mali.html',
	),
	'src/maria_ressa.php' => array(
		'dest'             => 'http/maria_ressa.html',
	),
	'src/media.php' => array(
		'dest'             => 'http/media.html',
	),
	'src/menu.php' => array(
		'dest'             => 'http/menu.html',
	),
	'src/osce.php' => array(
		'dest'             => 'http/osce.html',
	),
	'src/peaceful_resistance_in_times_of_war.php' => array(
		'dest'             => 'http/peaceful_resistance_in_times_of_war.html',
	),
	'src/philippines.php' => array(
		'dest'             => 'http/philippines.html',
	),
	'src/political_discourse.php' => array(
		'dest'             => 'http/political_discourse.html',
	),
	'src/politics.php' => array(
		'dest'             => 'http/politics.html',
	),
	'src/prc_china.php' => array(
		'dest'             => 'http/prc_china.html',
	),
	'src/primary_features_of_democracy.php' => array(
		'dest'             => 'http/primary_features_of_democracy.html',
	),
	'src/professionalism_without_elitism.php' => array(
		'dest'             => 'http/professionalism_without_elitism.html',
	),
	'src/project/codeberg.php' => array(
		'dest'             => 'http/project/codeberg.html',
	),
	'src/project/copyright.php' => array(
		'dest'             => 'http/project/copyright.html',
	),
	'src/project/development_website.php' => array(
		'dest'             => 'http/project/development_website.html',
	),
	'src/project/git.php' => array(
		'dest'             => 'http/project/git.html',
	),
	'src/project/index.php' => array(
		'dest'             => 'http/project/index.html',
	),
	'src/project/participate.php' => array(
		'dest'             => 'http/project/participate.html',
	),
	'src/project/standing_on_the_shoulders_of_giants.php' => array(
		'dest'             => 'http/project/standing_on_the_shoulders_of_giants.html',
	),
	'src/project/updates.php' => array(
		'dest'             => 'http/project/updates.html',
	),
	'src/project/wikipedia.php' => array(
		'dest'             => 'http/project/wikipedia.html',
	),
	'src/reporters_without_borders.php' => array(
		'dest'             => 'http/reporters_without_borders.html',
	),
	'src/rolf_dobelli.php' => array(
		'dest'             => 'http/rolf_dobelli.html',
	),
	'src/russia.php' => array(
		'dest'             => 'http/russia.html',
	),
	'src/secondary_features_of_democracy.php' => array(
		'dest'             => 'http/secondary_features_of_democracy.html',
	),
	'src/staffan_lindberg.php' => array(
		'dest'             => 'http/staffan_lindberg.html',
	),
	'src/taiwan.php' => array(
		'dest'             => 'http/taiwan.html',
	),
	'src/taiwan_ukraine_relations.php' => array(
		'dest'             => 'http/taiwan_ukraine_relations.html',
	),
	'src/taxes.php' => array(
		'dest'             => 'http/taxes.html',
	),
	'src/tertiary_features_of_democracy.php' => array(
		'dest'             => 'http/tertiary_features_of_democracy.html',
	),
	'src/the_art_of_thinking_clearly.php' => array(
		'dest'             => 'http/the_art_of_thinking_clearly.html',
	),
	'src/the_hidden_dimension_in_democracy.php' => array(
		'dest'             => 'http/the_hidden_dimension_in_democracy.html',
	),
	'src/the_remains_of_the_day.php' => array(
		'dest'             => 'http/the_remains_of_the_day.html',
	),
	'src/transnational_authoritarianism.php' => array(
		'dest'             => 'http/transnational_authoritarianism.html',
	),
	'src/turkey.php' => array(
		'dest'             => 'http/turkey.html',
	),
	'src/twilight_of_democracy.php' => array(
		'dest'             => 'http/twilight_of_democracy.html',
	),
	'src/types_of_democracy.php' => array(
		'dest'             => 'http/types_of_democracy.html',
	),
	'src/ukraine.php' => array(
		'dest'             => 'http/ukraine.html',
	),
	'src/united_nations.php' => array(
		'dest'             => 'http/united_nations.html',
	),
	'src/v_dem_institute.php' => array(
		'dest'             => 'http/v_dem_institute.html',
	),
	'src/world.php' => array(
		'dest'             => 'http/world.html',
	),
	'src/world_future_council.php' => array(
		'dest'             => 'http/world_future_council.html',
	),
);


$preprocess_index_dest = array(
	'anne_applebaum.html' => array(
		'include'	 => 'section/people.php',
		'name'	 => 'div_section_anne_applebaum',
		'keyword'	 => 'Anne Applebaum',
	),
	'beliefs.html' => array(
		'include'	 => 'section/society.php',
		'name'	 => 'div_section_beliefs',
		'keyword'	 => 'beliefs',
	),
	'china_and_taiwan.html' => array(
		'include'	 => 'section/world.php',
		'name'	 => 'div_section_china_and_taiwan',
		'keyword'	 => 'China and Taiwan',
	),
	'corruption.html' => array(
		'include'	 => 'section/institutions.php',
		'name'	 => 'div_section_corruption',
		'keyword'	 => 'corruption',
	),
	'cyberwarfare.html' => array(
		'include'	 => 'section/threats.php',
		'name'	 => 'div_section_cyberwarfare',
		'keyword'	 => 'cyberwarfare',
	),
	'decentralized_autonomous_organization.html' => array(
		'include'	 => 'section/democracy.php',
		'name'	 => 'div_section_decentralized_autonomous_organization',
		'keyword'	 => 'decentralized autonomous organization',
	),
	'democracy.html' => array(
		'include'	 => 'section/democracy.php',
		'name'	 => 'div_section_democracy',
		'keyword'	 => 'democracy',
	),
	'democracy_under_attack.html' => array(
		'include'	 => 'section/threats.php',
		'name'	 => 'div_section_democracy_under_attack',
		'keyword'	 => 'democracy under attack',
	),
	'denise_dresser.html' => array(
		'include'	 => 'section/people.php',
		'name'	 => 'div_section_denise_dresser',
		'keyword'	 => 'Denise Dresser',
	),
	'freedom_house.html' => array(
		'include'	 => 'section/giants.php',
		'name'	 => 'div_section_freedom_house',
		'keyword'	 => 'Freedom House',
	),
	'global_issues.html' => array(
		'include'	 => 'section/global_issues.php',
		'name'	 => 'div_section_global_issues',
		'keyword'	 => 'global issues',
	),
	'global_peace_index.html' => array(
		'include'	 => 'section/indices.php',
		'name'	 => 'div_section_global_peace_index',
		'keyword'	 => 'Global Peace Index',
	),
	'ground_news.html' => array(
		'include'	 => 'section/media.php',
		'name'	 => 'div_section_ground_news',
		'keyword'	 => 'Ground News',
	),
	'information_overload.html' => array(
		'include'	 => 'section/media.php',
		'name'	 => 'div_section_information_overload',
		'keyword'	 => 'information overload',
	),
	'institute_for_economics_and_peace.html' => array(
		'include'	 => 'section/institutions.php',
		'name'	 => 'div_section_institute_for_economics_and_peace',
		'keyword'	 => 'Institute for Economics and Peace',
	),
	'institutions.html' => array(
		'include'	 => 'section/institutions.php',
		'name'	 => 'div_section_institutions',
		'keyword'	 => 'institutions',
	),
	'karoline_wiesner.html' => array(
		'include'	 => 'section/people.php',
		'name'	 => 'div_section_karoline_wiesner',
		'keyword'	 => 'Karoline Wiesner',
	),
	'lawmaking.html' => array(
		'include'	 => 'section/institutions.php',
		'name'	 => 'div_section_lawmaking',
		'keyword'	 => 'lawmaking',
	),
	'list_of_books.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_list_of_books',
		'keyword'	 => 'list of books',
	),
	'list_of_indices.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_list_of_indices',
		'keyword'	 => 'list of indices',
	),
	'list_of_movies.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_list_of_movies',
		'keyword'	 => 'list of movies',
	),
	'list_of_organisations.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_list_of_organisations',
		'keyword'	 => 'list of organisations',
	),
	'list_of_people.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_list_of_people',
		'keyword'	 => 'list of people',
	),
	'lists.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_lists',
		'keyword'	 => 'lists',
	),
	'maria_ressa.html' => array(
		'include'	 => 'section/people.php',
		'name'	 => 'div_section_maria_ressa',
		'keyword'	 => 'Maria Ressa',
	),
	'media.html' => array(
		'include'	 => 'section/media.php',
		'name'	 => 'div_section_media',
		'keyword'	 => 'media',
	),
	'menu.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_menu',
		'keyword'	 => 'menu',
	),
	'philippines.html' => array(
		'include'	 => 'section/world.php',
		'name'	 => 'div_section_philippines',
		'keyword'	 => 'Philippines',
	),
	'political_discourse.html' => array(
		'include'	 => 'section/speech.php',
		'name'	 => 'div_section_political_discourse',
		'keyword'	 => 'political discourse',
	),
	'politics.html' => array(
		'include'	 => 'section/institutions.php',
		'name'	 => 'div_section_politics',
		'keyword'	 => 'politics',
	),
	'prc_china.html' => array(
		'include'	 => 'section/world.php',
		'name'	 => 'div_section_china',
		'keyword'	 => 'China',
	),
	'primary_features_of_democracy.html' => array(
		'include'	 => 'section/features_of_democracy.php',
		'name'	 => 'div_section_what_is_democracy_primary_feature',
		'keyword'	 => 'primary features of democracy',
	),
	'professionalism_without_elitism.html' => array(
		'include'	 => 'section/institutions.php',
		'name'	 => 'div_section_professionalism_without_elitism',
		'keyword'	 => 'professionalism without elitism',
	),
	'project/codeberg.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_codeberg',
		'keyword'	 => 'Codeberg',
	),
	'project/copyright.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_copyright',
		'keyword'	 => 'copyright',
	),
	'project/development_website.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_development_website',
		'keyword'	 => 'development website',
	),
	'project/git.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_git',
		'keyword'	 => 'git',
	),
	'project/index.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_project',
		'keyword'	 => 'project',
	),
	'project/standing_on_the_shoulders_of_giants.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_standing_on_the_shoulders_of_giants',
		'keyword'	 => 'standing on the shoulders of giants',
	),
	'project/wikipedia.html' => array(
		'include'	 => 'section/project.php',
		'name'	 => 'div_section_wikipedia',
		'keyword'	 => 'wikipedia',
	),
	'rolf_dobelli.html' => array(
		'include'	 => 'section/people.php',
		'name'	 => 'div_section_rolf_dobelli',
		'keyword'	 => 'Rolf Dobelli',
	),
	'russia.html' => array(
		'include'	 => 'section/world.php',
		'name'	 => 'div_section_russia',
		'keyword'	 => 'Russia',
	),
	'secondary_features_of_democracy.html' => array(
		'include'	 => 'section/features_of_democracy.php',
		'name'	 => 'div_section_what_is_democracy_secondary_feature',
		'keyword'	 => 'secondary features of democracy',
	),
	'staffan_lindberg.html' => array(
		'include'	 => 'section/people.php',
		'name'	 => 'div_section_staffan_lindberg',
		'keyword'	 => 'Staffan Lindberg',
	),
	'taiwan.html' => array(
		'include'	 => 'section/world.php',
		'name'	 => 'div_section_taiwan',
		'keyword'	 => 'Taiwan',
	),
	'taiwan_ukraine_relations.html' => array(
		'include'	 => 'section/world.php',
		'name'	 => 'div_section_taiwan_ukraine_relations',
		'keyword'	 => 'Taiwan–Ukraine relations',
	),
	'tertiary_features_of_democracy.html' => array(
		'include'	 => 'section/features_of_democracy.php',
		'name'	 => 'div_section_what_is_democracy_tertiary_feature',
		'keyword'	 => 'tertiary features of democracy',
	),
	'the_art_of_thinking_clearly.html' => array(
		'include'	 => 'section/books.php',
		'name'	 => 'div_section_the_art_of_thinking_clearly',
		'keyword'	 => 'The Art of Thinking Clearly',
	),
	'the_hidden_dimension_in_democracy.html' => array(
		'include'	 => 'section/pdf.php',
		'name'	 => 'div_section_the_hidden_dimension_in_democracy',
		'keyword'	 => 'The Hidden Dimension in Democracy',
	),
	'the_remains_of_the_day.html' => array(
		'include'	 => 'section/movies.php',
		'name'	 => 'div_section_the_remains_of_the_day',
		'keyword'	 => 'The Remains of the Day',
	),
	'transnational_authoritarianism.html' => array(
		'include'	 => 'section/threats.php',
		'name'	 => 'div_section_transnational_authoritarianism',
		'keyword'	 => 'transnational authoritarianism',
	),
	'twilight_of_democracy.html' => array(
		'include'	 => 'section/books.php',
		'name'	 => 'div_section_twilight_of_democracy',
		'keyword'	 => 'Twilight of Democracy',
	),
	'types_of_democracy.html' => array(
		'include'	 => 'section/democracy.php',
		'name'	 => 'div_section_types_of_democracy',
		'keyword'	 => 'types of democracy',
	),
	'ukraine.html' => array(
		'include'	 => 'section/world.php',
		'name'	 => 'div_section_ukraine',
		'keyword'	 => 'Ukraine',
	),
	'v_dem_institute.html' => array(
		'include'	 => 'section/giants.php',
		'name'	 => 'div_section_v_dem_institute',
		'keyword'	 => 'V-Dem Institute',
	),
	'world.html' => array(
		'include'	 => 'section/world.php',
		'name'	 => 'div_section_democracy_world',
		'keyword'	 => 'world',
	),
	'world_future_council.html' => array(
		'include'	 => 'section/institutions.php',
		'name'	 => 'div_section_world_future_council',
		'keyword'	 => 'World Future Council',
	),
);
