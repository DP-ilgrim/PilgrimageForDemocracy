<?php
require_once('include/content/freedom_house.php');
require_once('include/content/indices.php');
include_once('data/democracy_index.php');
include_once('data/press_freedom_index.php');
include_once('data/freedom_house/data.php');

$alternative_names = array(
	"Russian Federation" => "Russia",
	"Islamic Republic of Iran" => "Iran",
);

function printCountryIndices($country) {
	global $alternative_names;
	if (isset($arternatives_names[$country])) {
		$country = $arternatives_names[$country];
	}

	$out  = '';
	$out .= (new DemocracyIndexContentSection                   ($country))->print();
	$out .= (new PressFreedomIndexContentSection                ($country))->print();
	$out .= (new FreedomHouseGlobalFreedomContentSection        ($country))->print();
	$out .= (new FreedomHouseGlobalFreedomIndexContentSection   ($country))->print();
	$out .= (new FreedomHouseInternetFreedomContentSection      ($country))->print();
	$out .= (new FreedomHouseInternetFreedomIndexContentSection ($country))->print();

	return $out;
}
