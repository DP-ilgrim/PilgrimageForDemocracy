<?php
	include_once('init.php');
	include_once('header.php');
	include_once('footer.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php print_title() ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta name='viewport'   content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
	<link rel="stylesheet" href="<?php echo $url_prefix; ?>/semantic/semantic.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/style/style6.css" type="text/css" media="all" />

	<meta property="og:title"  content="<?php echo htmlspecialchars($h1['en']); ?>">
	<meta name="twitter:title" content="<?php echo htmlspecialchars($h1['en']); ?>">

	<meta name="description"         content="<?php echo htmlspecialchars($snp['description']); ?>">
	<meta property="og:description"  content="<?php echo htmlspecialchars($snp['description']); ?>">
	<meta name="twitter:description" content="<?php echo htmlspecialchars($snp['description']); ?>">

	<meta property="og:image"  content="<?php echo $url_prefix; ?><?php echo htmlspecialchars($snp['image']); ?>">
	<meta name="twitter:image" content="<?php echo $url_prefix; ?><?php echo htmlspecialchars($snp['image']); ?>">
	<meta name="twitter:card"  content="summary_large_image">
</head>

<body id="pagetop" class="">
	<svg width="0" height="0" aria-hidden="true" focusable="false" tabindex="-1">
		<symbol viewBox="0 0 24 24" id="icon-github"><path fill="#171515" d="M12 .3a12 12 0 0 0-3.8 23.38c.6.11.83-.26.83-.57L9 20.87c-3.34.73-4.04-1.41-4.04-1.41-.55-1.4-1.34-1.76-1.34-1.76-1.08-.74.09-.73.09-.73 1.2.09 1.84 1.24 1.84 1.24 1.07 1.83 2.8 1.3 3.49 1 .1-.78.42-1.31.76-1.61-2.67-.3-5.47-1.33-5.47-5.93 0-1.31.47-2.38 1.24-3.22a4.3 4.3 0 0 1 .12-3.18s1-.32 3.3 1.23a11.52 11.52 0 0 1 6 0c2.3-1.55 3.3-1.23 3.3-1.23a4.3 4.3 0 0 1 .12 3.18 4.64 4.64 0 0 1 1.24 3.22c0 4.6-2.81 5.62-5.48 5.92.43.37.81 1.1.81 2.22l-.01 3.3c0 .31.21.69.82.57A12 12 0 0 0 12 .3z"></path></symbol>
		<symbol viewBox="0 0 24 24" id="icon-google-scholar"><path fill="#4285f4" d="M12 19.3 0 9.5 12 0v19.3Z"></path><path fill="#356ac3" d="m12 19.3 12-9.8L12 0v19.3Z"></path><circle cx="12" cy="17" r="7" fill="#a0c3ff"></circle><path fill="#76a7fa" d="M5.7 14a7 7 0 0 1 12.6 0H5.7Z"></path></symbol>
		<symbol viewBox="0 0 24 24" id="icon-youtube"><path fill="#ed1f24" d="M21 24H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3Z"></path><path fill="#fff" d="M19.7 8.1a2 2 0 0 0-1.4-1.4C17 6.4 12 6.4 12 6.4s-5 0-6.3.3a2 2 0 0 0-1.4 1.4C4 9.4 4 12 4 12s0 2.6.3 3.9a2 2 0 0 0 1.4 1.4c1.3.3 6.3.3 6.3.3s5 0 6.3-.3a2 2 0 0 0 1.4-1.4c.3-1.3.3-3.9.3-3.9s0-2.6-.3-3.9Z"></path><path fill="#ed1f24" d="m10.4 14.4 4.1-2.4-4.1-2.4v4.8Z"></path></symbol>
	</svg>


<?php print_header() ?>

<?php print_h1() ?>

<?php print_wip() ?>

<?php if (!empty($body)) {echo $body;} ?>

<?php print_references() ?>

<?php print_footer() ?>


</body>
</html>
