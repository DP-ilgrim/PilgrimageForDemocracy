<?php
	include_once('settings.php');
	include_once('democracy_index.php');
	include_once('references.php');
	include_once('include/preprocess_index.php');
	require_once('include/content/all.php');
	require_once('templates/keywords.php');

$h1 = array(
	'id' => 'top',
	'class' => array('main-article'),
	'en' => 'Pilgrimage for Democracy',
);

$body = '';

// Social network preview
$snp = array(
	'description' => "Sowing the seeds of future peace",
	'image'       => '/copyrighted/hansjorg-keller-p7av1ZhKGBQ-1200-630.jpg',
);

$references = array();

function print_wip() {
	global $show_wip;
	if (!$show_wip) {
		return;
	}

	// Transform:
	//           src/file.php => wip/file.txt
	$file = $_SERVER['SCRIPT_NAME'];
	$file = substr($file, 0, -4);
	$file = substr($file, 4);
	$file = "wip/" . $file . ".txt";

	if (file_exists($file)) {
		echo '<pre style="white-space: pre-wrap;">';
		echo file_get_contents($file);
		echo '</pre>';
	}
}

function print_title() {
	global $h1;
	echo $h1['en'];
}

function print_h1() {
	global $h1;
	$class = join(' ', $h1['class']);

	echo '<h1 id="' . $h1['id'] . '" class="' . $class . '">';
	echo '<span lang="en">' . $h1['en'] . '</span>';
	echo '</h1>';
}

function newSection($type = NULL, $p1 = NULL, $p2 = NULL) {
	$section = array(
		'tag'   => 'div',
		'id'    => '',
		'class' => array('main-article'),
		'en'    => '',
		'stars' => -1,
			// -1: do not display.
			//  0: 4 grey stars   = not started.
			//  1: 1 red star     = stub.
			//  2: 2 orange stars = half written.
			//  3: 3 yellow stars = 1st draft completed / review / to complete.
			//  4: 4 blue stars  = almost complete.
			//  5: 5 green stars  = complete (but can be further improved if necessary).
	);

	if ($type == 'wikipedia') {
		$section['type']        = 'wikipedia';
		$section['link']['en']  = $p1;
		$section['title']['en'] = $p2;
		$section['class'][]     = 'wikipedia technical-section';
	}
	elseif ($type == 'codeberg') {
		$section['type']        = 'codeberg';
		$section['id']          = $p1;
		$section['title']['en'] = $p2;
		$section['class'][]     = 'codeberg technical-section';
	}

	return $section;
}


function newH2() {
	//fwrite(STDERR, "Deprecated use of newH2(). Use object h2HeaderContent instead.\n");
	$h2 = newSection();
	$h2['tag'] = 'h2';
	return $h2;
}

function printPageSection($page) {
	require('templates/keywords.php');
	global $preprocess_index_dest;
	if (!isset($preprocess_index_dest[$page])) {
		fwrite(STDERR, "Error: page section missing: ${page} \n");
		return '';
	}

	require($preprocess_index_dest[$page]['include']);
	$name = $preprocess_index_dest[$page]['name'];

	if (is_array($$name)) {
		//fwrite(STDERR, "[printPageSection] Deprecated use of array for: section '${name}', page '${page}'.\n");
		return printSection($$name);
	}
	if (is_object($$name)) {
		return $$name->print();
	}
	fwrite(STDERR, "[printPageSection] Error: section '${name}' for page '${page}' is not an object.\n");
	return '';
}

function printSection($section) {
	if (!is_array($section)) {
		fwrite(STDERR, "[printSection] Error: section is not an array: ${section} \n");
		return '';
	}

	$star_classes = array(
		-1 => "no-star-rating",
		0  => "star-rating zero-stars",
		1  => "star-rating one-star",
		2  => "star-rating two-stars",
		3  => "star-rating three-stars",
		4  => "star-rating four-stars",
		5  => "star-rating five-stars"
	);
	if (isset($star_classes[$section['stars']])) {
		$section['class'][] = $star_classes[$section['stars']];
	}


	$class = join(' ', $section['class']);

	$out  = "";
	$out .= "<{$section['tag']} id='{$section['id']}' class='$class'>";
	$out .= "<div lang='en'>" . printSectionStars($section['stars']);

	if (isset($section['type'])) {
		if ($section['type'] == 'wikipedia') {
			// Deprecated. Enable warning when the amount of leftovers is manageable.
			// fwrite(STDERR, "Deprecated use of array for \"{$section['title']['en']}\". Use object WikipediaContentSection instead.\n");
			$out .= '<i class="wikipedia w large icon"></i>';
			$out .= "<h3><a href='{$section['link']['en']}' class='wikipedia'>{$section['title']['en']}</a></h3>";
		}
		else if ($section['type'] == 'codeberg') {
			// fwrite(STDERR, "Deprecated use of array for \"{$section['title']['en']}\". Use object CodebergContentSection instead.\n");
			$out .= '<img class="ui tiny image codeberg" src="https://design.codeberg.org/logo-kit/icon_inverted.svg" width="26" height="26">';

			$out .= '<h3>';
			if ($section['id'] !== '') {
				$out .= "Issue #{$section['id']}: &nbsp;&nbsp;&nbsp;";
			}
			$out .= "<a href='https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/{$section['id']}' class='codeberg'>{$section['title']['en']}</a>";
			$out .= '</h3>';
		}
	}

	$out .= "{$section['en']} </div>";
	$out .= "</{$section['tag']}>";

	return $out;
}

function printStars($nb) {
	// fwrite(STDERR, "The function printStars() is deprecated. See ContentSection->printStars().\n");
	$out = '';
	switch ($nb) {
		case 0:
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 1:
			$out .= "<i class='star red     icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 2:
			$out .= "<i class='star orange  icon'></i>";
			$out .= "<i class='star orange  icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 3:
			$out .= "<i class='star yellow  icon'></i>";
			$out .= "<i class='star yellow  icon'></i>";
			$out .= "<i class='star yellow  icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 4:
			$out .= "<i class='star blue    icon'></i>";
			$out .= "<i class='star blue    icon'></i>";
			$out .= "<i class='star blue    icon'></i>";
			$out .= "<i class='star blue    icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 5:
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			break;
	}
	return $out;
}

function printSectionStars($nb) {
	// fwrite(STDERR, "The function printSectionStars() is deprecated. See ContentSection->printSectionStars().\n");
	$out = '';
	if ($nb >= 0) {
		$out .= "<div class='section-stars'>";
		$out .= printStars($nb);
		$out .= "</div>";
	}
	return $out;
}

function printH2($h2) {
	return printSection($h2);
}

function addUpdate(&$div_update, $date, $stars_from, $stars_to, $link, $title_en, $text_en = '') {
	$update_en  = '<li>';
	$update_en .= "<strong>" . formatDateEn($date) . "</strong> &nbsp; ";
	$update_en .= printStars($stars_from);
	$update_en .= $stars_to >= 0 ? "<i class='right arrow icon'></i>" : '';
	$update_en .= printStars($stars_to);
	$update_en .= empty($link) ? '' : "<a href='$link'>$title_en</a> ";
	$update_en .= $text_en;
	$update_en .= '</li>';
	$div_update['en'] .= $update_en;
}

function formatDateEn($date) {
	return date("Y F jS", strtotime($date));
}

$div_stars = newSection();
$div_stars['class'][] = 'technical-section';
$div_stars['en'] = <<<HTML
	<p>
	<i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Planned article that has not been published yet.
	<br>
	<i class='star red     icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Stub article, there is not much to read yet.
	<br>
	<i class='star orange  icon'></i><i class='star orange  icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Article in progress. It may be only half written.
	<br>
	<i class='star yellow  icon'></i><i class='star yellow  icon'></i><i class='star yellow  icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Article waiting for reader feedback, or in need of some improvement.
	<br>
	<i class='star blue   icon'></i><i class='star blue   icon'></i><i class='star blue   icon'></i><i class='star blue   icon'></i><i class='star outline icon'></i>
	Nearly completed article. It could still be further expanded and improved.
	<br>
	<i class='star green   icon'></i><i class='star green   icon'></i><i class='star green   icon'></i><i class='star green   icon'></i><i class='star green icon'></i>
	Completed article, albeit still subject to change.
	</p>
	HTML;

$div_stub = newSection();
$div_stub['stars']   = -1;
$div_stub['class'][] = 'notice stub technical-section';
$div_stub['en'] = <<<HTML
	<div><i class="exclamation triangle large red icon"></i></div>
	<p><em>This article is a stub.
	You can help by expanding it.
	You can <a href="/project/participate.html">join</a> and
	<a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues">suggest improvements or edits</a> to this page.
	</em></p>
	HTML;
