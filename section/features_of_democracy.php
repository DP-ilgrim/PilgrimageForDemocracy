<?php
include_once('include/init.php');

$div_section_what_is_democracy_primary_feature = newSection();
$div_section_what_is_democracy_primary_feature['stars'] = 3;
$div_section_what_is_democracy_primary_feature['en'] = <<<HTML
	<h3><a href="/primary_features_of_democracy.html">1: Primary features of democracy</a></h3>
	<p>This is the starting point in our journey towards a proper and complete understanding of what a democracy is.</p>
	<p>The etymological meaning is simple enough to comprehend, but the implications are easily overlooked.</p>
	<p>These primary features of democracy are fundamental.
	They are the basis upon which everything else is built.</p>
	<p>The primary flaws of democracy are:
		<span class="democracy flaw">individualism</span>,
		<span class="democracy flaw">selfishness</span>,
		<span class="democracy flaw">indifference</span>,
		<span class="democracy flaw">populism</span>.</p>
	<p>The primary virtues of democracy are:
		<span class="democracy virtue">freedom</span>,
		<span class="democracy virtue">peace</span>,
		<span class="democracy virtue">achievement</span>,
		<span class="democracy virtue">inspiration</span>,
		<span class="democracy virtue">aspiration</span>,
		<span class="democracy virtue">personal human development</span>,
		<span class="democracy virtue">personal spiritual development</span>.</p>
	HTML;

$div_section_what_is_democracy_secondary_feature = newSection();
$div_section_what_is_democracy_secondary_feature['stars'] = 4;
$div_section_what_is_democracy_secondary_feature['en'] = <<<HTML
	<h3><a href="/secondary_features_of_democracy.html">2: Secondary features of democracy</a></h3>
	<p>The primary features of democracy, if unmoderated and left unchecked, can lead to mob rule and the tyranny of the masses.
	The secondary features provide balance to the first ones.</p>
	<p>The secondary flaws of democracy are:
		<span class="democracy flaw">chaos</span>,
		<span class="democracy flaw">divisiveness</span>,
		<span class="democracy flaw">tribalism</span>,
		<span class="democracy flaw">party politics</span>,
		<span class="democracy flaw">blaming others</span>,
		<span class="democracy flaw">pride</span>,
		<span class="democracy flaw">exploitation</span>.</p>
	<p>The secondary virtues of democracy are:
		<span class="democracy virtue">order</span>,
		<span class="democracy virtue">solidarity</span>,
		<span class="democracy virtue">justice</span>,
		<span class="democracy virtue">compassion</span>,
		<span class="democracy virtue">humility</span>,
		<span class="democracy virtue">cooperation</span>.</p>
	HTML;

$div_section_what_is_democracy_tertiary_feature = newSection();
$div_section_what_is_democracy_tertiary_feature['stars'] = 0;
$div_section_what_is_democracy_tertiary_feature['en'] = <<<HTML
	<h3><a href="/tertiary_features_of_democracy.html">3: Tertiary features of democracy</a></h3>

	<p>The primary and secondary levels laid out the theoretical foundations, and stated the core values of democracies.
	The tertiary features of democracy are more about the concrete steps to make it work.
	We shall explore the core institutions, as well as the professionalism and practicalities required.</p>

	<p>The tertiary flaws of democracy are:
		<span class="democracy flaw">incompetence</span>,
		<span class="democracy flaw">corruption</span>,
		<span class="democracy flaw">cronyism</span>.</p>
	<p>The tertiary virtues of democracy are:
		<span class="democracy virtue">competence</span>,
		<span class="democracy virtue">professionalism</span>,
		<span class="democracy virtue">integrity</span>,
		<span class="democracy virtue">public service</span>.</p>
	HTML;

$div_section_what_is_democracy_quaternary_feature = newSection();
$div_section_what_is_democracy_quaternary_feature['stars'] = 0;
$div_section_what_is_democracy_quaternary_feature['en'] = <<<HTML
	<h3>4: Quaternary features of democracy</h3>
	HTML;

$div_section_what_is_democracy_quinary_feature = newSection();
$div_section_what_is_democracy_quinary_feature['stars'] = 0;
$div_section_what_is_democracy_quinary_feature['en'] = <<<HTML
	<h3>5: Quinary features of democracy</h3>
	HTML;

$div_section_democracy_wip = newSection();
$div_section_democracy_wip['stars']   = 3;
$div_section_democracy_wip['class'][] = '';
$div_section_democracy_wip['en'] = <<<HTML
	<h3><a href="/democracy_wip.html">6: Democracy — a Work in Progress.</a></h3>

	<p>Almost by definition, a democracy is a <em>work in progress</em>.
	The accronym <em>WIP</em> should be tagged after the name of each democratic country:
	<em>USA (WIP)</em>, <em>Germany (WIP)</em>, <em>France (WIP)</em>, etc.
	This article explains why.</p>
	HTML;

$div_section_democracy_soloist_choir = newSection();
$div_section_democracy_soloist_choir['stars']   = 0;
$div_section_democracy_soloist_choir['class'][] = '';
$div_section_democracy_soloist_choir['en'] = <<<HTML
	<h3>7: Democracy — the Soloist and the Choir</h3>
	HTML;

$div_section_democracy_saints_and_little_devils = newSection();
$div_section_democracy_saints_and_little_devils['stars'] = 0;
$div_section_democracy_saints_and_little_devils['en'] = <<<HTML
	<h3>8: Democracy — Saints and Little Devils</h3>
	HTML;
