<?php
include_once('include/init.php');

$div_section_freedom_house = newSection();
$div_section_freedom_house['stars']   = 2;
$div_section_freedom_house['class'][] = '';
$div_section_freedom_house['en'] = <<<HTML
	<h3><a href="/freedom_house.html">Freedom House</a></h3>

	<p>Freedom House is a non-profit organization group in Washington, D.C.
	advocating democracy, political freedom, and human rights.</p>
	HTML;

$div_section_v_dem_institute = new ContentSection();
$div_section_v_dem_institute->stars(0);
$div_section_v_dem_institute->content = <<<HTML
	<h3><a href="/v_dem_institute.html">V-Dem Institute</a></h3>

	<p>Varieties of Democracy (V-Dem) is a unique approach to conceptualizing and measuring democracy.</p>
	HTML;
