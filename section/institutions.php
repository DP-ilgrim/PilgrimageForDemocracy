<?php
include_once('include/init.php');

$div_section_institutions = newSection();
$div_section_institutions['stars']   = 0;
$div_section_institutions['class'][] = '';
$div_section_institutions['en'] = <<<HTML
	<h3><a href="/institutions.html">Institutions</a></h3>

	<p>Healthy institutions are the most critical parts to safeguard democracy.</p>
	HTML;

$div_section_constitution = newSection();
$div_section_constitution['stars']   = 1;
$div_section_constitution['class'][] = '';
$div_section_constitution['en'] = <<<HTML
	<h3><a href="/constitution.html">Constitution</a></h3>

	<p>A constitution sets the ground rules and provides stability for the democracy and resilience to subversion.</p>
	HTML;

$div_section_corruption = newSection();
$div_section_corruption['stars']   = 0;
$div_section_corruption['class'][] = '';
$div_section_corruption['en'] = <<<HTML
	<h3><a href="/corruption.html">Corruption</a></h3>

	<p>The continued failure of most countries to significantly control corruption
	is contributing to a crisis in democracy around the world.</p>
	HTML;

$div_section_lawmaking = newSection();
$div_section_lawmaking['stars']   = 0;
$div_section_lawmaking['class'][] = '';
$div_section_lawmaking['en'] = <<<HTML
	<h3><a href="/lawmaking.html">Lawmaking</a></h3>

	<p>
	</p>
	HTML;

$div_section_politics = newSection();
$div_section_politics['stars']   = 0;
$div_section_politics['class'][] = '';
$div_section_politics['en'] = <<<HTML
	<h3><a href="/politics.html">Politics</a></h3>

	<p>
	</p>
	HTML;

$div_section_professionalism_without_elitism = newSection();
$div_section_professionalism_without_elitism['stars']   = 1;
$div_section_professionalism_without_elitism['class'][] = '';
$div_section_professionalism_without_elitism['en'] = <<<HTML
	<h3><a href="/professionalism_without_elitism.html">Professionalism without Elitism</a></h3>

	<p>How to promote the idea of professionalism in politics, without falling into the trappings of elitism?</p>
	HTML;

$div_section_world_future_council = newSection();
$div_section_world_future_council['stars']   = 0;
$div_section_world_future_council['class'][] = '';
$div_section_world_future_council['en'] = <<<HTML
	<h3><a href="/world_future_council.html">World Future Council</a></h3>

	<p>The World Future Council (WFC) is a German non-profit foundation..
	It works to pass on a healthy and sustainable planet with just and peaceful societies to future generations.</p>
	HTML;

$div_section_institute_for_economics_and_peace = new ContentSection();
$div_section_institute_for_economics_and_peace->stars(0);
$div_section_institute_for_economics_and_peace->content = <<<HTML
	<h3><a href="/institute_for_economics_and_peace.html">Institute for Economics and Peace</a></h3>

	<p>Creating a paradigm shift in the way the world thinks about peace.</p>
	HTML;
