<?php
include_once('include/init.php');

$div_section_democracy_under_attack = newSection();
$div_section_democracy_under_attack['stars']   = 0;
$div_section_democracy_under_attack['class'][] = '';
$div_section_democracy_under_attack['en'] = <<<HTML
	<h3><a href="/democracy_under_attack.html">3: Democracy under attack</a></h3>

	<p>Established democracies are under attack from authoritarian regimes, as well as from within.</p>
	HTML;

$div_section_cyberwarfare = newSection();
$div_section_cyberwarfare['stars']   = 0;
$div_section_cyberwarfare['class'][] = '';
$div_section_cyberwarfare['en'] = <<<HTML
	<h3><a href="/cyberwarfare.html">Cyberwarfare</a></h3>

	<p>Both authoritarian regimes and democratic countries have active cyberwarfare units.</p>
	HTML;

$div_section_transnational_authoritarianism = newSection();
$div_section_transnational_authoritarianism['stars']   = 2;
$div_section_transnational_authoritarianism['class'][] = '';
$div_section_transnational_authoritarianism['en'] = <<<HTML
	<h3><a href="/transnational_authoritarianism.html">Transnational authoritarianism</a></h3>

	<p>Authoritarian regimes cross borders and repress refugees living in democratic countries.</p>
	HTML;
