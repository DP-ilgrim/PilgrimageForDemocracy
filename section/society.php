<?php
include_once('include/init.php');

$div_section_beliefs = newSection();
$div_section_beliefs['stars']   = 1;
$div_section_beliefs['class'][] = '';
$div_section_beliefs['en'] = <<<HTML
	<h3><a href="/beliefs.html">Beliefs</a></h3>

	<p>Beliefs from the point of view of democracy and social justice.</p>
	HTML;

$div_section_living_together= newSection();
$div_section_living_together['stars']   = 2;
$div_section_living_together['class'][] = '';
$div_section_living_together['en'] = <<<HTML
	<h3><a href="/living_together.html">Living Together</a></h3>

	<p>If we learn how to bridge our differences and learn how to live together,
	we can create a better society for all.
	Here is a collection of inspiring stories and ideas.</p>
	HTML;
