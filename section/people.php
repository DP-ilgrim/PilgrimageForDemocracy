<?php
include_once('include/init.php');

$div_section_staffan_lindberg = new ContentSection();
$div_section_staffan_lindberg->stars(0);
$div_section_staffan_lindberg->content = <<<HTML
	<h3><a href="/staffan_lindberg.html">Staffan Lindberg</a></h3>

	<p>Staffan Lindberg is a Swedish political scientist and Director of the ${'V-Dem Institute'}.</p>
	HTML;

$div_section_karoline_wiesner = new ContentSection();
$div_section_karoline_wiesner->stars(0);
$div_section_karoline_wiesner->content = <<<HTML
	<h3><a href="/karoline_wiesner.html">Karoline Wiesner</a></h3>

	<p>Professor of Complexity Science, University of Potsdam and a collaborator at the ${'V-Dem Institute'}.</p>
	HTML;

$div_section_rolf_dobelli = new ContentSection();
$div_section_rolf_dobelli->stars(0);
$div_section_rolf_dobelli->content = <<<HTML
	<h3><a href="/rolf_dobelli.html">Rolf Dobelli</a></h3>

	<p>
	</p>
	HTML;

$div_section_maria_ressa = new ContentSection();
$div_section_maria_ressa->stars(1);
$div_section_maria_ressa->content = <<<HTML
	<h3><a href="/maria_ressa.html">Maria Ressa</a></h3>

	<p>Filipino journalist and 2021 Nobel Peace Prize laureate.</p>
	HTML;

$div_section_anne_applebaum = new ContentSection();
$div_section_anne_applebaum->stars(0);
$div_section_anne_applebaum->content = <<<HTML
	<h3><a href="/anne_applebaum.html">Anne Applebaum</a></h3>

	<p>
	</p>
	HTML;

$div_section_denise_dresser = new ContentSection();
$div_section_denise_dresser->stars(1);
$div_section_denise_dresser->content = <<<HTML
	<h3><a href="/denise_dresser.html">Denise Dresser</a></h3>

	<p>
	</p>
	HTML;
