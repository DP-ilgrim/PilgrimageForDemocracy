<?php
include_once('include/init.php');

$div_section_freedom_of_speech = newSection();
$div_section_freedom_of_speech['stars']   = 0;
$div_section_freedom_of_speech['class'][] = '';
$div_section_freedom_of_speech['en'] = <<<HTML
	<h3><a href="/freedom_of_speech.html">Freedom of speech</a></h3>

	<p>
	</p>
	HTML;

$div_section_political_discourse = newSection();
$div_section_political_discourse['stars']   = 1;
$div_section_political_discourse['class'][] = '';
$div_section_political_discourse['en'] = <<<HTML
	<h3><a href="/political_discourse.html">Political discourse</a></h3>

	<p>A healthy political discourse is an important part of democratic life.</p>
	HTML;
