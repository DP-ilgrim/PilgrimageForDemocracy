<?php
include_once('include/init.php');

$div_section_global_issues = newSection();
$div_section_global_issues['stars']   = 1;
$div_section_global_issues['class'][] = '';
$div_section_global_issues['en'] = <<<HTML
	<h3><a href="/global_issues.html">Global Issues</a></h3>

	<p>We live in an interconnected world, with issues affecting the whole of humanity.</p>
	HTML;
//$body .= printSection($div_section_global_issues);

$div_section_global_natural_resources = newSection();
$div_section_global_natural_resources['stars']   = 0;
$div_section_global_natural_resources['class'][] = '';
$div_section_global_natural_resources['en'] = <<<HTML
	<h3><a href="/global_natural_resources.html">Global Natural Resources</a></h3>

	<p>Sharing the limited resources of a finite world.</p>
	HTML;
//$body .= printSection($div_section_global_natural_resources);

$div_section_hunger = newSection();
$div_section_hunger['stars']   = 1;
$div_section_hunger['class'][] = '';
$div_section_hunger['en'] = <<<HTML
	<h3><a href="/hunger.html">Hunger</a></h3>

	<p>How do we reconcile food waste with famine?</p>
	HTML;
//$body .= printSection($div_section_hunger);

$div_section_peaceful_opposition_war = newSection();
$div_section_peaceful_opposition_war['stars']   = 1;
$div_section_peaceful_opposition_war['class'][] = '';
$div_section_peaceful_opposition_war['en'] = <<<HTML
	<h3><a href="/peaceful_resistance_in_times_of_war.html">Peaceful resistance in times of war</a></h3>

	<p>Stories of peaceful resistance to bellicose autocratic regimes.</p>
	HTML;
//$body .= printSection($div_section_peaceful_opposition_war);
