<?php
include_once('include/init.php');


$div_section_democracy = newSection();
$div_section_democracy['stars']   = 0;
$div_section_democracy['class'][] = '';
$div_section_democracy['en'] = <<<HTML
	<h3><a href="/democracy.html">Democracy</a></h3>

	<p>
	</p>
	HTML;

$div_section_decentralized_autonomous_organization = new ContentSection();
$div_section_decentralized_autonomous_organization->stars(0);
$div_section_decentralized_autonomous_organization->content = <<<HTML
	<h3><a href="/decentralized_autonomous_organization.html">decentralized autonomous organization</a></h3>

	<p>A blockchain-based democratic decision-making process.</p>
	HTML;

$div_section_types_of_democracy = new ContentSection();
$div_section_types_of_democracy->stars(0);
$div_section_types_of_democracy->content = <<<HTML
	<h3><a href="/types_of_democracy.html">types of democracy</a></h3>

	<p>
	</p>
	HTML;
