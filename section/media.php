<?php
include_once('include/init.php');

$div_section_media = newSection();
$div_section_media['stars']   = 1;
$div_section_media['class'][] = '';
$div_section_media['en'] = <<<HTML
	<h3><a href="/media.html">Media</a></h3>

	<p>A good media environment is critical for a healthy and stable democracy.</p>

	<p>The quality of our knowledge of public matters is
	commensurate with the quality of the media that deliver us the information
	upon which we rely to create our own opinion of what is right and what is wrong, whom to vote for or against, etc.</p>
	HTML;

$div_section_foreign_influence_local_media = newSection();
$div_section_foreign_influence_local_media['stars']   = 2;
$div_section_foreign_influence_local_media['class'][] = '';
$div_section_foreign_influence_local_media['en'] = <<<HTML
	<h3><a href="/foreign_influence_local_media.html">Foreign influence in local media landscape</a></h3>

	<p>For good or for evil, openly or covertly, countries routinely influence each other's media landscape.
	Some countries have adopted legislation to control, regular, curtail or restrict foreign influence.</p>
	HTML;

$div_section_reporters_without_borders = newSection();
$div_section_reporters_without_borders['stars']   = 0;
$div_section_reporters_without_borders['class'][] = '';
$div_section_reporters_without_borders['en'] = <<<HTML
	<h3><a href="/reporters_without_borders.html">Reporters Without Borders</a></h3>

	<p>Reporters Without Borders is an international non-profit and non-governmental organization
	with the stated aim of safeguarding the right to freedom of information.
	It describes its advocacy as founded on the belief that everyone requires access to the news and information,
	in line with Article 19 of the Universal Declaration of Human Rights that recognizes the right to receive and share information regardless of frontiers.</p>
	HTML;

$div_section_information_overload = new ContentSection();
$div_section_information_overload->stars(2);
$div_section_information_overload->content = <<<HTML
	<h3><a href="/information_overload.html">Information overload</a></h3>

	<p>Often, we consume too much information for our own good.</p>
	HTML;

$div_section_ground_news = new ContentSection();
$div_section_ground_news->stars(1);
$div_section_ground_news->content = <<<HTML
	<h3><a href="/ground_news.html">Ground News</a></h3>

	<p>Ground News aims to be "a platform that makes it easy to compare news sources,
	read between the lines of media bias and break free from algorithms."</p>
	HTML;
