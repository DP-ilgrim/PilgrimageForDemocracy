<?php
include_once('include/init.php');

$div_section_project = newSection();
$div_section_project['stars']   = 3;
$div_section_project['class'][] = '';
$div_section_project['en'] = <<<HTML
	<h3><a href="/project/">Project</a></h3>

	<p>All the pages introducing the different aspects of the project
	"<em>Pilgrimage for Democracy and Social Justice</em>".</p>
	HTML;

$div_section_participate = newSection();
$div_section_participate['stars'] = 3;
$div_section_participate['en'] = <<<HTML
	<h3><a href="/project/participate.html">Participate</a></h3>
	<p>Let's pool our resources, knowledge and skills.
	Teamwork and our collective intelligence will ensure the success of this project.</p>
	<p>Share all around you, all that which inspires you.</p>
	<p>Together, let's try to inspire people around us to sow the seeds of future peace.</p>
	HTML;

$div_section_development_website = newSection();
$div_section_development_website['stars']   = 1;
$div_section_development_website['class'][] = '';
$div_section_development_website['en'] = <<<HTML
	<h3><a href="/project/development_website.html">Development website</a></h3>

	<p>This page provides instructions on how to set up a development website on your own computer.
	You can then change the source code of the website and see the results straight away.</p>
	HTML;

$div_section_git = newSection();
$div_section_git['stars']   = 1;
$div_section_git['class'][] = '';
$div_section_git['en'] = <<<HTML
	<h3><a href="/project/git.html">Git workflow</a></h3>

	<p>Using `git` to contribute to the Pilgrimage for Democracy project.</p>
	HTML;

$div_section_copyright = newSection();
$div_section_copyright['stars'] = -1;
$div_section_copyright['en'] = <<<HTML
	<h3><a href="/project/copyright.html">Copyright?</a></h3>
	<p>The best ideas for a mature democracy and social justice do not belong to any single person or organization.
	They belong to all of humanity.</p>
	<p>Most of the content on this website is placed in the public domain.
	You can share, copy, duplicate, republish, modify all of the most important articles,
	and add your best ideas, research, and policy proposals.</p>
	<p>There are, however, some minor exceptions for third-party material and code.
	You can check this page for details.</p>
	HTML;

$div_section_updates = newSection();
$div_section_updates['stars'] = -1;
$div_section_updates['en'] = <<<HTML
	<h3><a href="/project/updates.html">Updates</a></h3>
	<p>If you are already very familiar with the existing content of this website,
	this page makes it convenient to check all the newest updates and additions since your last visit.</p>
	HTML;

$div_section_menu = newSection();
$div_section_menu['stars'] = -1;
$div_section_menu['en'] = <<<HTML
	<h3><a href="/menu.html">Menu</a></h3>
	<p>There are so many topics to cover!
	Check the growing listing of sections and articles that are already published.
	You can also preview some of the topics that we plan to develop in the next few months and years.</p>
	<p>The menu provides a convenient entry point for new and returning visitors.</p>
	HTML;

$div_section_wikipedia = newSection();
$div_section_wikipedia['stars']   = 2;
$div_section_wikipedia['class'][] = '';
$div_section_wikipedia['en'] = <<<HTML
	<h3><a href="/project/wikipedia.html">Wikipedia</a></h3>

	<p>Discusses some differences and similarities between the Wikipedia project and the Pilgrimage for Democracy and Social Justice project.</p>
	HTML;

$div_section_standing_on_the_shoulders_of_giants = newSection();
$div_section_standing_on_the_shoulders_of_giants['stars']   = 1; // 1 star = 2 exploited giants.
$div_section_standing_on_the_shoulders_of_giants['class'][] = '';
$div_section_standing_on_the_shoulders_of_giants['en'] = <<<HTML
	<h3><a href="/project/standing_on_the_shoulders_of_giants.html">Standing on the shoulders of giants</a></h3>

	<p>Our nascent project is still tiny, but will achieve its goals by building up on the work of very large organizations.</p>
	HTML;

$div_section_list_of_people = new ContentSection();
$div_section_list_of_people->stars(0);
$div_section_list_of_people->content = <<<HTML
	<h3><a href="/list_of_people.html">List of people</a></h3>

	<p>
	</p>
	HTML;

$div_section_lists = newSection();
$div_section_lists['stars']   = 2;
$div_section_lists['class'][] = '';
$div_section_lists['en'] = <<<HTML
	<h3><a href="/lists.html">Lists and topics</a></h3>

	<p>List of lists and topical entry pages.</p>
	HTML;

$div_section_list_of_organisations = new ContentSection();
$div_section_list_of_organisations->stars(0);
$div_section_list_of_organisations->content = <<<HTML
	<h3><a href="/list_of_organisations.html">List of organisations</a></h3>

	<p>
	</p>
	HTML;

$div_section_codeberg = new ContentSection();
$div_section_codeberg->stars(2);
$div_section_codeberg->content = <<<HTML
	<h3><a href="/project/codeberg.html">Codeberg</a></h3>

	<p>How to use Codeberg to participate in the project.</p>
	HTML;

$div_section_list_of_movies = new ContentSection();
$div_section_list_of_movies->stars(0);
$div_section_list_of_movies->content = <<<HTML
	<h3><a href="/list_of_movies.html">List of movies</a></h3>

	<p>
	</p>
	HTML;

$div_section_list_of_books = new ContentSection();
$div_section_list_of_books->stars(0);
$div_section_list_of_books->content = <<<HTML
	<h3><a href="/list_of_books.html">List of books</a></h3>

	<p>
	</p>
	HTML;

$div_section_list_of_indices = new ContentSection();
$div_section_list_of_indices->stars(0);
$div_section_list_of_indices->content = <<<HTML
	<h3><a href="/list_of_indices.html">list of indices</a></h3>

	<p>All kinds of rating and indices about democracy, peace, freedom, justice etc.
	published by a variety of organisations.</p>
	HTML;
