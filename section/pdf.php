<?php
include_once('include/init.php');

$div_section_the_hidden_dimension_in_democracy = new ContentSection();
$div_section_the_hidden_dimension_in_democracy->stars(0);
$div_section_the_hidden_dimension_in_democracy->content = <<<HTML
	<h3><a href="/the_hidden_dimension_in_democracy.html">The Hidden Dimension in Democracy</a></h3>

	<p><strong>Abstract</strong>:
	"<em>The multidimensional dataset provided by the Varieties of Democracy (V-Dem) project reflects
	the complexity of the concept of democracy. And yet, all standard democracy measures are
	one-dimensional indices. Through a statistics-based approach, we identify two so-far hidden
	dimensions in the dataset. The first dimension aligns well with the V-Dem index of Electoral Democracy
	but the second dimension represents the so-far overlooked trade-off between
	electoral control and citizen freedom, which clearly distinguishes electoral autocracies from
	countries in which citizens are free but which struggle with corruption and violence.
	We interpret this second dimension as capturing a crucial element of stability for non-democracies.
	Using this second dimension relative to the first, we clarify long-standing debates on ‘waves of
	democracy’ and open up new avenues in understanding the recent phenomenon of ‘backsliding’
	among consolidated democracies.</em>"
	</p>
	HTML;
