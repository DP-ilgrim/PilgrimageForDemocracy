<?php
include_once('include/init.php');

$div_section_duverger_syndrome = newSection();
$div_section_duverger_syndrome['stars']   = 1;
$div_section_duverger_syndrome['class'][] = '';
$div_section_duverger_syndrome['en'] = <<<HTML
	<h3><a href="/duverger_syndrome.html">1: Duverger Syndrome</a></h3>

	<p>
	</p>
	HTML;
//$body .= printSection($div_section_duverger_syndrome);

$div_section_symptom_dualism = newSection();
$div_section_symptom_dualism['stars']   = 0;
$div_section_symptom_dualism['class'][] = '';
$div_section_symptom_dualism['en'] = <<<HTML
	<h3><a href="/duverger_syndrome_duality.html">Duverger symptom 1: divisive dualism</a></h3>

	<p>
	</p>
	HTML;

$div_section_symptom_alternance = newSection();
$div_section_symptom_alternance['stars']   = 0;
$div_section_symptom_alternance['class'][] = '';
$div_section_symptom_alternance['en'] = <<<HTML
	<h3><a href="/duverger_syndrome_alternance.html">Duverger symptom 2: destructive alternance</a></h3>

	<p>
	</p>
	HTML;

$div_section_symptom_party_politics = newSection();
$div_section_symptom_party_politics['stars']   = 0;
$div_section_symptom_party_politics['class'][] = '';
$div_section_symptom_party_politics['en'] = <<<HTML
	<h3><a href="/duverger_syndrome_party_politics.html">Duverger symptom 3: party politics</a></h3>

	<p>
	</p>
	HTML;

$div_section_symptom_negative_campaigning = newSection();
$div_section_symptom_negative_campaigning['stars']   = 0;
$div_section_symptom_negative_campaigning['class'][] = '';
$div_section_symptom_negative_campaigning['en'] = <<<HTML
	<h3><a href="/duverger_syndrome_negative_campaigning.html">Duverger symptom 4: negative campaigning</a></h3>

	<p>
	</p>
	HTML;

$div_section_symptom_lack_choice_good_candidates = newSection();
$div_section_symptom_lack_choice_good_candidates['stars']   = 0;
$div_section_symptom_lack_choice_good_candidates['class'][] = '';
$div_section_symptom_lack_choice_good_candidates['en'] = <<<HTML
	<h3><a href="/duverger_syndrome_lack_choice.html">Duverger symptom 5: lack of choice, lack of good candidates</a></h3>

	<p>
	</p>
	HTML;


$div_section_duverger_law = newSection();
$div_section_duverger_law['stars']   = 0;
$div_section_duverger_law['class'][] = '';
$div_section_duverger_law['en'] = <<<HTML
	<h3><a href="/duverger_law.html">Duverger's Law</a></h3>

	<p>
	</p>
	HTML;

$div_section_example_usa_19th_century = newSection();
$div_section_example_usa_19th_century['stars']   = 0;
$div_section_example_usa_19th_century['class'][] = '';
$div_section_example_usa_19th_century['en'] = <<<HTML
	<h3><a href="/duverger_usa_19_century.html">Duverger example: USA, 19th century</a></h3>

	<p>
	</p>
	HTML;

$div_section_example_taiwan_2000 = newSection();
$div_section_example_taiwan_2000['stars']   = 0;
$div_section_example_taiwan_2000['class'][] = '';
$div_section_example_taiwan_2000['en'] = <<<HTML
	<h3><a href="/duverger_taiwan_2000_2004.html">Duverger example: Taiwan 2000 and 2004 presidential elections</a></h3>

	<p>
	</p>
	HTML;

$div_section_example_france_alternance = newSection();
$div_section_example_france_alternance['stars']   = 0;
$div_section_example_france_alternance['class'][] = '';
$div_section_example_france_alternance['en'] = <<<HTML
	<h3><a href="/duverger_france_alternance_danger.html">Duverger example: France and the dangers of alternance</a></h3>

	<p>
	</p>
	HTML;
