<?php
include_once('include/init.php');

$div_section_taxes = newSection();
$div_section_taxes['stars'] = 0;
$div_section_taxes['class'][] = '';
$div_section_taxes['en'] = <<<HTML
	<h3><a href="/taxes.html">Taxes</a></h3>
	<p>It is said that there are only two things that are certain in life: death and taxes.	</p>
	<p>The government needs the financial resources to perform its duties.
	Taxes are obviously necessary.
	However, not all taxes are created equal.</p>
	<p>What criteria can we use to evaluate the usefulness of a tax, and its contribution to running a balanced society?
	What taxes are harmful and should be abolished?
	What taxes would, overall, be more beneficial?
	What proportions do harmful taxes take in a government's budget, as compared to more benign taxes?
	</p>
	HTML;
//$body .= printSection($div_section_taxes);
