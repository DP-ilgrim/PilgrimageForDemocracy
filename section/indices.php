<?php
include_once('include/init.php');


$div_section_global_peace_index = new ContentSection();
$div_section_global_peace_index->stars(0);
$div_section_global_peace_index->content = <<<HTML
	<h3><a href="/global_peace_index.html">Global Peace Index</a></h3>

	<p>The Global Peace Index (GPI), published by the $IEP, measures the relative peacefulness of each country.</p>
	HTML;
