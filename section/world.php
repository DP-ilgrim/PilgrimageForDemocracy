<?php
include_once('include/init.php');

$div_section_democracy_world = newSection();
$div_section_democracy_world['stars']   = 1;
$div_section_democracy_world['class'][] = '';
$div_section_democracy_world['en'] = <<<HTML
	<h3><a href="/world.html">List of countries and international organizations</a></h3>

	<p>Although we are only getting started, we aim to progressively extend our coverage of countries around the world.</p>
	HTML;

$div_section_united_nations = newSection();
$div_section_united_nations['stars']   = 2;
$div_section_united_nations['class'][] = '';
$div_section_united_nations['en'] = <<<HTML
	<h3><a href="/united_nations.html">The United Nations</a></h3>

	<p>The United Nations has a mandate to maintain peace among nations.</p>
	HTML;

$div_section_osce = newSection();
$div_section_osce['stars']   = 0;
$div_section_osce['class'][] = '';
$div_section_osce['en'] = <<<HTML
	<h3><a href="/osce.html">OSCE (Organization for Security and Co-operation in Europe)</a></h3>

	<p>The OSCE's mandate includes issues such as arms control, promotion of human rights, freedom of the press, and free and fair elections.</p>
	HTML;

$div_section_costa_rica = newSection();
$div_section_costa_rica['stars']   = 1;
$div_section_costa_rica['class'][] = '';
$div_section_costa_rica['en'] = <<<HTML
	<h3><a href="/costa_rica.html">Costa Rica</a></h3>

	<p>A democracy in Central America, with good press freedom.</p>
	HTML;

$div_section_prc_china = newSection();
$div_section_prc_china['stars']   = 0;
$div_section_prc_china['class'][] = '';
$div_section_prc_china['en'] = <<<HTML
	<h3><a href="/prc_china.html">The People's Republic of China</a></h3>

	<p>The People's Republic of China represents one of the most critical frontlines for democracy in the world.</p>
	HTML;

$div_section_georgia = newSection();
$div_section_georgia['stars']   = 1;
$div_section_georgia['class'][] = '';
$div_section_georgia['en'] = <<<HTML
	<h3><a href="/georgia.html">Georgia</a></h3>
	<p>A former Soviet Union republic, Georgia is caught in the repercutions of Russia's war in Ukraine.</p>
	HTML;

$div_section_iran = newSection();
$div_section_iran['stars']   = 1;
$div_section_iran['class'][] = '';
$div_section_iran['en'] = <<<HTML
	<h3><a href="/iran.html">Iran</a></h3>

	<p>Imposition of Islamic law, a continuing economic crisis, lack of freedom of expression,
	violation of women's rights, brutality carried out during protests, internet cutoffs,
	and the killing of Mahsa Amini were some of the reasons for the start of civil protests in Iran in 2021-2022.</p>
	HTML;

$div_section_mali = newSection();
$div_section_mali['stars']   = 0;
$div_section_mali['class'][] = '';
$div_section_mali['en'] = <<<HTML
	<h3><a href="/mali.html">Mali</a></h3>

	<p>
	</p>
	HTML;

$div_section_philippines = newSection();
$div_section_philippines['stars']   = 1;
$div_section_philippines['class'][] = '';
$div_section_philippines['en'] = <<<HTML
	<h3><a href="/philippines.html">Philippines</a></h3>

	<p>
	</p>
	HTML;

$div_section_russia = newSection();
$div_section_russia['stars']   = 1;
$div_section_russia['class'][] = '';
$div_section_russia['en'] = <<<HTML
	<h3><a href="/russia.html">Russia</a></h3>
	<p>Russia is one of the major threat to global peace and democracy.</p>
	HTML;

$div_section_turkey = newSection();
$div_section_turkey['stars']   = 0;
$div_section_turkey['class'][] = '';
$div_section_turkey['en'] = <<<HTML
	<h3><a href="/turkey.html">Turkey</a></h3>

	<p>
	</p>
	HTML;

$div_section_ukraine = newSection();
$div_section_ukraine['stars']   = 2;
$div_section_ukraine['class'][] = '';
$div_section_ukraine['en'] = <<<HTML
	<h3><a href="/ukraine.html">Ukraine</a></h3>

	<p>The front line of the war for democracy.</p>
	HTML;

$div_section_taiwan = new ContentSection();
$div_section_taiwan->stars(1);
$div_section_taiwan->content = <<<HTML
	<h3><a href="/taiwan.html">Taiwan</a></h3>

	<p>
	</p>
	HTML;

$div_section_taiwan_ukraine_relations = new ContentSection();
$div_section_taiwan_ukraine_relations->stars(0);
$div_section_taiwan_ukraine_relations->content = <<<HTML
	<h3><a href="/taiwan_ukraine_relations.html">Taiwan–Ukraine relations</a></h3>

	<p>Two democracies threatened by aggressive totalitarian neighbours...</p>
	HTML;

$div_section_china_and_taiwan = new ContentSection();
$div_section_china_and_taiwan->stars(0);
$div_section_china_and_taiwan->content = <<<HTML
	<h3><a href="/china_and_taiwan.html">China and Taiwan</a></h3>

	<p>
	</p>
	HTML;
