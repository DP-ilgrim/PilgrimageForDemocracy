<?php
include_once('include/init.php');

$div_section_the_remains_of_the_day = new ContentSection();
$div_section_the_remains_of_the_day->stars(3);
$div_section_the_remains_of_the_day->content = <<<HTML
	<h3><a href="/the_remains_of_the_day.html">The Remains of the Day</a></h3>

	<p>The novel "The Remains of the Day", and the film adapted from it, is set in Britain in the 1930's.
	Pre-World War II Britain is seen through the eyes of Stevens,
	a butler who dedicated his whole life to be at the service of nobility,
	taking care of the large mansion of a Lord Darlington.</p>
	HTML;
